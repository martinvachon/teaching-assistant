const path = require('path')
const fs = require('fs')
const { create } = require('xmlbuilder2')

const { version } = require('../package.json')

const {
    COURSE_FILE_NAME,
    getOutPath,
    getLanguages,
    getSectionFolder,
    logger,
    getMetas,
    SECTION_FOLDER,
    META_DOC,
    META_VERSION,
    META_COURSE_DATETIME
} = require('./build')

function appendSection (course, xmlString) {
    const firstFileBodyElement = course.root().find(xmlBuilder => xmlBuilder.node.nodeName === 'body')

    // For every others files, parse the file
    const sectionFile = create(xmlString)
    // Filter section elements
    const sections = sectionFile.filter(xmlBuilder => {
        return xmlBuilder.node.nodeName === 'section'
    }, false, true)
    // Append each section in the first file
    sections.forEach(section => firstFileBodyElement.import(section))
}

function appendSectionMetas (course, language) {
    const metas = getMetas(language)
    metas.forEach(meta => {
        if (meta['@name'] === META_DOC) {
            const folder = path.join(meta['@content'], language.code, SECTION_FOLDER)
            logger.info(META_DOC + ' Import section from: ' + folder)
            fs.readdirSync(folder).forEach((fileName, index) => {
                const sectionFile = path.join(folder, fileName)
                const xmlString = fs.readFileSync(sectionFile, { encoding: 'utf8', flag: 'r' })
                appendSection(course, xmlString)
            })
        }
    })
}

function formatCourseDateTime () {
    const pad = (value) => value.toString().padStart(2, '0')
    const date = new Date()

    return date.getFullYear() +
        '-' +
        pad(date.getMonth() + 1) +
        '-' +
        pad(date.getDate()) +
        ' ' +
        pad(date.getHours()) +
        ':' +
        pad(date.getMinutes()) +
        ':' +
        pad(date.getSeconds())
}

function generateCourseXML (language) {
    let course

    const folder = getSectionFolder(language)
    fs.readdirSync(folder).forEach((fileName, index) => {
        const sectionFile = path.join(folder, fileName)
        const xmlString = fs.readFileSync(sectionFile, { encoding: 'utf8', flag: 'r' })

        if (index === 0) {
            // First file in alphabetic order
            course = create(xmlString)
            course.find(xmlBuilder => xmlBuilder.node.nodeName === 'html').att('lang', language.code)
            const headElement = course.find(xmlBuilder => xmlBuilder.node.nodeName === 'head', false, true)
            headElement.ele('meta', { name: META_VERSION, content: version })
            headElement.ele('meta', { name: META_COURSE_DATETIME, content: formatCourseDateTime() })
        } else {
            appendSection(course, xmlString)
        }
    })

    appendSectionMetas(course, language)

    fs.writeFileSync(
        path.join(getOutPath(), language.code, COURSE_FILE_NAME + '.xml'),
        course.end({ prettyPrint: true })
    )
}

function aggregate (context) {
    if (context.error) return

    const AGGREGATE_TIME = 'Aggregate'
    logger.profile(AGGREGATE_TIME)

    getLanguages().forEach(language => {
        const distLanguageFolder = path.join(getOutPath(), language.code)
        if (!fs.existsSync(distLanguageFolder)) {
            fs.mkdirSync(distLanguageFolder)
        }

        generateCourseXML(language)

        logger.info('XML generated from section: ' + language.code)
    })

    logger.profile(AGGREGATE_TIME)
}

module.exports = aggregate
