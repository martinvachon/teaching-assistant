const path = require('path')
const copyDir = require('copy-dir')
const fs = require('fs')
const { create } = require('xmlbuilder2')
const childProcess = require('child_process')

const {
    DOC_FOLDER,
    LIB_FOLDER,
    getRootPath,
    getProjectRootPath,
    getOutPath,
    getLanguages,
    getMetas,
    logger,
    DIST_FOLDER,
    OUT_FOLDER,
    DISTRIBUTION_ARCHIVE_FILE_NAME,
    ASSET_FOLDER,
    META_DOC
} = require('./build')

function resetDistFolder () {
    // Delete and create /dist/out folder
    const distPath = path.join(getProjectRootPath(), DIST_FOLDER)
    fs.rmSync(path.join(distPath, OUT_FOLDER), { force: true, recursive: true })
    fs.mkdirSync(getOutPath(), { recursive: true })

    // Delete archive file
    fs.rmSync(path.join(distPath, DISTRIBUTION_ARCHIVE_FILE_NAME), { force: true })
}

function copyDocFolder () {
    copyDir.sync(path.join(getProjectRootPath(), DOC_FOLDER), getOutPath(), {
        filter: function (stat, filepath, filename) {
            return !(stat === 'directory' && filename === 'section')
        }
    })
    logger.info('Doc asset copied')

    getLanguages().forEach(language => {
        const metas = getMetas(language)
        metas.forEach(meta => {
            if (meta['@name'] === META_DOC) {
                const folder = path.join(meta['@content'], language.code, ASSET_FOLDER)
                if (fs.existsSync(folder)) {
                    copyDir.sync(folder, path.join(getOutPath(), language.code, ASSET_FOLDER))
                    logger.info(META_DOC + ' copied assets from: ' + folder)
                }
            }
        })
    })
}

function copyLibfolder () {
    const libPath = path.join(getOutPath(), LIB_FOLDER)
    if (!fs.existsSync(libPath)) {
        fs.mkdirSync(libPath)
    }
    copyDir.sync(path.join(getRootPath(), LIB_FOLDER), libPath)
    logger.info('Lib folder copied')
}

function generateIndexPageXML () {
    const indexElement = create({ encoding: 'utf-8' }).ele('index')

    const rootPath = path.resolve(__dirname, '..')
    indexElement.att('rootPath', rootPath.split(path.sep).join(path.posix.sep))

    const languageElements = indexElement.ele('languages')
    getLanguages().forEach(language => {
        const languageElement = languageElements.ele('language', { code: language.code, nativeName: language.nativeName })
        const pages = languageElement.ele('pages')

        const metas = getMetas(language)
        // todo : duplicate
        const META_GENERATOR = 'teaching-assistant-generator'

        metas.forEach(meta => {
            if (meta['@name'] === META_GENERATOR) {
                pages.ele('page', { name: meta['@content'], extension: '.html' })
            }
        })
    })

    fs.writeFileSync(
        path.join(getOutPath(), 'index.xml'),
        indexElement.end({ prettyPrint: true })
    )
}

function transformIndexPageHTML () {
    const xslt3Command = path.join(getRootPath(), 'node_modules/.bin/xslt3')
    const xslFile = path.join(getRootPath(), LIB_FOLDER, 'index-page/index-page.xslt')

    const xmlFile = path.join(getOutPath(), 'index.xml')
    const resultFile = path.join(getOutPath(), 'index.html')

    const xslt3Params = ['-s:' + xmlFile, '-xsl:' + xslFile, '-o:' + resultFile]

    logger.debug('xslt3 params:', xslt3Params)

    const result = childProcess.spawnSync(xslt3Command, xslt3Params, { shell: true })
    if (result.status !== 0) {
        process.stderr.write(result.stderr)
        process.exit(result.status)
    } else {
        process.stdout.write(result.stdout)
        process.stderr.write(result.stderr)
    }
}

function initialize (context) {
    if (context.error) return

    const INITIALIZE_TIME = 'Initialize'
    logger.profile(INITIALIZE_TIME)
    resetDistFolder()
    copyDocFolder()
    copyLibfolder()
    generateIndexPageXML()
    transformIndexPageHTML()
    logger.profile(INITIALIZE_TIME)
}

module.exports = initialize
