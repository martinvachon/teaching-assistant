const childProcess = require('child_process')
const path = require('path')

const {
    LIB_FOLDER,
    COURSE_FILE_NAME,
    getRootPath,
    getOutPath,
    getLanguages,
    getMetas,
    logger
} = require('./build')

function executeXSLT (language, xslFile, resultFileName) {
    const xslt3Command = path.join(getRootPath(), 'node_modules/.bin/xslt3')

    const xmlFile = path.join(getOutPath(), language.code, COURSE_FILE_NAME + '.xml')
    const resultFile = path.join(getOutPath(), language.code, resultFileName + '.html')

    const xslt3Params = ['-s:' + xmlFile, '-xsl:' + xslFile, '-o:' + resultFile]

    logger.debug('xslt3 params:', xslt3Params)

    const result = childProcess.spawnSync(xslt3Command, xslt3Params, { shell: true })
    if (result.status !== 0) {
        process.stderr.write(result.stderr)
        process.exit(result.status)
    } else {
        process.stdout.write(result.stdout)
        process.stderr.write(result.stderr)
    }
}

function generateCourseHTML (language) {
    const metas = getMetas(language)

    const META_GENERATOR = 'teaching-assistant-generator'

    metas.forEach(meta => {
        if (meta['@name'] === META_GENERATOR) {
            const content = meta['@content']

            if (content === 'moodle-player') {
                executeXSLT(language, path.join(getRootPath(), LIB_FOLDER, 'moodle-player/moodle-player.xslt'), 'moodle-player')
            } else if (content === 'moodle-slides') {
                executeXSLT(language, path.join(getRootPath(), LIB_FOLDER, 'moodle-slides/moodle-slides.xslt'), 'moodle-slides')
            } else if (content.startsWith('moodle-player_')) {
                const betaFile = content.substr('moodle-player_'.length)
                console.log('BEATA', betaFile)
                executeXSLT(language, path.join(getRootPath(), LIB_FOLDER, 'moodle-player/moodle-player_' + betaFile + '.xslt'), 'moodle-player_' + betaFile)
            } else {
                logger.error('Invalid ' + META_GENERATOR + ' meta tag. [moodle-player|moodle-slides]')
            }
        }
    })
}

function transform (context) {
    if (context.error) return

    getLanguages().forEach(language => {
        const xsltTimer = 'XSLT ' + language.code
        logger.profile(xsltTimer)

        generateCourseHTML(language)

        logger.profile(xsltTimer)
        logger.info('HTML generated from: ' + language.code)
    })
}

module.exports = transform
