const xmlChecker = require('xmlchecker')
const fs = require('fs')
const packageJson = require('../package.json')

const fileNameFR = './doc/' + packageJson.name + '_fr.xml'
const fileNameEN = './doc/' + packageJson.name + '_en.xml'

fs.readFile(fileNameFR, 'UTF-8', function read (err, data) {
    if (err) {
        throw err
    }

    try {
        console.log('Start validation: ' + fileNameFR)
        xmlChecker.check(data)
        console.log('Complete validation: success')
    } catch (error) {
        console.log(
            'Complete validation: ' +
      error.name +
      ' at ' +
      error.line +
      ',' +
      error.column +
      ': ' +
      error.message
        )
        process.exit(1)
    }
})

fs.readFile(fileNameEN, 'UTF-8', function read (err, data) {
    if (err) {
        throw err
    }

    try {
        console.log('Start validation: ' + fileNameEN)
        xmlChecker.check(data)
        console.log('Complete validation: success')
    } catch (error) {
        console.log(
            'Complete validation: ' +
        error.name +
        ' at ' +
        error.line +
        ',' +
        error.column +
        ': ' +
        error.message
        )
        process.exit(1)
    }
})
