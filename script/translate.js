const path = require('path')
const fs = require('fs')
const jsdom = require('jsdom')
const { JSDOM } = jsdom
const fetch = require('cross-fetch')
const ISO6391 = require('iso-639-1')

const {
    getOutPath,
    getSectionFolder,
    logger,
    COURSE_FILE_NAME
} = require('./build')

// Node.TEXT_NODE
const TEXT_NODE = 3

const CONTENT_TAGS = ['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'li', 'p', 'strong', 'em', 'span', 'label', 'td', 'th', 'caption', 'summary', 'legend']
const UPPERCASE_TAGS = ['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'li', 'p', 'caption', 'summary', 'legend']

async function requestTranslate (text, url) {
    const res = await fetch(url, {
        method: 'POST',
        body: JSON.stringify({
            q: text,
            source: 'fr',
            target: 'en'
        }),
        headers: { 'Content-Type': 'application/json' }
    })

    const result = await res.json()
    return result.translatedText
}

function getEnSectionFolder () {
    const language = {
        code: 'en',
        nativeName: ISO6391.getNativeName('en')
    }
    return getSectionFolder(language)
}

function restoreHTMLEntities (text) {
    return text // .replace(/&amp;amp;/g, '&amp;').replace(/&amp;lt;/g, '&lt;')
}

async function translateAttributes (elements, url) {
    for (const element of elements) {
        if (element.hasAttribute('data-title')) {
            const title = await requestTranslate(element.getAttribute('data-title'), url)
            element.setAttribute('data-title', title.charAt(0).toUpperCase() + title.slice(1))
        }
        if (element.hasAttribute('data-subtitle')) {
            const subtitle = await requestTranslate(element.getAttribute('data-subtitle'), url)
            element.setAttribute('data-subtitle', subtitle.charAt(0).toUpperCase() + subtitle.slice(1))
        }
    }
}

async function translateElements (elements, tag, url) {
    for (const element of elements) {
        const childs = element.childNodes
        for (const child of childs) {
            if (child.nodeType === TEXT_NODE) {
                child.textContent = await requestTranslate(restoreHTMLEntities(child.textContent), url)

                // Basic attempt to uppercase first letter.
                // The input file may contain indentation or carriage return, in that case, it's doesn't work.
                if (UPPERCASE_TAGS.includes(tag) && Array.prototype.indexOf.call(childs, child) === 0) {
                    child.textContent = child.textContent.charAt(0).toUpperCase() + child.textContent.slice(1)
                }
            }
        }
    }
}

async function translateCourse (dom, tags, url) {
    dom.window.document.documentElement.removeAttribute('lang')
    dom.window.document.querySelector('meta[name=teaching-assistant-version]').remove()
    dom.window.document.querySelector('meta[name=teaching-assistant-course-datetime]').remove()

    for (let i = 0; i < tags.length; i++) {
        const tag = tags[i]
        logger.info('Translating tags: ' + tag)
        await translateElements(dom.window.document.getElementsByTagName(tag), tag, url)
    }

    logger.info('Translating attributes for tags: code')
    await translateAttributes(dom.window.document.getElementsByTagName('code'), url)

    logger.info('Translating attributes for tags: img')
    await translateAttributes(dom.window.document.getElementsByTagName('img'), url)

    const sectionFolder = getEnSectionFolder()
    if (!fs.existsSync(sectionFolder)) {
        fs.mkdirSync(sectionFolder, { recursive: true })
    }

    fs.writeFileSync(
        path.join(sectionFolder, 'automatic.xml'),
        dom.serialize()
    )
}

async function translate (parameters) {
    let url = 'http://localhost:5000/translate'
    if (parameters.length > 0) {
        url = parameters[0].substring(6)
    }

    logger.info('Using translation service URL: ' + url)

    const enSectionFolder = getEnSectionFolder()
    if (!fs.existsSync(enSectionFolder)) {
        // Use the generated file after aggregation as input
        const frXmlFile = path.join(getOutPath(), 'fr', COURSE_FILE_NAME + '.xml')

        // The XSLT use use-character-maps to avoid the reality and here we pay the price
        const fileString = fs.readFileSync(frXmlFile, { encoding: 'utf8', flag: 'r' })
        const replacedEntities = fileString
            .replace(/&amp;amp;/g, '&amp;')
            .replace(/&amp;lt;/g, '&lt;')
            .replace(/&amp;gt;/g, '&#62;')
            .replace(/&amp;apos;/g, '&apos;')

        const options = {
            contentType: 'text/xml; charset=UTF-8'
        }
        const dom = new JSDOM(replacedEntities, options)
        await translateCourse(dom, CONTENT_TAGS, url)

        // JSDOM.fromFile(frXmlFile, options).then(async function (dom) {
        //    await translateCourse(dom, CONTENT_TAGS, url)
        // })
    } else {
        logger.warn('The translation operation can not update existing file. The en folder already exist: ' + enSectionFolder)
    }
}

module.exports = translate
