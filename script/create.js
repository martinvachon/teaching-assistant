const path = require('path')
const copyDir = require('copy-dir')
const fs = require('fs')

const {
    // DOC_FOLDER,
    SRC_FOLDER,
    TEMPLATE_FOLDER,
    PROJECT_FOLDER,
    getRootPath,
    getProjectRootPath,
    logger
} = require('./build')

/* function copyDocFolder (context) {
    const projectDocFolder = path.join(getProjectRootPath(), DOC_FOLDER)
    if (!fs.existsSync(projectDocFolder)) {
        fs.mkdirSync(projectDocFolder)
        copyDir.sync(path.join(getRootPath(), DOC_FOLDER), projectDocFolder)
    } else {
        logger.warn('Careful with that axe eugene. ' + DOC_FOLDER + ' already exist.')
        context.error = true
    }
} */

function copyTemplateProjectFolder (context) {
    const projectSrcFolder = path.join(getProjectRootPath(), SRC_FOLDER)
    if (!fs.existsSync(projectSrcFolder)) {
        copyDir.sync(path.join(getRootPath(), TEMPLATE_FOLDER, PROJECT_FOLDER), getProjectRootPath())
    } else {
        logger.warn('Careful with that axe eugene. ' + SRC_FOLDER + ' already exist.')
        context.error = true
    }
}

function create (context) {
    if (context.error) return

    // copyDocFolder(context)
    copyTemplateProjectFolder(context)
}

module.exports = create
