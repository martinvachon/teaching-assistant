const winston = require('winston')
const path = require('path')
const fs = require('fs')
const { create } = require('xmlbuilder2')
const ISO6391 = require('iso-639-1')

const DOC_FOLDER = 'doc'
const DIST_FOLDER = 'dist'
const LIB_FOLDER = 'lib'
const SRC_FOLDER = 'src'
const OUT_FOLDER = 'out'
const ASSET_FOLDER = 'asset'
const SECTION_FOLDER = 'section'
const TEMPLATE_FOLDER = 'template'
const PROJECT_FOLDER = 'project'

const COURSE_FILE_NAME = 'index'
const DISTRIBUTION_ARCHIVE_FILE_NAME = 'html.zip'

const PARMETER_PREFIX = '--'
const PORT_PARMETER_PREFIX = '--port='

const META_DOC = 'teaching-assistant-doc'
const META_VERSION = 'teaching-assistant-version'
const META_COURSE_DATETIME = 'teaching-assistant-course-datetime'

const winstonFormat = winston.format.combine(
    winston.format.colorize(),
    winston.format.timestamp({
        format: 'MMM-DD-YYYY HH:mm:ss'
    }),
    winston.format.align(),
    winston.format.printf(({ level, message, timestamp, ...metadata }) => {
        let msg = `${timestamp} [${level}] : ${message} `
        if (metadata && Object.keys(metadata).length > 0) {
            msg += JSON.stringify(metadata)
        }
        return msg
    })
)

const logConfiguration = {
    transports: [
        new winston.transports.Console({
            level: 'debug',
            format: winstonFormat
        })
    ]
}

const logger = winston.createLogger(logConfiguration)

/**
 * Helper function to return the project root folder
 * @returns Project root folder path
 */
function getRootPath () {
    return path.join(__dirname, '..')
}

function getProjectRootPath () {
    return process.cwd()
}

function getOutPath () {
    return path.join(getProjectRootPath(), DIST_FOLDER, OUT_FOLDER)
}

/**
 * Helper function for conversion of bytes to a readable value.
 * Reference: https://stackoverflow.com/questions/15900485/correct-way-to-convert-size-in-bytes-to-kb-mb-gb-in-javascript
 * @param {*} bytes
 * @param {*} decimals
 * @returns Formatted string
 */
function formatBytes (bytes, decimals = 2) {
    if (bytes === 0) return '0 Bytes'

    const k = 1024
    const dm = decimals < 0 ? 0 : decimals
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']

    const i = Math.floor(Math.log(bytes) / Math.log(k))

    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i]
}

function getSectionFolder (language) {
    const languageFolder = path.join(getProjectRootPath(), DOC_FOLDER, language.code)
    return path.join(languageFolder, SECTION_FOLDER)
}

function getMetas (language) {
    let metas = []

    const sectionFolder = getSectionFolder(language)
    const sectionFiles = fs.readdirSync(sectionFolder)
    // Only the first file contain meta element
    if (sectionFiles.length > 0) {
        const xmlString = fs.readFileSync(path.join(sectionFolder, sectionFiles[0]), { encoding: 'utf8', flag: 'r' })
        const xmlDoc = create(xmlString)

        const headElement = xmlDoc.root().find(xmlBuilder => xmlBuilder.node.nodeName === 'head')
        metas = headElement
            .filter(xmlBuilder => xmlBuilder.node.nodeName === 'meta')
            .map(meta => meta.toObject().meta)
    } else {
        logger.warn('No xml file found in: ' + sectionFolder)
    }

    return metas
}

function getLanguages () {
    const docFolders = fs.readdirSync(path.join(getProjectRootPath(), DOC_FOLDER))
    const languages = docFolders.filter(folderName => {
        const languageFolder = path.join(getProjectRootPath(), DOC_FOLDER, folderName)
        const stats = fs.statSync(languageFolder)

        return folderName !== ASSET_FOLDER && stats.isDirectory() && ISO6391.validate(folderName)
    })

    return languages.map(language => {
        return {
            code: language,
            nativeName: ISO6391.getNativeName(language)
        }
    })
}

module.exports = {
    DOC_FOLDER,
    DIST_FOLDER,
    LIB_FOLDER,
    SRC_FOLDER,
    OUT_FOLDER,
    ASSET_FOLDER,
    SECTION_FOLDER,
    TEMPLATE_FOLDER,
    PROJECT_FOLDER,
    COURSE_FILE_NAME,
    DISTRIBUTION_ARCHIVE_FILE_NAME,
    PARMETER_PREFIX,
    PORT_PARMETER_PREFIX,
    META_DOC,
    META_VERSION,
    META_COURSE_DATETIME,
    getRootPath,
    getProjectRootPath,
    getOutPath,
    formatBytes,
    getLanguages,
    getSectionFolder,
    getMetas,
    logger
}
