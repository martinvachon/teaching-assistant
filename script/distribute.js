const fs = require('fs')
const path = require('path')
const archiver = require('archiver')

const {
    DIST_FOLDER,
    DISTRIBUTION_ARCHIVE_FILE_NAME,
    getProjectRootPath,
    formatBytes,
    getOutPath,
    logger
} = require('./build')

function distribute (context) {
    if (context.error) return

    const DISTRIBUTE_TIME = 'Distribute'

    const archiveFileName = path.join(getProjectRootPath(), DIST_FOLDER, DISTRIBUTION_ARCHIVE_FILE_NAME)
    const outputStream = fs.createWriteStream(archiveFileName)
    const archive = archiver('zip')

    archive.on('error', function (error) {
        throw error
    })

    outputStream.on('close', function () {
        logger.info('Archive created: ' + archiveFileName + ' ' + formatBytes(archive.pointer()))
        logger.profile(DISTRIBUTE_TIME)
    })

    archive.pipe(outputStream)
    archive.directory(getOutPath(), false)

    logger.profile(DISTRIBUTE_TIME)
    archive.finalize()
}

module.exports = distribute
