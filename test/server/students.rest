###

GET http://localhost:8080/students HTTP/1.1



###

GET http://localhost:8080/students/2 HTTP/1.1


###

POST http://localhost:8080/students HTTP/1.1
content-type: application/json

{
    "firstName": "Dude firstName",
    "lastName": "Dude lastName",
    "permanentCode": "code666",
    "email": "dude@server.com"
}

###

PUT http://localhost:8080/students HTTP/1.1
content-type: application/json

{
    "id": "3",
    "firstName": "Dude firstName update",
    "lastName": "Dude lastName update",
    "permanentCode": "code667",
    "email": "dude@server.com"
}

###

DELETE http://localhost:8080/students/3 HTTP/1.1


