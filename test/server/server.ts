import path from 'path'
import fs from 'fs'

import { startServer } from '../../src/server'

const userHomeFolder = path.join('test', 'server', 'userhome')
if (!fs.existsSync(userHomeFolder)) {
    fs.mkdirSync(userHomeFolder)
}

const repository = path.join(__dirname, 'repository')

/**
 * Start the server in test mode using test folder instead of user home for database and configuration.
 * The repository simulation is also executed in a second test folder.
 */
startServer(userHomeFolder, repository)
