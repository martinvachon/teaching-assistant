SELECT absenceDate FROM absence;

SELECT date(absenceDate, 'unixepoch') FROM absence;

SELECT strftime('%s', date('Now', 'localtime'));


-- INTEGER NOT NULL DEFAULT (strftime('%s', DateTime('Now', 'localtime'))),


SELECT
    e.courseInstanceId AS courseInstanceId,
    t.evaluationId AS evaluationId,
    tr.studentId,
    SUM(tr.studentResult) AS total,
    -- COUNT(t.id) AS taskCount,
    -- (SELECT id FROM task WHERE task.evaluationId = t.evaluationId ORDER BY displayOrder ASC LIMIT 1) AS taskId,
    CASE
        WHEN COUNT(t.id) = 1 THEN (
            SELECT
                id
            FROM
                task
            WHERE
                task.evaluationId = t.evaluationId
            ORDER BY
                displayOrder ASC
            LIMIT
                1
        )
        ELSE '0'
    END as taskId
FROM
    taskResult AS tr
    JOIN task AS t ON tr.taskId = t.id
    JOIN evaluation AS e ON t.evaluationId = e.id
GROUP BY
    courseInstanceId,
    evaluationId,
    studentId;

select
    *
from
    task;

SELECT
    *
FROM
    v_instanceStudents
WHERE
    courseInstanceId = 1;

SELECT
    *
FROM
    taskResult;

SELECT
    studentId,
    SUM(taskResult.studentResult) AS total
FROM
    taskResult
    JOIN task ON taskResult.taskId = task.id
    JOIN evaluation ON task.evaluationId = evaluation.id
WHERE
    evaluation.id = 2
GROUP BY
    studentId;

SELECT
    courseInstanceId,
    evaluationId,
    studentId,
    total
FROM
    v_task_result_summary
WHERE
    courseInstanceId = 1;

-- ========================================================================
-- Query result
SELECT
    evaluation.courseInstanceId AS courseInstanceId,
    task.evaluationId AS evaluationId,
    taskResult.studentId,
    SUM(taskResult.studentResult) AS total
FROM
    taskResult
    JOIN task ON taskResult.taskId = task.id
    JOIN evaluation ON task.evaluationId = evaluation.id
GROUP BY
    taskResult.studentId,
    task.evaluationId;

SELECT
    taskResult.studentId,
    task.title,
    taskResult.studentResult
FROM
    taskResult
    JOIN task ON taskResult.taskId = task.id;

-- ========================================================================
-- Course
-- ========================================================================
INSERT INTO
    courseTemplate (title, contents, displayOrder, folder)
VALUES
    (
        'programmation-web',
        '[]',
        10,
        'C:/dev/vsc-workspace/programmation-web'
    );

INSERT INTO
    courseTemplate (title, contents, displayOrder, folder)
VALUES
    (
        'programmation-web-1',
        '[]',
        20,
        'C:/dev/vsc-workspace/programmation-web-1'
    );

INSERT INTO
    courseTemplate (title, contents, displayOrder, folder)
VALUES
    (
        'programmation-web-2',
        '[]',
        30,
        'C:/dev/vsc-workspace/programmation-web-2'
    );

INSERT INTO
    courseTemplate (title, contents, displayOrder, folder)
VALUES
    (
        'structure-logicielle',
        '[]',
        40,
        'C:/dev/vsc-workspace/structure-logicielle'
    );

INSERT INTO
    courseTemplate (title, contents, displayOrder, folder)
VALUES
    (
        'programmation-web-3',
        '[]',
        50,
        'C:/dev/vsc-workspace/programmation-web-3'
    );

INSERT INTO
    courseInstance (
        title,
        contents,
        folder,
        courseTemplateId,
        startDate,
        endDate
    )
VALUES
    (
        'programmation-web_e20',
        '[]',
        'C:/dev/vsc-workspace/programmation-web_e20',
        1,
        '2019-12-13',
        '2019-12-13'
    );

-- ========================================================================
-- Student
-- ========================================================================
INSERT INTO
    student (firstName, lastName, permanentCode, email)
VALUES
    ('Martin', 'Vachon', 666, 'mvachon@isi-mtl.com');

INSERT INTO
    student (firstName, lastName, permanentCode, email)
VALUES
    ('Melanie', 'Simard', 999, 'msimard@isi-mtl.com');

-- ========================================================================
-- Absence
-- ========================================================================
INSERT INTO
    absenceType (label, absenceMinute)
VALUES
    ('Absent-4', 4),
    ('Delay', 1),
    ('Departure', 1);

INSERT INTO
    absence (
        studentId,
        courseInstanceId,
        absenceTypeId
    )
VALUES
    (1, 1, 1),
    (2, 1, 2),
    (2, 1, 3);

INSERT INTO
    courseInstanceStudent (courseInstanceId, studentId)
VALUES
    (1, 1);

INSERT INTO
    courseInstanceStudent (courseInstanceId, studentId)
VALUES
    (1, 2);


-- View

SELECT a.id, a.courseInstanceId, a.studentId, a.absenceDate, t.label, t.absenceMinute
FROM absence AS a
JOIN absenceType AS t ON a.absenceTypeId = t.id
WHERE a.courseInstanceId = 1;


SELECT a.courseInstanceId, a.studentId, a.absenceDate, SUM(t.absenceMinute)
FROM absence AS a
JOIN absenceType AS t ON a.absenceTypeId = t.id
WHERE a.courseInstanceId = 1 AND a.absenceDate = 1645975887
GROUP BY studentId;

-- ========================================================================
-- Evaluation
-- ========================================================================
INSERT INTO
    evaluationCategory (title)
VALUES
    ('Examens');

INSERT INTO
    evaluationCategory (title)
VALUES
    ('Laboratoires');

INSERT INTO
    evaluation (
        title,
        courseInstanceId,
        evaluationCategoryId,
        total,
        displayOrder
    )
VALUES
    ('Quiz 1', 1, 1, 15, 0);

INSERT INTO
    task (title, total, evaluationId, displayOrder)
VALUES
    ('Résultat', 15, 1, 0);

INSERT INTO
    evaluation (
        title,
        courseInstanceId,
        evaluationCategoryId,
        total,
        displayOrder
    )
VALUES
    ('Travail pratique', 1, 2, 25, 1);

INSERT INTO
    task (title, total, evaluationId, displayOrder)
VALUES
    ('Ouverture du formulaire', 5, 2, 0);

INSERT INTO
    task (title, total, evaluationId, displayOrder)
VALUES
    ('Sauvegarde du formulaire', 5, 2, 10);

INSERT INTO
    task (title, total, evaluationId, displayOrder)
VALUES
    ('Affichage du résultat', 5, 2, 20);

INSERT INTO
    taskResult (taskId, studentId, studentResult)
VALUES
    (2, 1, 2.5);

INSERT INTO
    taskResult (taskId, studentId, studentResult)
VALUES
    (3, 1, 2.6);

INSERT INTO
    taskResult (taskId, studentId, studentResult)
VALUES
    (4, 1, 2.7);

INSERT INTO
    taskResult (taskId, studentId, studentResult)
VALUES
    (2, 2, 5);

INSERT INTO
    taskResult (taskId, studentId, studentResult)
VALUES
    (3, 2, 4);

INSERT INTO
    taskResult (taskId, studentId, studentResult)
VALUES
    (4, 2, 3.5);

INSERT INTO
    taskResult (taskId, studentId, studentResult)
VALUES
    (1, 1, 1.4);

INSERT INTO
    taskResult (taskId, studentId, studentResult)
VALUES
    (1, 2, 1.5);

-- ========================================
-- UPSERT
-- ========================================
DELETE FROM
    taskResult
WHERE
    taskId = 3
    AND studentId = 1;

INSERT INTO
    taskResult (taskId, studentId, studentResult)
VALUES
    (3, 1, 6) ON CONFLICT (taskId, studentId) DO
UPDATE
SET
    studentResult = 6;

SELECT
    *
FROM
    taskResult;