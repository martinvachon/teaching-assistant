const jsdom = require('jsdom')
const { JSDOM } = jsdom

const file = 'C:\\dev\\vsc-workspace\\teaching-assistant\\dist\\out\\fr\\index.xml'

// Node.TEXT_NODE
const TEXT_NODE = 3

const options = {

}

const fetch = require('node-fetch')

async function requestTranslate (text) {
    const res = await fetch('http://localhost:5000/translate', {
        method: 'POST',
        body: JSON.stringify({
            q: text,
            source: 'fr',
            target: 'en'
        }),
        headers: { 'Content-Type': 'application/json' }
    })

    const result = await res.json()
    return result.translatedText
}

async function translate (dom, tags) {
    for (let i = 0; i < tags.length; i++) {
        const tag = tags[i]
        console.log('Translating tag:', tag)
        const elements = dom.window.document.getElementsByTagName(tag)
        for (const element of elements) {
            const childs = element.childNodes
            for (const child of childs) {
                if (child.nodeType === TEXT_NODE) {
                    child.textContent = await requestTranslate(child.textContent)
                }
            }
        }
    }

    console.log(dom.serialize())
}

JSDOM.fromFile(file, options).then(async function (dom) {
    await translate(dom, ['p', 'h1', 'h2', 'h3', 'h4', 'li'])
})
