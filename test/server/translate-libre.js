const fetch = require('node-fetch')

async function test () {
    const res = await fetch('http://localhost:5000/translate', {
        method: 'POST',
        body: JSON.stringify({
            q: 'Langage de programmation fonctionnelle orienté objet, créer par la compagnie',
            source: 'fr',
            target: 'en'
        }),
        headers: { 'Content-Type': 'application/json' }
    })

    console.log(await res.json())
}

test()
