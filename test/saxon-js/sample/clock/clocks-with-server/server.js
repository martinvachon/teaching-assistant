var http = require('http'); // Import Node.js core module
var fs = require('fs');
var static = require('node-static');
var staticServer = new (static.Server)();
var SaxonJS = require('saxon-js');

try {
  fs.readFileSync("clocks/make-clock-data.sef.json");
} catch (e) {
  console.error("Stylesheet not found: check that the current directory is clocks-with-server");
  process.exit(2);
}

var server = http.createServer(function (req, res) {   //create web server
        console.log("Processing request: " + req.url);
        if (req.url == "/clocks/tz") {
            console.log("Doing transformation...");
            SaxonJS.transform({
                stylesheetFileName: "clocks/make-clock-data.sef.json",
                sourceFileName: "clocks/timezone-data.xml",
                destination: "serialized"
            }, "async")
            .then (result => {
                console.log("Writing transformation result...");
                res.writeHead(200, {'Content-Type': 'application/xml'});
                res.write(result.principalResult);
                res.end();
            }).catch(err => {
                console.log(err.getMessage());
                res.writeHead(200, {'Content-Type': 'application/xml'});
                res.write("Transformation failed: " + err.getMessage());
                res.end();
            });
        } else {
            staticServer.serve(req, res);
        }


    });

server.listen(5000); 

console.log('Node.js web server at port 5000 is running..')