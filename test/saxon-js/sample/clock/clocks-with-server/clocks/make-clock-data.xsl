<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:f="http://ns.saxonica.com/clocks"
  exclude-result-prefixes="xs f"
  version="3.0">
  
  <!-- This stylesheet is designed to take the document timezone-data.xml as input. It outputs 
       a document containing the names of cities, together with the current offset of the
       local time in that city from UTC, taking daylight savings into account -->
  
  <xsl:variable name="dst-calendar" as="element(region)*" select="/timezones/daylight-saving/region"/>

  <xsl:variable name="locations" as="element(city)*" select="/timezones/cities/city"/>
  
  <xsl:function name="f:offset" as="xs:dayTimeDuration">
    <xsl:param name="city" as="xs:string"/>
    <xsl:variable name="cityElem" select="($locations[@name=$city], $locations[@name='London'])[1]"/>
    <xsl:variable name="now" select="current-dateTime()"/>
    <xsl:variable name="standard-offset" select="xs:dayTimeDuration($cityElem!@tz)"/>   
    <xsl:variable name="adjusted" select="adjust-dateTime-to-timezone($now, $standard-offset)"/>
    <xsl:variable name="dst-data" select="$dst-calendar[@name=$cityElem!@region]!year[@yr=year-from-dateTime($now)]"/>
    <xsl:variable name="is-DST" select="$dst-data ! (adjust-dateTime-to-timezone(@start, $standard-offset) lt $now 
      and adjust-dateTime-to-timezone(@end, $standard-offset) gt $now)"/>
    <xsl:sequence select="if ($is-DST) 
      then $standard-offset + xs:dayTimeDuration($dst-data!@offset)
      else $standard-offset"/>
  </xsl:function>
  
  <xsl:template match="/">
    <clocks>
      <xsl:for-each select="$locations ! @name">
        <clock location="{.}" offset="{f:offset(.)}"/>
      </xsl:for-each>
    </clocks>   
  </xsl:template>
</xsl:stylesheet>