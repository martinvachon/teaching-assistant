<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math" xmlns:map="http://www.w3.org/2005/xpath-functions/map"
    xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:saxon="http://saxon.sf.net/"
    xmlns:ixsl="http://saxonica.com/ns/interactiveXSLT" xmlns:doc="http://www.saxonica.com/ns/documentation" extension-element-prefixes="ixsl"
    exclude-result-prefixes="xs math svg ixsl saxon xlink doc" version="3.0">

    <doc:doc xmlns="http://www.w3.org/1999/xhtml">
        <h1>Clocks demonstration in XSLT</h1>
        <p>This code builds a number of analogue clocks displaying the time for several locations in a browser page</p>
    </doc:doc>


 <!-- <clocks>
    <clock location="London" offset="PT1H"/>
    <clock location="Paris" offset="PT2H"/>
    <clock location="Athens" offset="PT3H"/>
    <clock location="New York" offset="-PT4H"/>
    <clock location="San Francisco" offset="-PT7H"/>
    <clock location="Delhi" offset="PT5H30M"/>
    <clock location="Tokyo" offset="PT9H"/>
  </clocks>-->
  
    <xsl:template match="/">
      <ixsl:schedule-action document="tz">
        <xsl:call-template name="go"/>
      </ixsl:schedule-action>
    </xsl:template>

    <xsl:template name="go">
      <xsl:variable name="clock-data" select="document('tz')"/><!-- as="document-node(element(clocks))"-->
      <!--<xsl:variable name="clock-displays" as="map(*)">
        <xsl:map>
          <xsl:for-each select="$clock-data/clocks/clock">
            <xsl:map-entry key="string(@location)">
              <xsl:apply-templates select="." mode="make-clock"/>
            </xsl:map-entry>
          </xsl:for-each>
        </xsl:map>
      </xsl:variable>-->
        <xsl:result-document href="#main" method="ixsl:replace-content" expand-text="true">
            <table>
                <tbody>
                    <tr>
                        <xsl:variable name="locations" select="$clock-data/clocks/clock/@location" as="xs:string*"/>
                        <xsl:for-each select="subsequence($locations, 1, 3)">
                            <xsl:variable name="loc" select="."/>
                            <td>
                                <div>
                                    <select class="city">
                                        <xsl:for-each select="$locations">
                                            <option><xsl:if test=". eq $loc"><xsl:attribute name="selected"/></xsl:if>{.}</option>
                                        </xsl:for-each>
                                    </select>
                                    <div class="container">
                                        <xsl:apply-templates select="$clock-data/clocks/clock[@location eq current()]" mode="make-clock"/>
                                    </div>
                                </div>
                            </td>
                        </xsl:for-each>
                      </tr>
                </tbody>
            </table>            
        </xsl:result-document>
        <xsl:call-template name="setClocks" doc:doc="Now set the clocks to the current time">
          <xsl:with-param name="clock-data" select="$clock-data" tunnel="yes"/>
        </xsl:call-template>
    </xsl:template>

    <xsl:template name="setClocks">
        <xsl:apply-templates select="ixsl:page()//svg:*[contains-token(@class, 'clock')]" mode="setClock"
            doc:doc="Find every clock, each of which has a  'clock' class token, and set its time">
            <xsl:with-param name="now" select="saxon:timestamp()"
                doc:doc="This is the date/time now. Note current-dateTime() is deterministic and merely is some time within the current exectuion, which won't help"
            />
        </xsl:apply-templates>
        <ixsl:schedule-action wait="1000">
            <xsl:call-template name="setClocks"/>
        </ixsl:schedule-action>
    </xsl:template>

    <xsl:template match="select[@class = 'city']" mode="ixsl:onchange" expand-text="true" 
      doc:doc="Respond to the drop-down city selector">
        <xsl:variable name="city" select="ixsl:get(., 'value')"/>
        <xsl:message>Clicked {$city}</xsl:message>
        <xsl:variable name="clock-data" select="document('tz')"/>
        <xsl:for-each select="../div[contains-token(@class, 'container')]">
            <xsl:result-document href="?." method="ixsl:replace-content">
                <xsl:apply-templates select="$clock-data/clocks/clock[@location eq $city]" mode="make-clock"/>
            </xsl:result-document>
        </xsl:for-each>
    </xsl:template>

    <xsl:variable name="text-components" select="'year', 'month', 'day', 'weekday', 'hour', 'minute', 'second'"
        doc:doc="Possible components that can be displayed as text"/>

    <xsl:mode name="setClock" on-no-match="deep-skip" doc:doc="Set the time/date for parts of a clock"/>
  
    <xsl:template match="svg:*[contains-token(@class, 'clock')]" mode="setClock" doc:doc="Setting the time for a single clock">
        <xsl:param name="now" as="xs:dateTime"/>
      <xsl:param name="clock-data"  tunnel="yes"/><!--as="document-node(element(clocks))"-->
        <xsl:variable name="location" select="normalize-space(.//*[@name eq 'city'])"
            doc:doc="Find the location, which has been stored as a field with @name='city'"/>
        <xsl:variable name="nowZoned"
            select="
                if ($location eq 'Local Time') then
                    $now
                else
                    adjust-dateTime-to-timezone($now, xs:dayTimeDuration($clock-data/clocks/clock[@location=$location]/@offset))"
            doc:doc="Adjust the dateTime to that of the timezone of the given location"/>
        <xsl:variable name="time" select="xs:time($nowZoned)"/>
        <xsl:variable name="date" select="xs:date($nowZoned)"/>
        <xsl:variable name="components"
            doc:doc="Generate a map of the values (number or text) of the components of the dateTime for this clock's location"
            select="map{
                'year': year-from-date($date),
                'month' : format-dateTime($nowZoned,'[MNn,*-3]'),
                'day': day-from-date($date),
                'weekday': format-dateTime($nowZoned,'[FNn,*-3]'),
                'hour': hours-from-time($time),
                'minute': minutes-from-time($time),
                'second': seconds-from-time($time)}"/>
        <xsl:variable name="rotations" as="map(*)" doc:doc="Calculate rotational angles in degrees for clock hands"
            select="map{'hour' : 30 * $components?hour + $components?minute div 2, 'minute' : 6 * $components?minute + $components?second div 10, 'second' : 6 * $components?second}"/>
        <xsl:apply-templates select=".//svg:line[@class], .//svg:path[@class], .//svg:tspan[@name = $text-components]" mode="#current"
            doc:doc="Now set the time for those components of the current clock that alter as time passes">
            <xsl:with-param name="rotations" select="$rotations" tunnel="true"/>
            <xsl:with-param name="components" select="$components" tunnel="true"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="svg:line[@class = ('hour', 'minute', 'second')] | svg:path[@class = ('hour', 'minute', 'second')]" mode="setClock"
        doc:doc="Rotate a clock hand to the required angle for the component displayed">
        <xsl:param name="rotations" as="map(*)" select="map{}" tunnel="true"/>
        <ixsl:set-attribute name="transform" select="'rotate(' || $rotations(@class) || ')'"/>
    </xsl:template>

    <xsl:template match="svg:tspan[@name = $text-components]" mode="setClock"
        doc:doc="Set a text display to the appropriate value for the component displayed">
        <xsl:param name="components" as="map(*)" select="map{}" tunnel="true"/>
        <xsl:result-document href="?." method="ixsl:replace-content">
            <xsl:value-of
                select="
                    let $v := $components(@name)
                    return
                        if ($v castable as xs:numeric) then
                            format-number($v, '00')
                        else
                            $v"
            />
        </xsl:result-document>
    </xsl:template>

    <xsl:variable name="hand-sizes" select="map{'hour':25,'minute':35,'second':40}" 
      doc:doc="The sizes (length) for the various clock hands"/>

    <xsl:template match="clock" expand-text="true" mode="make-clock" doc:doc="Draw up the clock according to the specification">
        <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 100 100" preserveAspectRatio="xMinYMin" width="100%"
            xmlns:xlink="http://www.w3.org/1999/xlink">
            <symbol id="mins5" overflow="visible">
                <line class="hourTick tick" x1="45" x2="50" transform="rotate(0)"/>
                <xsl:for-each select="1 to 4">
                    <line class="tick" x1="48" x2="50" transform="rotate({6 * .})"/>
                </xsl:for-each>
            </symbol>
            <symbol id="clockFace" viewBox="0 0 100 100">
                <rect width="100" height="100" fill="grey"/>
                <circle cx="50" cy="50" r="50" fill="white" stroke="black"/>
                <g class="ticks" transform="translate(50,50)">
                    <xsl:for-each select="0 to 11">
                        <use xlink:href="#mins5" transform="rotate({30 * .})"/>
                    </xsl:for-each>
                </g>
            </symbol>
            <xsl:variable name="hands" select="tokenize((@hands, 'hour minute second')[1])"
                doc:doc="The hands required can be defined in a space-separated attribute"/>
            <!--<xsl:variable name="date-parts" select="tokenize((@components, 'weekday day month hour minute second')[1])"
                doc:doc="Text displays required can be defined in a space-separated attribute"/>-->
            <g class="small clock">
                <use xlink:href="#clockFace"/>
                <text class="label" name="city" x="50" y="20" text-anchor="middle">{@location}</text>
                <text class="digital" x="50" y="30" text-anchor="middle">
                  <tspan name="weekday"/>
                  <tspan><xsl:text> </xsl:text></tspan>
                  <tspan name="day"/>
                  <tspan><xsl:text> </xsl:text></tspan>
                  <tspan name="month"/>
                  <tspan><xsl:text> </xsl:text></tspan>
                  <tspan name="hour"/>
                  <tspan><xsl:text>:</xsl:text></tspan>
                  <tspan name="minute"/>
                  <tspan><xsl:text>:</xsl:text></tspan>
                  <tspan name="second"/>
                    <!--<xsl:for-each select="$date-parts">
                        <tspan name="{.}">{.}</tspan>
                        <tspan>
                            <xsl:text> </xsl:text>
                        </tspan>
                    </xsl:for-each>-->
                </text>
                <g transform="translate(50,50)">
                    <xsl:for-each select="$hands">
                        <line class="{.}" y2="-{$hand-sizes(.)}"/>
                    </xsl:for-each>
                </g>
            </g>
        </svg>
    </xsl:template>

    

</xsl:stylesheet>
