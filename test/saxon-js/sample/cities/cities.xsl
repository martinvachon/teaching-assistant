<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    xmlns:ixsl="http://saxonica.com/ns/interactiveXSLT"
    extension-element-prefixes="ixsl"
    expand-text="yes"
    version="3.0" >
    
    <!-- 
    XSLT stylesheet to display US cities data contained in JSON files. 
        
    Authors: Michael Kay, Debbie Lockett
    Date: July 2016
    Modified: May 2020 to use asynchronous loading of JSON files.
        
    Designed to run interactively in the browser using Saxon-JS.
    
    Uses XPath 3.1 maps and arrays, and functions for processing JSON.
    
    Original sample datasets were obtained from http://openweathermap.org. There was some server
    side preparation of these source files (using XSLT) - firstly to extract cities in the US, and
    then to split into separate files by initial letter of the city name. Together these JSON files 
    contain nearly 2MB of data. 
    -->
        
    <!-- $baseURL - URL for the directory containing the JSON source files -->
    <xsl:variable name="baseURL" select="resolve-uri('src/', ixsl:location())"/>

    <xsl:template name="main">
        <!-- At initialization, build the index list, by updating the contents of the element 
            of the HTML page which has id="index". 
            The contents is a list of span elements which will act as links (see the
            event handler template for clicks on these span elements below; note also
            the use of "cursor: pointer" in the CSS). -->
        <xsl:result-document href="#index" method="ixsl:replace-content">
            <ul class="index">
                <xsl:for-each
                    select="('6','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z')">
                    <li><span class="link" id="{.}">{.}</span></li>
                </xsl:for-each>
            </ul>
        </xsl:result-document>
        
        <xsl:call-template name="loadJSON"/>
    </xsl:template>
    
    <xsl:template name="loadJSON">
        <!-- $X - some letter of the alphabet, either one which has been selected from the index,
                    or default 'A' -->
        <xsl:param name="X" select="'A'"/>
        
        <!-- Use ixsl:schedule-action/@document to asynchonously load the selected source JSON file. 
            Note the use of the XPath 3.0 string concatenation operator "||". -->
        <ixsl:schedule-action document="{$baseURL||$X||'.json'}">
            <xsl:call-template name="show-all">
                <xsl:with-param name="X" select="$X"/>
            </xsl:call-template>
        </ixsl:schedule-action>
    </xsl:template>

    <xsl:template name="show-all">
        <xsl:param name="X"/>
        
        <!-- Update the page title. 
            Note the use of XSLT 3.0 text value templates, allowed by the expand-text="yes" attribute on
            the xsl:stylesheet element. -->
        <xsl:result-document href="#title" method="ixsl:replace-content">
            A List of Cities Beginning With {$X}
        </xsl:result-document>
        
        <!-- Update the contents of the element of the HTML page which has id="target". 
            The contents is a table whose entries are taken from the source JSON file. -->
        <xsl:result-document href="#target" method="ixsl:replace-content">
            <table id="cities-table">
                <thead>
                    <tr>
                        <th>City</th>
                        <th>Country</th>
                        <th data-type="number">Longitude</th>
                        <th data-type="number">Latitude</th>
                    </tr>
                </thead>
                <tbody>
                    <!-- The XPath 3.1 function json-doc() is used to read the selected source JSON
                        file; which has been asynchronously preloaded using ixsl:schedule-action.
                        Each file consists of a JSON array of objects containing the cities data
                        (one object per city). The json-doc() function returns the data in an XDM
                        array of XDM maps. One row is added to the table for each city. -->
                    <!-- Note also the use of the XPath 3.0 string concatenation operator "||"; the
                        XPath 3.1 lookup operator "?" for arrays and maps; and the use of XSLT 3.0
                        text value templates. -->
                    <xsl:for-each select="json-doc($baseURL||$X||'.json')?*">
                        <tr>
                            <td>{?name}</td>
                            <td>{?country}</td>
                            <td>{?coord?lon}</td>
                            <td>{?coord?lat}</td>
                        </tr>
                    </xsl:for-each>
                </tbody>
            </table>
        </xsl:result-document>
    </xsl:template>
    
    <!-- Event handler template rule to navigate between lists when a user clicks on an index link. -->
    <xsl:template match="span[@class='link']" mode="ixsl:onclick">
        <xsl:call-template name="loadJSON">
            <xsl:with-param name="X" select="string(@id)"/>
        </xsl:call-template>
    </xsl:template>
    
    
    <!-- Event handler template rule to sort the contents of the table when a user clicks on a
        column heading. -->
    <xsl:template match="th" mode="ixsl:onclick">
        <!-- $colNr - the column which has been clicked (numbered from the left) -->
        <xsl:variable name="colNr" as="xs:integer" select="count(preceding-sibling::th)+1"/>  
        
        <xsl:apply-templates select="ancestor::table[1]" mode="sort">
            <xsl:with-param name="colNr" select="$colNr"/>
            <xsl:with-param name="dataType" select="(@data-type,'text')[1]"/>
            <xsl:with-param name="ascending" select="not(../../@data-order=$colNr)"/>
        </xsl:apply-templates>
    </xsl:template>
    
    <!-- Note the use of user-defined attributes data-order and data-type to retain state
        information in the HTML tree, so that the sorting process can decide whether to sort
        ascending or descending, and whether to sort alphabetically or numerically. -->
    
    <xsl:template match="table" mode="sort">
        <xsl:param name="colNr" as="xs:integer" required="yes"/>
        <xsl:param name="dataType" as="xs:string" required="yes"/>
        <xsl:param name="ascending" as="xs:boolean" required="yes"/>
        
        <!-- Update the contents of the id="cities-table" table element of the HTML page. -->
        <xsl:result-document href="#cities-table" method="ixsl:replace-content">
            <thead data-order="{if ($ascending) then $colNr else -$colNr}">
                <xsl:copy-of select="thead/tr"/>
            </thead>
            <tbody>
                <xsl:perform-sort select="tbody/tr">
                    <xsl:sort select="td[$colNr]" 
                        data-type="{$dataType}" 
                        order="{if ($ascending) then 'ascending' else 'descending'}"/>
                </xsl:perform-sort>
            </tbody>
        </xsl:result-document>
    </xsl:template>

</xsl:stylesheet>
