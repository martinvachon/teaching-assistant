<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs" version="2.0">

    <!--
        Solution found here
        https://stackoverflow.com/questions/18923464/using-xsl-how-to-keep-entities-in-hex-to-the-output
     -->
    <xsl:character-map name="cm">
        <xsl:output-character character="&lt;" string="&amp;lt;" />
        <xsl:output-character character="&gt;" string="&amp;gt;" />
    </xsl:character-map>

    <xsl:output use-character-maps="cm" />

    <xsl:template match="/">
        <html lang="fr">
            <head>
                <title>Test</title>
            </head>
            <body>
                <h1>test</h1>

                <xsl:apply-templates />
            </body>
        </html>
    </xsl:template>

    <xsl:template match="items">
        <ul>
            <xsl:apply-templates />
        </ul>
    </xsl:template>

    <xsl:template match="item">
        <li>
            <xsl:value-of select="." />
        </li>
    </xsl:template>

</xsl:transform>