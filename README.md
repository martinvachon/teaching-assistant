# teaching-assistant (beta)

## Command line and web application for course management.

[Release notes](https://bitbucket.org/martinvachon/teaching-assistant/src/master/md/release.md)

The [web application](https://bitbucket.org/martinvachon/teaching-assistant/src/master/md/webapp.md) is incomplete.

## Installation / Uninstallation

Make sure your system already have a recent version of [Node.js](https://nodejs.org/en/download/) installed. The command **node -v** return your currrently installed version.

Installation
```
npm install -g teaching-assistant
```

Uninstallation
```
npm uninstall -g teaching-assistant
```

## Usage

> ### To create a new course:

Open a shell in an empty folder and execute:

```
ta create
```
A complete course will be initialized and ready to use.
The final distributable file is **/dist/html.zip** and the folder **/dist/out/** contain the same files before packaging.

> ### To edit an existing course:

```
ta edit
```
After each file modification inside the **/doc/** folder, the corresponding files inside the **/dist/out/** will be update.

Note: CTRL-C will stop the watch process.

> ### To distribute a course:

```
ta dist
```
The file **/dist/html.zip** contain all courses languages.

> ### To translate a course:

```
ta translate
```
The folder **/doc/en** will contain the translated course and should not exist before executing the command.

The translation executes request by default to a local instance of this service: [LibreTranslate](https://github.com/LibreTranslate/LibreTranslate)

Use the **url** parameter to change the default server, as an example:

```
ta translate --url=http://192.168.4.182:5000/translate
```
