import React from 'react'

import {
    withRouter,
    RouteComponentProps
} from 'react-router-dom'

import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Image from 'react-bootstrap/Image'
import Dropdown from 'react-bootstrap/Dropdown'

import Player from 'player/player'

import 'player/style/player/player.css'

interface ViewerProps {
    localIndex?: number
    offlineKey?: string
}

interface ViewerState {
}

class Viewer extends React.Component<RouteComponentProps & ViewerProps, ViewerState> {

    constructor(props: RouteComponentProps & ViewerProps) {
        super(props)

        this.state = {

        }

    }

    renderPlayer() {
        // See webpack-dev-server devServer.contentBase
        let indexFile = 'index.xml'
        let courseFolder = '/doc/'
        let moodlePlayerFolder = ''

        console.log('courseFolder', courseFolder)
        console.log('indexFile', indexFile)
        console.log('moodlePlayerFolder', moodlePlayerFolder)

        return <Player moodlePlayerFolder={moodlePlayerFolder} courseFolder={courseFolder} indexFile={indexFile} />
    }

    render() {

        return (
            <div id='courseTODO'>

                {this.renderPlayer()}

            </div>
        )

    }
}

export default withRouter(Viewer)
