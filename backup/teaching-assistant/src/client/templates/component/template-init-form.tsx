import React from 'react'

import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'

import { FormRow } from 'client/component/form-row'
import { Init } from 'common/init'

interface TemplateInitFormProps {
    showForm: boolean
    handleCancel: any
    handleOpen: any
    handleChange: (event: React.ChangeEvent<HTMLInputElement>) => void
    init: Init
}

export const TemplateInitForm = (props: TemplateInitFormProps) => (
    <Modal show={props.showForm} onHide={props.handleCancel} >
        <Modal.Header closeButton>
            <Modal.Title>Open Course Template</Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <Form>
                <p>Import the course template from the file: teaching-assistant.json. <br />If the file is not found, a new course template is create.</p>
                <FormRow id='folder' label='Folder:'>
                    <Form.Control type='text' placeholder='drive:/path/to/course' value={props.init.folder} onChange={props.handleChange} />
                </FormRow>
            </Form>
        </Modal.Body>
        <Modal.Footer>
            <Button variant='secondary' onClick={props.handleCancel}>Cancel</Button>
            <Button variant='secondary' onClick={props.handleOpen}>Open</Button>
        </Modal.Footer>
    </Modal>
)
