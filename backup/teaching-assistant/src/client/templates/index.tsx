import React from 'react'
import {
    withRouter,
    RouteComponentProps,
    Switch,
    Route
} from 'react-router-dom'

import { CourseTemplateDTO, CourseInstanceDTO } from 'common/course'

import { TemplateList } from './component/template-list'
import { TemplateForm } from './component/template-form'
import { InstanceForm } from './component/instance-form'
import { TemplateInitForm } from './component/template-init-form'

import Viewer from './viewer'
import InstancesContainer from './instances'
import { Api } from 'client/service/api'

import { Init } from 'common/init'
import { Content } from 'common/content'

// const logoImage = require('../style/image/logo.png')

const CONTENTS = [
    {
        'id': 10,
        'name': 'Exercice',
        'type': 1,
        'value': 10
    },
    {
        'id': 20,
        'name': 'Quiz 1',
        'type': 2,
        'value': 15
    },
    {
        'id': 30,
        'name': 'Quiz 2',
        'type': 2,
        'value': 15
    },
    {
        'id': 40,
        'name': 'Quiz 3',
        'type': 2,
        'value': 15
    },
    {
        'id': 50,
        'name': 'Travail pratique',
        'type': 3,
        'value': 25
    },
    {
        'id': 60,
        'name': 'Examain pratique',
        'type': 4,
        'value': 20
    }
]

interface TemplatesContainerProps {
    api: Api
    showInit: boolean
    handleCloseInit: () => void
}

interface TemplatesContainerState {
    courseTemplateDTOs: CourseTemplateDTO[]
    courseTemplateDTO: CourseTemplateDTO
    courseInstanceDTO: CourseInstanceDTO
    isInstanceFormShow: boolean
    isTemplateFormShow: boolean
    init: Init
}

const COURSE_TEMPLATE_DTO_STATE = {
    id: 0,
    name: '',
    folder: '',
    displayOrder: 0,
    contents: CONTENTS,
    courseInstances: []
}

const COURSE_INSTANCE_DTO_STATE = {
    id: 0,
    courseTemplateId: 0,
    name: '',
    folder: '',
    startDate: '',
    endDate: '',
    displayOrder: 0,
    contents: []
}

class TemplatesContainer extends React.Component<RouteComponentProps & TemplatesContainerProps, TemplatesContainerState> {

    constructor(props: RouteComponentProps & TemplatesContainerProps) {
        super(props)

        this.state = {
            courseTemplateDTOs: [],
            courseTemplateDTO: COURSE_TEMPLATE_DTO_STATE,
            courseInstanceDTO: COURSE_INSTANCE_DTO_STATE,
            isInstanceFormShow: false,
            isTemplateFormShow: false,
            init: {
                folder: ''
            }
        }

        this.exportInstanceHandler = this.exportInstanceHandler.bind(this)
    }

    componentDidMount() {
        this.props.api.get('/templates', courseTemplateDTOs => {
            this.setState({
                courseTemplateDTOs: courseTemplateDTOs
            })
        })
    }

    handleEditTemplate = (id: number) => () => {
        this.props.api.get('/templates/' + id, courseTemplateDTO => {
            this.setState({
                courseTemplateDTO: courseTemplateDTO,
                isTemplateFormShow: true
            })
        })
    }

    handleSaveTemplate = () => {
        this.props.api.save('/templates', this.state.courseTemplateDTO, courseTemplateDTOs => {
            this.setState({
                isTemplateFormShow: false,
                courseTemplateDTOs: courseTemplateDTOs
            })
        })
    }

    handleDeleteTemplate = (templateId: number) => () => {
        if (confirm('Delete course template ?')) {
            this.props.api.delete('/templates/' + templateId, courseTemplateDTOs => {
                this.setState({
                    courseTemplateDTOs: courseTemplateDTOs
                })
            })
        }
    }

    handleAddInstance = (template: CourseTemplateDTO) => () => {
        this.setState({
            courseInstanceDTO: {
                ...COURSE_INSTANCE_DTO_STATE,
                courseTemplateId: template.id,
                name: template.name + '_instance',
                folder: template.folder + '_instance',
                contents: template.contents
            },
            isInstanceFormShow: true
        })
    }

    handleEditInstance = (templateId: number, instanceId: number) => () => {
        this.props.api.get('/templates/' + templateId + '/instances/' + instanceId, courseInstanceDTO => {
            this.setState({
                courseInstanceDTO: courseInstanceDTO,
                isInstanceFormShow: true
            })
        })
    }

    handleDeleteInstance = (templateId: number, instanceId: number) => () => {
        if (confirm('Delete course instance ?')) {
            this.props.api.delete('/templates/' + templateId + '/instances/' + instanceId, courseTemplateDTOs => {
                this.setState({
                    courseTemplateDTOs: courseTemplateDTOs
                })
            })
        }
    }

    handleSaveInstance = () => {
        this.props.api.save('/templates/' + this.state.courseInstanceDTO.courseTemplateId + '/instances', this.state.courseInstanceDTO, courseTemplateDTOs => {
            this.setState({
                isInstanceFormShow: false,
                courseTemplateDTOs: courseTemplateDTOs
            })
        })
    }

    handleClose = () => {
        this.setState({
            isTemplateFormShow: false,
            isInstanceFormShow: false
        })
    }

    handleTemplateChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({
            courseTemplateDTO: {
                ...this.state.courseTemplateDTO,
                [event.target.id]: event.target.value
            }
        })
    }

    handleTemplateContentChange = (contentIndex: number) => (event: React.ChangeEvent<HTMLInputElement>) => {
        const contentName = event.target.id
        const contentValue = event.target.value

        this.setState(state => {
            return {
                courseTemplateDTO: {
                    ...state.courseTemplateDTO,
                    contents: state.courseTemplateDTO.contents.map((content, index) => {
                        let contentResult
                        if (contentIndex === index) {
                            contentResult = {
                                ...state.courseTemplateDTO.contents[index],
                                [contentName]: contentValue
                            }
                        } else {
                            contentResult = content
                        }

                        return contentResult
                    })
                }
            }
        })
    }

    handleInstanceChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({
            courseInstanceDTO: {
                ...this.state.courseInstanceDTO,
                [event.target.id]: event.target.value
            }
        })
    }

    exportInstanceHandler(event: React.MouseEvent, instanceId: number) {
        const url = 'http://localhost:8080/instances/' + instanceId + '/export'

        fetch(url, {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.json())
            .then(courseTemplateDTOs => {
                this.setState({
                    courseTemplateDTOs: courseTemplateDTOs
                })
            }
            ).catch(error => {
                throw error
            })
    }

    handleInitChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({
            init: {
                ...this.state.init,
                [event.target.id]: event.target.value
            }
        })
    }

    handleInitOpen = () => {
        this.props.api.post('/templates/init', this.state.init, (courseTemplateDTOs) => {
            this.setState({
                courseTemplateDTOs: courseTemplateDTOs
            })

            this.props.handleCloseInit()
        })
    }

    render() {
        return (
            <div id='templates'>
                <TemplateInitForm
                    init={this.state.init}
                    handleChange={this.handleInitChange}
                    showForm={this.props.showInit}
                    handleCancel={this.props.handleCloseInit}
                    handleOpen={this.handleInitOpen} />

                <TemplateForm
                    template={this.state.courseTemplateDTO}
                    handleChange={this.handleTemplateChange}
                    handleContentChange={this.handleTemplateContentChange}
                    handleSave={this.handleSaveTemplate}
                    handleClose={this.handleClose}
                    showForm={this.state.isTemplateFormShow} />

                <InstanceForm
                    instance={this.state.courseInstanceDTO}
                    handleChange={this.handleInstanceChange}
                    handleSave={this.handleSaveInstance}
                    handleClose={this.handleClose}
                    showForm={this.state.isInstanceFormShow} />

                <section>
                    <Switch>
                        <Route
                            path={this.props.match.url + '/:templateId/instances'}
                            exact={false}
                            render={props => <InstancesContainer
                                api={this.props.api}
                                templateId={props.match.params.templateId}
                                instanceId={props.match.params.instanceId} />} />

                        <Route
                            path={this.props.match.url + '/:templateId/viewer'}
                            exact={false}
                            render={() => <Viewer />} />

                        <Route
                            exact={true}
                            render={() => <TemplateList
                                exportInstanceHandler={this.exportInstanceHandler}
                                handleEditTemplate={this.handleEditTemplate}
                                handleDeleteTemplate={this.handleDeleteTemplate}
                                handleAddInstance={this.handleAddInstance}
                                handleEditInstance={this.handleEditInstance}
                                handleDeleteInstance={this.handleDeleteInstance}
                                courseTemplateDTOs={this.state.courseTemplateDTOs} />} />
                    </Switch>
                </section>
            </div>
        )
    }
}

export default withRouter(TemplatesContainer)
