import React from 'react'
import {
    Route,
    withRouter,
    RouteComponentProps,
    Switch,
    Redirect
} from 'react-router-dom'

import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'

import TemplatesContainer from './templates'
import MainNavbar from './component/main-navbar'
import { Api } from './service/api'
import StudentsContainer from './students'

interface TeachingAssistantProps {

}

interface TeachingAssistantState {
    showInit: boolean
}

class TeachingAssistant extends React.Component<RouteComponentProps<any> & TeachingAssistantProps, TeachingAssistantState> {
    private api: Api

    constructor (props: RouteComponentProps & TeachingAssistantProps) {
        super(props)

        this.state = {
            showInit: false
        }

        this.api = new Api('http://localhost:8080')
    }

    handleInitTemplate = () => {
        this.setState({
            showInit: true
        })
    }

    handleCloseInit = () => {
        this.setState({
            showInit: false
        })
        this.props.history.push('/')
    }

    render () {
        return (
            <Container id='teaching_assistant' fluid>
                <MainNavbar handleInitTemplate={this.handleInitTemplate} />

                <Row>
                    <Col>
                        <Switch>
                            <Route
                                path='/students'
                                render={() => <StudentsContainer api={this.api} />} />

                            <Route
                                path='/templates'
                                exact={false}
                                render={() => <TemplatesContainer showInit={this.state.showInit} handleCloseInit={this.handleCloseInit} api={this.api} />} />

                            <Route
                                path='*'
                                render={() => <Redirect to='/templates' />} />
                        </Switch>
                    </Col>
                </Row>
            </Container>
        )
    }
}

export default withRouter(TeachingAssistant)
