import React, { FunctionComponent } from 'react'

import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button'

interface DialogProps {
    title: string
    body: string
    buttons: Button[]
    isShow: boolean
}

export const Dialog: FunctionComponent<DialogProps> = ({ title, body, buttons, isShow }) => (
    <Modal show={isShow}>
        <Modal.Header closeButton>
            <Modal.Title>{title}</Modal.Title>
        </Modal.Header>

        <Modal.Body>
            <p>{body}</p>
        </Modal.Body>

        <Modal.Footer>
            <Button variant='secondary'>close</Button>
            {buttons.map(button => button)}
        </Modal.Footer>
    </Modal>
)
