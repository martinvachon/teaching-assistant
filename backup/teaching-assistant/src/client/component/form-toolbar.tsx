import React, { FunctionComponent } from 'react'

import { Link } from 'react-router-dom'

import Form from 'react-bootstrap/Form'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import ButtonToolbar from 'react-bootstrap/ButtonToolbar'
import Button from 'react-bootstrap/Button'

interface FormToolbarProps {

}

export const FormToolbar: FunctionComponent<FormToolbarProps> = ({ }) => (
    <ButtonToolbar>
        <Button type='submit' variant='secondary'>Save</Button>
        <Button href='/' variant='secondary'>Cancel</Button>
    </ButtonToolbar>
)
