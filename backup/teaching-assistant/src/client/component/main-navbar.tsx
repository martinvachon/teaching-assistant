import React from 'react'
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import NavDropdown from 'react-bootstrap/NavDropdown'
import NavLink from 'react-bootstrap/NavLink'
import { Link } from 'react-router-dom'

interface MainNavbarProps {
    handleInitTemplate: () => void
}

const MainNavbar = (props: MainNavbarProps) => (
    <Navbar id='main_navbar' collapseOnSelect expand='lg' bg='dark' variant='dark' fixed='top'>
        <Navbar.Brand>
            <Link to='/templates'>
                Teaching Assistant
            </Link>
        </Navbar.Brand>
        <Navbar.Toggle aria-controls='responsive-navbar-nav' />
        <Navbar.Collapse id='responsive-navbar-nav'>
            <Nav className='mr-auto'>

            </Nav>
            <Nav>
                <NavDropdown title='Course' alignRight as={NavLink} id='course_dropdown'>
                    <NavDropdown.Item onClick={props.handleInitTemplate}>Course template</NavDropdown.Item>
                    <NavDropdown.Divider />
                    <Link className='dropdown-item' to='/students'>Student</Link>
                </NavDropdown>
            </Nav>
        </Navbar.Collapse>
    </Navbar>
)

export default MainNavbar
