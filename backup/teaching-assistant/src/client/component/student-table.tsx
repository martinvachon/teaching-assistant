import React from 'react'

import ButtonGroup from 'react-bootstrap/ButtonGroup'
import Table from 'react-bootstrap/Table'

import { StudentDTO } from 'common/student'

interface StudentListProps {
    studentDTOs: StudentDTO[]
    renderRowCommand: (studentDTO: StudentDTO) => JSX.Element
    renderHeadCommand: () => JSX.Element
}

function renderStudent(props: StudentListProps, studentDTO: StudentDTO) {
    return (
        <tr key={studentDTO.id}>
            <td>{studentDTO.firstName}</td>
            <td>{studentDTO.lastName}</td>
            <td>{studentDTO.email}</td>
            <td>{studentDTO.permanentCode}</td>
            <td className='command-column'>
                <ButtonGroup aria-label='Student toolbar' size='sm'>
                    {props.renderRowCommand(studentDTO)}
                </ButtonGroup>
            </td>
        </tr>
    )
}

export const StudentTable = (props: StudentListProps) => (
    <Table striped bordered hover responsive>
        <thead>
            <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Permanent code</th>
                <th className='command-column'>
                    <ButtonGroup aria-label='Add student' size='sm'>
                        {props.renderHeadCommand()}
                    </ButtonGroup>
                </th>
            </tr>
        </thead>
        <tbody>
            {props.studentDTOs.map(studentDTO => renderStudent(props, studentDTO))}
        </tbody>
    </Table>
)
