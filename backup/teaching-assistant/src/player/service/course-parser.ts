import { Course, Article } from '../interface/course'

export function parseCourse(htmlCourse: DocumentFragment): Course {
    const sections = Array.from(htmlCourse.querySelectorAll('section')).map((section: HTMLElement, index: number) => {
        let sectionTitle = 'Section h1 not found'
        let sectionArticles: Article[] = []
        let sectionId = ''

        const sectionTitleElement = section.querySelector('h1 span')
        if (sectionTitleElement) {
            if (sectionTitleElement.textContent) {
                sectionTitle = sectionTitleElement.textContent

                const sectionIdElement = section.querySelector('div:first-child')
                if (sectionIdElement) {
                    sectionId = sectionIdElement.id
                }
            }

            sectionArticles = Array.from(section.querySelectorAll('article')).map((article: HTMLElement) => {
                let articleTitle = 'h* not found'
                let articleSize = 'h2'
                let articleId = ''

                const articleElement = article.querySelector('h2, h3, h4, h5, h6')
                if (articleElement) {
                    if (articleElement.textContent) {
                        articleTitle = articleElement.textContent
                    }
                    articleSize = articleElement.tagName
                    const idElement = article.querySelector('div:first-child')
                    if (idElement) {
                        articleId = idElement.id
                    }
                }

                return {
                    title: articleTitle,
                    id: articleId,
                    size: articleSize.toLowerCase()
                }
            })
        }

        return {
            title: sectionTitle,
            id: sectionId,
            articles: sectionArticles
        }
    })

    return {
        sections: sections
    }
}
