
const XML_MIME_TYPE = 'application/xml'

function loadFile(fileUrl: string, callback: (response: string) => void) {
    fetch(fileUrl)
        .then(response => response.text())
        .then(response => callback(response))
        .catch((error) => { throw error })
}

export function transform(xmlFilePath: string, xsltFilePath: string, parameterMap: Map<string, string>, callback: (result: DocumentFragment) => void) {
    const domParser = new DOMParser()
    const xsltProcessor = new XSLTProcessor()

    parameterMap.forEach((value, key) => xsltProcessor.setParameter('', key, value))

    loadFile(xsltFilePath, (xsltString: string) => {

        xsltProcessor.importStylesheet(
            domParser.parseFromString(xsltString, XML_MIME_TYPE)
        )

        loadFile(xmlFilePath, (xmlString: string) => {

            const result = xsltProcessor.transformToFragment(
                domParser.parseFromString(xmlString, XML_MIME_TYPE),
                document
            )

            callback(result)
        })
    })
}
