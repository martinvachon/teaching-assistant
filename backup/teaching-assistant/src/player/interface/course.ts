
export interface Article {
    title: string
    size: string
    id: string
}

export interface Section {
    title: string
    id: string
    articles: Article[]
}

export interface Course {
    sections: Section[]
}
