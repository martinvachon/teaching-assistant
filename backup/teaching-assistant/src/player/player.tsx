import React from 'react'
import ButtonGroup from 'react-bootstrap/ButtonGroup'
import Button from 'react-bootstrap/Button'
import DropdownButton from 'react-bootstrap/DropdownButton'
import Dropdown from 'react-bootstrap/Dropdown'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons/faArrowLeft'
import { faArrowRight } from '@fortawesome/free-solid-svg-icons/faArrowRight'

import { PlayerMenu } from './component/player-menu'
import { transform } from './service/xslt'
import { parseCourse } from './service/course-parser'
import { Course } from './interface/course'
import { highlightBlock } from 'highlight.js'

interface PlayerProps {
    moodlePlayerFolder: string
    courseFolder: string
    indexFile: string
}

interface PlayerState {
    course?: Course
    sideMenuVisibility: boolean
    level: number
}

export default class Player extends React.Component<PlayerProps, PlayerState> {

    componentRef: React.RefObject<HTMLDivElement>

    constructor(props: PlayerProps) {
        super(props)

        this.componentRef = React.createRef<HTMLDivElement>()

        this.state = {
            course: undefined,
            sideMenuVisibility: true,
            level: 6
        }

        this.processTransform = this.processTransform.bind(this)
        this.toggleSideMenu = this.toggleSideMenu.bind(this)
        this.handleScroll = this.handleScroll.bind(this)
        this.levelOnSelectHandler = this.levelOnSelectHandler.bind(this)
    }

    processCourseInit() {
        const codeNodeList = this.componentRef.current!.querySelectorAll<HTMLElement>('pre > code')
        codeNodeList.forEach(function (codeNode) {
            try {
                const codeArray = codeNode.innerHTML.split('\n')

                // If the CDATA element is not formatted on a single line: ![CDATA[ example ]]
                // And if the CDATA element is formatted on his own line, remove the white space line
                if (codeArray.length > 1 && !codeArray[1].replace(/\s/g, '').length) {
                    codeArray.splice(0, 1)
                    codeArray[0] = codeArray[0].trim()
                }

                const codeText = codeArray.join('\n')

                const pattern = codeText.match(/\s*\n[\t\s]*/)
                codeNode.innerHTML = codeText.replace(new RegExp(pattern!.join('|'), 'g'), '\n')

                highlightBlock(codeNode)
            } catch (error) {
                console.log('Error while parsing and initializing syntaxe highlight:', error)
            }
        })

    }

    processTransform() {
        const parameterMap = new Map()
        parameterMap.set('courseFolder', this.props.courseFolder)

        transform(
            // 'course://programmation-web-2/programmation-web-2.xml',
            // 'course://programmation-web-2/moodle-player/html.xslt',
            this.props.courseFolder + '/' + this.props.indexFile,
            this.props.moodlePlayerFolder + '/html.xslt',
            parameterMap,
            (result) => {

                this.setState({
                    course: parseCourse(result)
                })

                this.componentRef.current!.innerHTML = ''
                this.componentRef.current!.appendChild(result)
                this.processCourseInit()
            }
        )
    }

    componentDidMount() {
        this.processTransform()

        document.addEventListener('scroll', this.handleScroll, { capture: false, passive: true })
    }
    componentWillUnmount() {
        document.removeEventListener('scroll', this.handleScroll)
    }

    componentDidUpdate(prevProps: PlayerProps, prevState: PlayerState) {
        if (this.props.courseFolder !== prevProps.courseFolder) {
            this.processTransform()
        }
    }

    handleScroll() {
        const lastScrollY = window.scrollY
        const links = document.querySelectorAll('#player_menu a')

        links.forEach(link => {
            const href = link.getAttribute('href') || ''
            const section = document.querySelector<HTMLDivElement>('#course ' + href)
            if (section) {
                const offsetTop = section.offsetTop
                const offsetHeight = section.parentElement!.offsetHeight

                if (offsetTop <= lastScrollY && offsetTop + offsetHeight > lastScrollY) {
                    link.classList.add('active')
                } else {
                    link.classList.remove('active')
                }
            }
        })
    }

    toggleSideMenu() {
        this.setState({
            sideMenuVisibility: !this.state.sideMenuVisibility
        })
    }

    // TODO: code cleanup
    levelOnSelectHandler(event: any) {
        this.setState({ level: event })
    }

    render() {
        return (
            <div id='player'>
                <div className={'playerMenuCloseButton ' + (this.state.sideMenuVisibility ? 'd-none' : 'd-block')}>
                    <Button variant='secondary' onClick={this.toggleSideMenu}><FontAwesomeIcon icon={faArrowRight} /></Button>
                </div>
                <div className='row'>
                    <div className={this.state.sideMenuVisibility ? 'col-3' : 'd-none'}>
                        <div className='playerMenuWrapper'>
                            <div className='d-flex flex-column'>
                                <ButtonGroup>
                                    <Button variant='secondary' onClick={this.toggleSideMenu}><FontAwesomeIcon icon={faArrowLeft} /></Button>
                                    <Button variant='secondary'>2</Button>
                                    <DropdownButton onSelect={this.levelOnSelectHandler} variant='secondary' as={ButtonGroup} title={'Level ' + this.state.level} id='bg-nested-dropdown'>
                                        <Dropdown.Item eventKey='1'>Level 1</Dropdown.Item>
                                        <Dropdown.Item eventKey='2'>Level 2</Dropdown.Item>
                                        <Dropdown.Item eventKey='3'>Level 3</Dropdown.Item>
                                        <Dropdown.Item eventKey='4'>Level 4</Dropdown.Item>
                                        <Dropdown.Item eventKey='5'>Level 5</Dropdown.Item>
                                        <Dropdown.Item eventKey='6'>Level 6</Dropdown.Item>
                                    </DropdownButton>
                                </ButtonGroup>
                            </div>
                            <PlayerMenu level={this.state.level} sections={this.state.course ? this.state.course.sections : []} />
                        </div>
                    </div>

                    <div className={this.state.sideMenuVisibility ? 'col-9' : 'col-12'}>
                        <link rel='stylesheet' href={this.props.moodlePlayerFolder + '/style/course/course.css'} />
                        <link rel='stylesheet' href={this.props.moodlePlayerFolder + '/style/course/lib/highlight/styles/ir-black.css'} />
                        <div id='course' className='shadow rounded'>
                            <div ref={this.componentRef} />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
