import React from 'react'

import { Section, Article } from '../interface/course'

interface PlayerMenuProps {
    sections: Section[]
    level: number
}

function renderArticles(articles: Article[], level: number) {
    return articles.filter((article) => filterArticle(article, level)).map((article) => renderLink(article.id, article.title, article.size))
}

function filterArticle(article: Article, level: number) {
    return parseInt(article.size.charAt(1), 10) <= level
}

function renderLink(id: string, title: string, size: string) {
    return <a key={id} className={'list-group-item size-' + size} href={'#' + id}>{title}</a>
}

function renderSections(sections: Section[], level: number) {
    return sections.map((section) => {
        return (
            <div key={section.id} className='list-group'>
                {renderLink(section.id, section.title, 'h1')}
                <div className='list-group'>
                    {renderArticles(section.articles, level)}
                </div>
            </div>
        )
    })
}

export const PlayerMenu = (props: PlayerMenuProps) => (
    <div id='player_menu' className='shadow rounded'>
        <div className='list-group list-group-root'>
            {renderSections(props.sections, props.level)}
        </div>
    </div>
)
