import React from 'react'
import { render } from 'react-dom'
import { BrowserRouter } from 'react-router-dom'

import 'bootstrap/dist/css/bootstrap.min.css'
import './style/player/player.css'
import Player from './player'

/**
 * Default launcher for Player.
 * The course use for development is the Player documentation configured by defaut here.
 */
render(
    <BrowserRouter>
        <Player moodlePlayerFolder='.' courseFolder='../' indexFile='index.xml' />
    </BrowserRouter>,
    document.getElementById('app')
)
