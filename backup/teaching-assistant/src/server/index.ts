import express from 'express'
import fs from 'fs'
import path from 'path'
// import winston, { Logger } from 'winston'
import winston from 'winston'
import morgan from 'morgan'
// import expressWinston from 'express-winston'

const createError = require('http-errors')
const packageJson = require('../../package.json')
const figlet = require('figlet')

import Configuration from 'common/configuration'
import { SqliteDao } from './service/database'
import { templateRouterBuilder, TemplateService } from './template'
import { FileService } from './service/file'
import { StudentService } from './student/service/student-service'
import { studentRouterBuilder } from './student/router'
import { initLogger, LoggerStream, errorMiddleware } from './service/logger'

const CONFIGURATION_FOLDER = '.teaching-assistant'
const CONFIGURATION_FILE = 'teaching-assistant.json'
const DATABASE_FILE = 'teaching-assistant.sqlite3'

/**
 *
 * @param storageFolder Root folder for application configuration and data.
 *
 * @see require('os').homedir()
 */
export function start(storageFolder: string) {
    const configurationFolder = path.join(storageFolder, CONFIGURATION_FOLDER)
    const configuration = loadConfiguration(configurationFolder)
    startServer(configuration, configurationFolder)
}

function loadConfiguration(configurationFolder: string) {
    let configuration: Configuration

    const configurationFile = path.join(configurationFolder, CONFIGURATION_FILE)

    if (fs.existsSync(configurationFile)) {
        // TODO: use constant instead
        configuration = JSON.parse(fs.readFileSync(configurationFile, { encoding: 'utf-8' }))
    } else {
        fs.mkdirSync(configurationFolder, { recursive: true })

        configuration = {
            option: {
                databaseFile: path.join(configurationFolder, DATABASE_FILE),
                port: 8080
            }
        }

        fs.writeFileSync(configurationFile, JSON.stringify(configuration))
    }

    return configuration
}

function startServer(configuration: Configuration, configurationFolder: string) {
    const port = configuration.option.port

    const app = express()

    const logger = initLogger(configurationFolder)

    // app.use(morgan('combined', { stream: new LoggerStream(logger) }))

    if (process.env.NODE_ENV !== 'production') {
        // CORS for development
        // https://enable-cors.org/server_expressjs.html
        // WARNING: should be first
        app.use(function (req, res, next) {
            res.header('Access-Control-Allow-Origin', '*')
            res.header('Access-Control-Allow-Methods', 'POST, PUT, GET, OPTIONS')
            res.header('Access-Control-Allow-Headers', 'Content-Type, Depth, User-Agent, X-File-Size, X-Requested-With, If-Modified-Since, X-File-Name, Cache-Control')
            res.header('Access-Control-Allow-Credentials', 'false')
            next()
        })
    }

    app.use(express.static(path.join(__dirname, '..', 'client')))

    app.use(express.json())
    app.use(express.urlencoded({ extended: true }))

    const sqliteDao = new SqliteDao(path.join(configurationFolder, DATABASE_FILE))

    // Initialize with module root folder
    const fileService = new FileService(path.join(__dirname, '..', '..'))
    const templateService = new TemplateService(sqliteDao, fileService)
    const studentService = new StudentService(sqliteDao)

    app.use('/templates', templateRouterBuilder(templateService))
    app.use('/students', studentRouterBuilder(studentService))

    app.use(errorMiddleware)

    app.set('json spaces', 4)

    app.listen(port, function () {
        console.info(figlet.textSync('Teaching Assistant', { font: 'doom' }))
        console.info('Version:', packageJson.version)
        console.info('Server:', 'http://localhost:' + port)
        console.info('Environment:', process.env.NODE_ENV)
    })
}
