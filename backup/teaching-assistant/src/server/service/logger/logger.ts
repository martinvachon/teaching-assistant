import winston, { createLogger, transports } from 'winston'
import path from 'path'

const MAX_FILE_SIZE = 5 * 1024 * 1024 // 5MB
const MAX_FILES = 5

function initLogger(configurationFolder: string) {
    const options = {
        file: {
            level: 'info',
            filename: path.join(configurationFolder, 'error.log'),
            handleExceptions: true,
            json: true,
            maxsize: MAX_FILE_SIZE,
            maxFiles: MAX_FILES,
            colorize: false
        },
        console: {
            level: 'debug',
            handleExceptions: true,
            json: false,
            colorize: true
        }
    }

    // Instantiate a new Winston Logger
    const logger = createLogger({
        transports: [
            new transports.File(options.file),
            new transports.Console(options.console)
        ],
        // do not exit on handled exceptions
        exitOnError: false
    })

    return logger
}

class LoggerStream {
    private logger: winston.Logger

    constructor(logger: winston.Logger) {
        this.logger = logger
    }
    write(message: string) {
        this.logger.info(message.substring(0, message.lastIndexOf('\n')))
    }
}

export { initLogger, LoggerStream }
