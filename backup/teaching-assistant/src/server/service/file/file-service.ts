import path from 'path'

const copydir = require('copy-dir')

export class FileService {

    private rootFolder: string

    constructor(rootFolder: string) {
        this.rootFolder = rootFolder
    }

    initTemplateFolder = (tempateFolder: string) => {
        const options = {}

        // Copy base course content files
        copydir.sync(path.join(this.rootFolder, 'dist', 'template'), tempateFolder, options)

        // Copy React XSLT player
        copydir.sync(path.join(this.rootFolder, 'dist', 'player'), path.join(tempateFolder, 'doc', 'player'), options)
    }

}
