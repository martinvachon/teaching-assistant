import express from 'express'

import { ExerciceService } from '../template/service/exercice-service'
import Configuration from 'common/configuration'

const HTTP_OK = 200
const CONTENT_TYPE_JSON = 'application/json'
const exerciceRouter = express.Router()

export function exerciceRouterBuilder() {

    const exerciceService = new ExerciceService()

    exerciceRouter.post('/:section/:exercice', (request, response) => {
        const exerciceIndex = parseInt(request.params.exercice, 10)
        const sectionIndex = parseInt(request.params.section, 10)

        const resultBefore = exerciceService.parseSrcFolder('', '')
        const exerciceBefore = resultBefore[sectionIndex].exercices[exerciceIndex]

        if (!exerciceBefore.question) {
            exerciceService.copyExercice(sectionIndex, exerciceIndex)
        } else if (!exerciceBefore.solution) {
            exerciceService.copySolution(sectionIndex, exerciceIndex)
        }

        const result = exerciceService.parseSrcFolder('', '')

        response.status(HTTP_OK).send({
            result: result[sectionIndex].exercices[exerciceIndex]
        })
    })

    return exerciceRouter
}
