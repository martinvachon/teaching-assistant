import express, { Request, Response, NextFunction } from 'express'
import path from 'path'

import { studentRouterBuilder, StudentService } from './student'
import { FileService, StorageService } from '../server/service/file-system'
import { SqliteDao } from './service/database'
import { templateRouterBuilder, TemplateService } from './template'
import { instanceRouterBuilder, EvaluationService, TaskService, InstanceStudentService, SummaryService, ExerciceService } from './instance'

const figlet = require('figlet')
const packageJson = require('../../package.json')
const {
    logger,
    PORT_PARMETER_PREFIX
} = require('../../script/build')

const DEFAULT_PORT = 8080

export function startServer (storageFolder: string, parameters: string[] = []) {
    const app = express()

    if (process.env.NODE_ENV !== 'production') {
        // CORS for development
        // https://enable-cors.org/server_expressjs.html
        // WARNING: should be first
        app.use(function (request: Request, response: Response, next: NextFunction) {
            response.header('Access-Control-Allow-Origin', '*')
            response.header('Access-Control-Allow-Methods', 'POST, PUT, GET, DELETE, OPTIONS')
            response.header('Access-Control-Allow-Headers', 'Content-Type, Depth, User-Agent, X-File-Size, X-Requested-With, If-Modified-Since, X-File-Name, Cache-Control')
            response.header('Access-Control-Allow-Credentials', 'false')
            next()
        })
    }

    // Client hosting for dist folder
    app.use(express.static(path.join(__dirname, '..', 'client')))

    app.use(express.json())
    app.use(express.urlencoded({ extended: true }))

    app.set('json spaces', 4)

    const storageService = new StorageService(storageFolder)
    const sqliteDao = new SqliteDao(storageService.getDatabaseFilePath())

    const studentService = new StudentService(sqliteDao)
    const fileService = new FileService(path.join(__dirname, '..', '..'))
    const templateService = new TemplateService(sqliteDao, fileService)

    const exerciceService = new ExerciceService(sqliteDao)
    const evaluationService = new EvaluationService(sqliteDao)
    const instanceStudentService = new InstanceStudentService(sqliteDao)
    const summaryService = new SummaryService(sqliteDao, instanceStudentService, evaluationService)
    const taskService = new TaskService(sqliteDao)

    app.use('/templates', templateRouterBuilder(templateService))
    app.use('/instances', instanceRouterBuilder(exerciceService, evaluationService, taskService, instanceStudentService, summaryService, 'firstName'))
    app.use('/students', studentRouterBuilder(studentService))

    let port = DEFAULT_PORT
    const portParameter = parameters.find(parameter => parameter.startsWith(PORT_PARMETER_PREFIX))
    if (portParameter) {
        port = parseInt(portParameter.replace(PORT_PARMETER_PREFIX, ''), 10)
    }

    console.log('parameters', parameters)
    console.log('port', port)

    app.listen(port, function () {
        console.info(figlet.textSync('Teaching Assistant', { font: 'Doom' }))
        logger.info('Version: ' + packageJson.version)
        logger.info('Server: http://localhost:' + port)
        logger.info('Environment: ' + process.env.NODE_ENV)
    })
}
