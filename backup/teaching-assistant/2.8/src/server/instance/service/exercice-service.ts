import path from 'path'
import fs from 'fs'

import Exercice from '../../../common/exercice'
import Section from '../../../common/section'

import { Dao } from '../../service/database/dao'

// const copydir = require('copy-dir')

const MAQUETTE_EXTENSION = '.maquette.'
const QUESTION_EXTENSION = '.question.'

interface ExerciceConfig {
    templateFolder: string
    instanceFolder: string
}

export class ExerciceService {
    private dao: Dao

    constructor (dao: Dao) {
        this.dao = dao
    }

    async getExercicesStatus (instenceId: string) {
        const id = parseInt(instenceId, 10)
        const sql = 'SELECT templateFolder, instanceFolder FROM v_exercicesStatus WHERE instanceId = ?'

        const exerciceConfig = await this.dao.getOne<ExerciceConfig, ExerciceConfig>(sql, [id], (row: ExerciceConfig) => {
            return {
                templateFolder: row.templateFolder,
                instanceFolder: row.instanceFolder
            }
        })

        return this.parseSrcFolder(exerciceConfig.templateFolder, exerciceConfig.instanceFolder)
    }

    parseExercicesFolder (templateSectionFolder: string, section: string, instanceSrcFolder: string) {
        const exercices: Exercice[] = []

        const templateExercicesFolder = path.join(templateSectionFolder, 'exercice')

        if (fs.existsSync(templateExercicesFolder)) {
            fs.readdirSync(templateExercicesFolder).forEach(file => {
                const templateExerciceFolder = path.join(templateExercicesFolder, file)
                const fileStats = fs.statSync(templateExerciceFolder)

                if (fileStats.isDirectory()) {
                    const instanceExerciceFolder = path.join(instanceSrcFolder, section, 'exercice', file)
                    const instanceSolutionFolder = path.join(instanceExerciceFolder, 'solution')

                    exercices.push({
                        name: file,
                        question: fs.existsSync(instanceExerciceFolder),
                        solution: fs.existsSync(instanceSolutionFolder)
                    })
                }
            })
        }

        return exercices
    }

    parseSrcFolder (templateFolder: string, instanceFolder: string) {
        const sections: Section[] = []

        const templateSrcFolder = path.join(templateFolder, 'src')
        const instanceSrcFolder = path.join(instanceFolder, 'src')

        fs.readdirSync(templateSrcFolder).forEach(file => {
            const templateSectionFolder = path.join(templateSrcFolder, file)
            const fileStats = fs.statSync(templateSectionFolder)

            if (fileStats.isDirectory()) {
                sections.push({
                    name: file,
                    exercices: this.parseExercicesFolder(templateSectionFolder, file, instanceSrcFolder)
                })
            }
        })

        return sections
    }

    /**
     * Copy an exercice folder from the course template to the course instance.
     * Note: The file containing the solution is not copied
     */
    copyExercice (sectionIndex: number, exerciceIndex: number) {
        const result = this.parseSrcFolder('', '')

        const sectionFolderName = result[sectionIndex].name
        const exerciceFolderName = result[sectionIndex].exercices[exerciceIndex].name

        // Students should create this file, so it is not copied with the exercice.
        // The file is the exercice name without the number prefix (01-) and a configurable extension (.js)
        const exerciceFileName = exerciceFolderName.substring(3) + '.js'

        const options = {
            filter: function (stat: string, filepath: string, filename: string) {
                if (stat === 'file' && path.basename(filepath) === exerciceFileName) {
                    return false
                }

                return true
            }
        }

        // const exerciceFolderFrom = path.join(this.configuration.courses[0].templateFolder, 'src', sectionFolderName, 'exercice', exerciceFolderName)
        // const exerciceFolderTo = path.join(this.configuration.courses[0].instanceFolder, 'src', sectionFolderName, 'exercice', exerciceFolderName)

        // fs.mkdirSync(exerciceFolderTo, { recursive: true })

        // copydir.sync(exerciceFolderFrom, exerciceFolderTo, options)
    }

    /**
     * Create a sub folder name /solution in the exercice course instance
     * Copy everyting from the template exception .question. and .maquette. files
     */
    copySolution (sectionIndex: number, exerciceIndex: number) {
        const result = this.parseSrcFolder('', '')

        const sectionFolderName = result[sectionIndex].name
        const exerciceFolderName = result[sectionIndex].exercices[exerciceIndex].name

        const options = {
            filter: function (stat: string, filepath: string, filename: string) {
                if (stat === 'file' &&
                    (path.basename(filepath).includes(MAQUETTE_EXTENSION) || path.basename(filepath).includes(QUESTION_EXTENSION))) {
                    return false
                }

                return true
            }
        }

        // const exerciceFolderFrom = path.join(this.configuration.courses[0].templateFolder, 'src', sectionFolderName, 'exercice', exerciceFolderName)
        // const exerciceFolderTo = path.join(this.configuration.courses[0].instanceFolder, 'src', sectionFolderName, 'exercice', exerciceFolderName, 'solution')

        // fs.mkdirSync(exerciceFolderTo, { recursive: true })

        // copydir.sync(exerciceFolderFrom, exerciceFolderTo, options)
    }
}
