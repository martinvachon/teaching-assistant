import express, { Request } from 'express'

import { SummaryService } from './service/summary-service'
import { EvaluationService } from './service/evaluation-service'
import { InstanceStudentService } from './service/instance-student-service'
import { TaskService } from './service/task-service'
import { ExerciceService } from './service/exercice-service'

const HTTP_OK = 200 // TODO duplicate

export function instanceRouterBuilder (exerciceService: ExerciceService, evaluationService: EvaluationService, taskService: TaskService, instanceStudentService: InstanceStudentService, summaryService: SummaryService, defaultSortField: string) {
    const router = express.Router()

    const getInstanceId = (request: Request) => {
        return parseInt(request.params.instanceId, 10)
    }

    router.get('/:instanceId/exercices', async (request, response, next) => {
        try {
            const sections = await exerciceService.getExercicesStatus(request.params.instanceId)
            response.status(HTTP_OK).send(sections)
        } catch (error) {
            return next(error)
        }
    })

    router.post('/:instanceId/exercices/copy/:section/:exercice', (request, response) => {
        // TODO: move everything in service methode
        const exerciceIndex = parseInt(request.params.exercice, 10)
        const sectionIndex = parseInt(request.params.section, 10)

        const resultBefore = exerciceService.parseSrcFolder('', '')
        const exerciceBefore = resultBefore[sectionIndex].exercices[exerciceIndex]

        if (!exerciceBefore.question) {
            exerciceService.copyExercice(sectionIndex, exerciceIndex)
        } else if (!exerciceBefore.solution) {
            exerciceService.copySolution(sectionIndex, exerciceIndex)
        }

        const result = exerciceService.parseSrcFolder('', '')

        response.status(HTTP_OK).send({
            result: result[sectionIndex].exercices[exerciceIndex]
        })
    })

    router.get('/:instanceId/evaluations', async (request, response, next) => {
        try {
            const evaluations = await evaluationService.getAll(getInstanceId(request))

            response.status(HTTP_OK).send(evaluations)
        } catch (error) {
            return next(error)
        }
    })

    router.get('/:instanceId/evaluations/:evaluationId', async (request, response, next) => {
        try {
            const evaluationId = parseInt(request.params.evaluationId, 10)
            const evaluation = await evaluationService.one(evaluationId)

            response.status(HTTP_OK).send(evaluation)
        } catch (error) {
            return next(error)
        }
    })

    router.get('/:instanceId/evaluations/:evaluationId/tasks', async (request, response, next) => {
        try {
            const evaluationId = parseInt(request.params.evaluationId, 10)
            const tasks = await taskService.getAll(evaluationId)

            response.status(HTTP_OK).send(tasks)
        } catch (error) {
            return next(error)
        }
    })

    router.get('/:instanceId/students', async (request, response, next) => {
        try {
            const field = request.query.field as string || defaultSortField
            const order = request.query.order as string || 'desc'

            const students = await instanceStudentService.getAll(getInstanceId(request), field, order)

            response.status(HTTP_OK).send(students)
        } catch (error) {
            return next(error)
        }
    })

    router.get('/:instanceId/summary', async (request, response, next) => {
        try {
            const summary = await summaryService.getSummary(getInstanceId(request))

            response.status(HTTP_OK).send(summary)
        } catch (error) {
            return next(error)
        }
    })

    router.post('/:instanceId/summary/result', async (request, response, next) => {
        try {
            const summary = await summaryService.saveResult(request.body)

            response.status(HTTP_OK).send(summary)
        } catch (error) {
            return next(error)
        }
    })

    return router
}
