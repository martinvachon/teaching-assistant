import { crudRouterBuilder } from '../service/crud'
import { StudentService } from './service/student-service'

export function studentRouterBuilder (studentService: StudentService) {
    const router = crudRouterBuilder(studentService, 'firstName')

    return router
}
