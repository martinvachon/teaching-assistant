import { Student } from '../../../common/student'
import { AbstractCRUDService, CRUDService } from '../../service/crud'
import { Dao } from '../../service/database/dao'

export class StudentService extends AbstractCRUDService<Student, Student> implements CRUDService<Student, Student> {
    constructor (dao: Dao) {
        super(dao, [], 'student', 'firstName')
    }
}
