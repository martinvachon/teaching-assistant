import sqlite3, { Database } from 'sqlite3'
import fs from 'fs'
import path from 'path'

import { Dao } from './dao'

const { logger } = require('../../../../script/build')

const DDL_FILE = 'teaching-assistant.sql'

export class SqliteDao implements Dao {
    database: Database

    constructor (databaseFile: string) {
        const create = !fs.existsSync(databaseFile)

        let mode = sqlite3.OPEN_READWRITE
        if (create) {
            mode = sqlite3.OPEN_READWRITE | sqlite3.OPEN_CREATE
        }

        this.database = new sqlite3.Database(databaseFile, mode, (error) => {
            if (error) {
                throw error
            }

            // Enable foreign key support
            this.database.exec('PRAGMA foreign_keys = ON;', (error) => {
                if (error) {
                    throw Error("Pragma statement didn't work.")
                } else {
                    logger.info('SQLite3 Foreign Key Enforcement is on.')

                    if (create) {
                        this.initSync()
                            .catch(error => {
                                throw error
                            })
                    }
                }
            })
        })
    }

    private async initSync () {
        const sql = fs.readFileSync(path.join(__dirname, DDL_FILE), 'utf-8')
        await this.exec(sql)
    }

    exec (sql: string) {
        return new Promise((resolve, reject) => {
            this.database.exec(sql, function (error) {
                if (error) {
                    reject(error)
                } else {
                    resolve({})
                }
            })
        })
    }

    execute (sql: string, params: any) {
        return new Promise((resolve, reject) => {
            this.database.run(sql, params, function (error) {
                if (error) {
                    reject(error)
                } else {
                    resolve({ id: this.lastID })
                }
            })
        })
    }

    getOne<E, T> (sql: string, params: any[] = [], rowMapper: (row: E) => T) {
        return new Promise<T>((resolve, reject) => {
            this.database.get(sql, params, (error, result) => {
                if (error) {
                    reject(error)
                } else if (!result) {
                    reject(Error('Row not found'))
                } else {
                    if (rowMapper) {
                        resolve(rowMapper(result))
                    } else {
                        resolve(result)
                    }
                }
            })
        })
    }

    getAll<E, T> (sql: string, params: any[] = [], rowMapper?: (row: E) => T) {
        return new Promise<T[]>((resolve, reject) => {
            this.database.all(sql, params, (error, rows: any[]) => {
                if (error) {
                    reject(error)
                } else {
                    if (rowMapper) {
                        resolve(rows.map(rowMapper))
                    } else {
                        resolve(rows)
                    }
                }
            })
        })
    }

    disconnect (disconnectCallback?: () => void) {
        this.database.close((error) => {
            if (error) {
                throw new Error(error.message)
            }

            disconnectCallback && disconnectCallback()
        })
    }
}
