PRAGMA foreign_keys = OFF;

DROP TABLE IF EXISTS student;

CREATE TABLE student (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    firstName VARCHAR(100) NOT NULL,
    lastName VARCHAR(100) NOT NULL,
    permanentCode INTEGER NOT NULL UNIQUE,
    email VARCHAR(255) NOT NULL UNIQUE
);

DROP TABLE IF EXISTS courseTemplate;

CREATE TABLE courseTemplate (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    title VARCHAR(100) NOT NULL UNIQUE,
    contents TEXT NOT NULL,
    displayOrder INTEGER NOT NULL,
    folder TEXT NOT NULL UNIQUE
);

DROP TABLE IF EXISTS evaluationCategory;

CREATE TABLE evaluationCategory (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    title VARCHAR(100) NOT NULL UNIQUE
);

DROP TABLE IF EXISTS evaluation;

CREATE TABLE evaluation (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    courseInstanceId INTEGER NOT NULL,
    title VARCHAR(100) NOT NULL UNIQUE,
    evaluationCategoryId INTEGER NOT NULL,
    total REAL NOT NULL,
    displayOrder INTEGER NOT NULL,
    FOREIGN KEY(courseInstanceId) REFERENCES courseInstance(id) ON UPDATE RESTRICT ON DELETE RESTRICT,
    FOREIGN KEY(evaluationCategoryId) REFERENCES evaluationCategory(id) ON UPDATE RESTRICT ON DELETE RESTRICT
);

DROP TABLE IF EXISTS task;

CREATE TABLE task (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    title VARCHAR(250) NOT NULL UNIQUE,
    total REAL NOT NULL,
    evaluationId INTEGER NOT NULL,
    displayOrder INTEGER NOT NULL,
    FOREIGN KEY(evaluationId) REFERENCES evaluation(id) ON UPDATE RESTRICT ON DELETE RESTRICT
);

DROP TABLE IF EXISTS courseInstance;

CREATE TABLE courseInstance (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    title VARCHAR(100) NOT NULL UNIQUE,
    contents TEXT NOT NULL,
    folder TEXT NOT NULL UNIQUE,
    courseTemplateId INTEGER NOT NULL,
    startDate TEXT NOT NULL,
    endDate TEXT NOT NULL,
    FOREIGN KEY(courseTemplateId) REFERENCES courseTemplate(id) ON UPDATE RESTRICT ON DELETE RESTRICT
);

DROP TABLE IF EXISTS courseInstanceStudent;

CREATE TABLE IF NOT EXISTS courseInstanceStudent (
    courseInstanceId INTEGER NOT NULL,
    studentId INTEGER NOT NULL,
    FOREIGN KEY(courseInstanceId) REFERENCES courseInstance(id) ON UPDATE RESTRICT ON DELETE RESTRICT,
    FOREIGN KEY(studentId) REFERENCES student(id) ON UPDATE RESTRICT ON DELETE RESTRICT,
    UNIQUE (courseInstanceId, studentId)
);

DROP TABLE IF EXISTS taskResult;

CREATE TABLE taskResult (
    taskId INTEGER NOT NULL,
    studentId INTEGER NOT NULL,
    studentResult REAL NOT NULL,
    FOREIGN KEY(taskId) REFERENCES task(id) ON UPDATE RESTRICT ON DELETE RESTRICT,
    FOREIGN KEY(studentId) REFERENCES student(id) ON UPDATE RESTRICT ON DELETE RESTRICT,
    UNIQUE (taskId, studentId)
);

DROP TABLE IF EXISTS absenceType;

CREATE TABLE absenceType (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    label VARCHAR(20) NOT NULL,
    absenceMinute INTEGER NOT NULL
);

DROP TABLE IF EXISTS absence;

CREATE TABLE absence (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    studentId INTEGER NOT NULL,
    courseInstanceId INTEGER NOT NULL,
    absenceDate INTEGER NOT NULL DEFAULT (strftime('%s', date('Now', 'localtime'))),
    absenceTypeId INTEGER NOT NULL,
    FOREIGN KEY(studentId) REFERENCES student(id) ON UPDATE RESTRICT ON DELETE RESTRICT,
    FOREIGN KEY(absenceTypeId) REFERENCES absenceType(id) ON UPDATE RESTRICT ON DELETE RESTRICT,
    FOREIGN KEY(courseInstanceId) REFERENCES courseInstance(id) ON UPDATE RESTRICT ON DELETE RESTRICT
);

DELETE FROM
    sqlite_sequence;

PRAGMA foreign_keys = ON;

-- =================================================================
-- Students by instance view
-- =================================================================
DROP VIEW IF EXISTS v_instanceStudents;

CREATE VIEW v_instanceStudents (
    id,
    firstName,
    lastName,
    permanentCode,
    email,
    courseInstanceId
) AS
SELECT
    s.id,
    s.firstName,
    s.lastName,
    s.permanentCode,
    s.email,
    c.courseInstanceId
FROM
    courseInstanceStudent AS c
    JOIN student AS s ON c.studentId = s.id;

-- =================================================================
-- Dashboard summary view
-- The summary is read only for evaluation containing more then one task
-- =================================================================
DROP VIEW IF EXISTS v_task_result_summary;

CREATE VIEW v_task_result_summary (
    courseInstanceId,
    evaluationId,
    studentId,
    total,
    taskId
) AS
SELECT
    e.courseInstanceId AS courseInstanceId,
    t.evaluationId AS evaluationId,
    tr.studentId,
    SUM(tr.studentResult) AS total,
    CASE
        WHEN COUNT(t.id) = 1 THEN (
            SELECT
                id
            FROM
                task
            WHERE
                task.evaluationId = t.evaluationId
            ORDER BY
                displayOrder ASC
            LIMIT
                1
        )
        ELSE 0
    END as taskId
FROM
    taskResult AS tr
    JOIN task AS t ON tr.taskId = t.id
    JOIN evaluation AS e ON t.evaluationId = e.id
GROUP BY
    courseInstanceId,
    evaluationId,
    studentId;

-- =================================================================
-- Exercices status by instance view
-- =================================================================
DROP VIEW IF EXISTS v_exercicesStatus;

CREATE VIEW v_exercicesStatus (
    instanceId,
    templateFolder,
    instanceFolder
) AS
SELECT
    i.id AS instanceId,
    t.folder AS templateFolder,
    i.folder AS instanceFolder
FROM
    courseInstance AS i
    JOIN courseTemplate AS t ON i.courseTemplateId = t.id;

