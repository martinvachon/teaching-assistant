
export interface Dao {
    getOne<E, T>(sql: string, params: any[], rowMapper?: (row: E) => T): Promise<E | T>

    getAll<E, T>(sql: string, params: any[], rowMapper?: (row: E) => T): Promise<E[] | T[]>

    execute(sql: string, params: any): Promise<any>
}
