
import fs from 'fs'
import path from 'path'

import { SqliteDao } from '../../service/database'
import { CourseTemplate, CourseInstance, CourseTemplateDTO } from '../../../common/course'
import { Init } from '../../../common/init'

import { FileService } from '../../service/file-system'

// TODO: why the typescript alias does not work here ?
// import { HttpException } from '../../service/logger'

const copydir = require('copy-dir')

const COURSE_TEMPLATE_TABLE = 'courseTemplate'
const COURSE_INSTANCE_TABLE = 'courseInstance'

const TEMPLATE_INSTANCE_FOLDER = 'instance'

export class TemplateService {
    private sqliteDao: SqliteDao
    private fileService: FileService

    constructor (sqliteDao: SqliteDao, fileService: FileService) {
        this.sqliteDao = sqliteDao
        this.fileService = fileService

        this.mapCourseInstance = this.mapCourseInstance.bind(this)
    }

    async getCourseTemplateDTOs () {
        const sql = 'SELECT * FROM ' + COURSE_TEMPLATE_TABLE + ' ORDER BY displayOrder'

        const courseTemplates = await this.sqliteDao.getAll<CourseTemplate, CourseTemplate>(sql, [], (row) => {
            return {
                id: row.id,
                title: row.title,
                folder: row.folder,
                displayOrder: row.displayOrder,
                courseInstances: [],
                contents: JSON.parse(row.contents.toString())
            }
        })

        return Promise.all(courseTemplates.map(this.mapCourseInstance))
    }

    async getCourseTemplateDTO (templateId: string) {
        const id = parseInt(templateId, 10)
        const sql = 'SELECT * FROM ' + COURSE_TEMPLATE_TABLE + ' WHERE id = ?'

        const courseTemplate = await this.sqliteDao.getOne<CourseTemplate, CourseTemplate>(sql, [id], (row) => {
            return {
                id: row.id,
                title: row.title,
                folder: row.folder,
                displayOrder: row.displayOrder,
                courseInstances: [],
                contents: JSON.parse(row.contents.toString())
            }
        })

        return this.mapCourseInstance(courseTemplate)
    }

    private async mapCourseInstance (courseTemplateDTO: CourseTemplateDTO) {
        const sql = 'SELECT * FROM ' + COURSE_INSTANCE_TABLE + ' WHERE courseTemplateId = ? ORDER BY startDate DESC'

        courseTemplateDTO.courseInstances = await this.sqliteDao.getAll<CourseInstance, CourseInstance>(sql, [courseTemplateDTO.id], row => {
            return {
                id: row.id,
                title: row.title,
                folder: row.folder,
                startDate: row.startDate,
                endDate: row.endDate,
                courseTemplateId: row.courseTemplateId,
                contents: JSON.parse(row.contents.toString())
            }
        })

        return courseTemplateDTO
    }

    createCourseInstance (instance: CourseInstance) {
        if (fs.existsSync(instance.folder) && fs.readdirSync(instance.folder).length !== 0) {
            throw Error('The specified folder exists and is not empty: ' + instance.folder)
        }

        const sqlSelectFolder = 'SELECT folder FROM ' + COURSE_TEMPLATE_TABLE + ' WHERE id = ?'

        const sqlInsertInstance = 'INSERT INTO ' + COURSE_INSTANCE_TABLE + ' (title, folder, startDate, endDate, courseTemplateId, contents) VALUES (?, ?, ?, ?, ?, ?)'

        const params = [
            instance.title,
            instance.folder,
            instance.startDate,
            instance.endDate,
            instance.courseTemplateId,
            JSON.stringify(instance.contents)
        ]

        interface Folder {
            folder: string
        }

        // First query to get the template folder
        return this.sqliteDao.getOne<Folder, Promise<unknown>>(sqlSelectFolder, [instance.courseTemplateId], (row) => {
            // Copy the sample from the course template to create the course instance
            copydir.sync(path.join(row.folder, TEMPLATE_INSTANCE_FOLDER), instance.folder)

            return this.sqliteDao.execute(sqlInsertInstance, params)
        })
    }

    createCourseTemplate (template: CourseTemplate) {
        const sql = 'INSERT INTO ' + COURSE_TEMPLATE_TABLE + ' (title, folder, displayOrder, contents) VALUES (?, ?, ?, ?)'

        const params = [
            template.title,
            template.folder,
            template.displayOrder,
            JSON.stringify(template.contents)
        ]

        return this.sqliteDao.execute(sql, params).then(() => this.getCourseTemplateDTOs())
    }

    /**
     * TODO: PUT should update only specified fields
     * @param template
     */
    updateCourseTemplate (template: CourseTemplate) {
        const sql = 'UPDATE ' + COURSE_TEMPLATE_TABLE + ' SET title = ?, folder = ?, displayOrder = ?, contents = ? WHERE id = ?'

        const params = [
            template.title,
            template.folder,
            template.displayOrder,
            JSON.stringify(template.contents),
            template.id
        ]

        return this.sqliteDao.execute(sql, params)
            .then(() => this.getCourseTemplateDTOs())
    }

    deleteCourseTemplate (templateId: string) {
        const id = parseInt(templateId, 10)
        const sql = 'DELETE FROM ' + COURSE_TEMPLATE_TABLE + ' WHERE id = ?'

        return this.sqliteDao.execute(sql, [id]).then(() => this.getCourseTemplateDTOs())
    }

    updateCourseInstance (instance: CourseInstance) {
        const sql = 'UPDATE ' + COURSE_INSTANCE_TABLE + ' SET title = ?, folder = ?, startDate = ?, endDate = ?, contents = ? WHERE id = ?'

        const params = [
            instance.title,
            instance.folder,
            instance.startDate,
            instance.endDate,
            instance.contents,
            instance.id
        ]

        return this.sqliteDao.execute(sql, params)
    }

    exportCourseInstance (instanceId: string) {
        // Export the complete course instance in a zip archive
        // todo export code

        // if archive success
        // Delete the course instance from the database

        const id = parseInt(instanceId, 10)
        const sql = 'DELETE FROM ' + COURSE_INSTANCE_TABLE + ' WHERE id = ?'

        return this.sqliteDao.execute(sql, [id])
    }

    deleteCourseInstance (instenceId: string) {
        const id = parseInt(instenceId, 10)
        const sql = 'DELETE FROM ' + COURSE_INSTANCE_TABLE + ' WHERE id = ?'

        return this.sqliteDao.execute(sql, [id]).then(() => this.getCourseTemplateDTOs())
    }

    getCourseInstance (instenceId: string) {
        const id = parseInt(instenceId, 10)
        const sql = 'SELECT * FROM ' + COURSE_INSTANCE_TABLE + ' WHERE id = ?'

        return this.sqliteDao.getOne<CourseInstance, CourseInstance>(sql, [id], (row: CourseInstance) => {
            return {
                id: row.id,
                title: row.title,
                folder: row.folder,
                startDate: row.startDate,
                endDate: row.endDate,
                courseTemplateId: row.courseTemplateId,
                contents: JSON.parse(row.contents.toString())
            }
        })
    }

    initTemplate (init: Init) {
        const folder = init.folder

        if (!fs.existsSync(folder)) {
            throw Error('Folder not found: ' + folder)
        }

        const template = {
            id: 0,
            title: path.basename(folder),
            folder,
            displayOrder: 0,
            courseInstances: [],
            contents: []
        }

        // Update all existing template order to insert the new template in first position
        const sqlOrder = 'UPDATE ' + COURSE_TEMPLATE_TABLE + ' SET displayOrder = displayOrder + 10'
        return this.sqliteDao.execute(sqlOrder, [])
            // Insert new template
            .then(() => this.createCourseTemplate(template)
                .then(() => {
                    // TODO: initilize before the database transaction
                    if (fs.readdirSync(folder).length === 0) {
                        this.fileService.initTemplateFolder(folder)
                    }
                })
            )
        // TODO: .then(() => templateService.getCourseTemplateDTOs())
    }
}
