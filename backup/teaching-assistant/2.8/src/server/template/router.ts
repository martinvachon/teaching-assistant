import express from 'express'

import { TemplateService } from './service/template-service'

const HTTP_OK = 200

export function templateRouterBuilder (templateService: TemplateService) {
    const router = express.Router()

    router.get('/:templateId/instances/:instanceId', async (request, response, next) => {
        const result = await templateService.getCourseInstance(request.params.instanceId).catch(next)

        response.status(HTTP_OK).send(result)
    })

    router.delete('/:templateId/instances/:instanceId', async (request, response, next) => {
        const result = await templateService.deleteCourseInstance(request.params.instanceId).catch(next)
        response.status(HTTP_OK).send(result)
    })

    router.delete('/:templateId', async (request, response, next) => {
        const result = await templateService.deleteCourseTemplate(request.params.templateId).catch(next)
        response.status(HTTP_OK).send(result)
    })

    router.post('/:templateId/instances', async (request, response, next) => {
        const result = await templateService.createCourseInstance(request.body)
            .then(() => templateService.getCourseTemplateDTOs())
            .catch(next)

        response.status(HTTP_OK).send(result)
    })

    router.put('/:templateId/instances', async (request, response, next) => {
        const result = await templateService.updateCourseInstance(request.body)
            .then(() => templateService.getCourseTemplateDTOs())
            .catch(next)

        response.status(HTTP_OK).send(result)
    })

    router.post('/init', async (request, response, next) => {
        const result = await templateService.initTemplate(request.body)
            .then(() => templateService.getCourseTemplateDTOs())
            .catch(next)

        response.status(HTTP_OK).send(result)
    })

    router.post('/', async (request, response, next) => {
        const result = await templateService.createCourseTemplate(request.body).catch(next)
        response.status(HTTP_OK).send(result)
    })

    router.put('/', async (request, response, next) => {
        const result = await templateService.updateCourseTemplate(request.body).catch(next)
        response.status(HTTP_OK).send(result)
    })

    // TODO: not implemented
    router.post('/:instanceId/export', async (request, response, next) => {
        try {
            await templateService.exportCourseInstance(request.params.instanceId)
            const result = await templateService.getCourseTemplateDTOs()
            response.status(HTTP_OK).send(result)
        } catch (error) {
            return next(error)
        }
    })

    router.get('/:templateId', async (request, response, next) => {
        const result = await templateService.getCourseTemplateDTO(request.params.templateId)
        // .catch((error) => { next(new HttpException(404, error)) })

        response.status(HTTP_OK).send(result)
    })

    router.get('/', async (request, response, next) => {
        try {
            const result = await templateService.getCourseTemplateDTOs()

            response.status(HTTP_OK).send(result)
        } catch (error) {
            return next(error)
        }
    })

    return router
}
