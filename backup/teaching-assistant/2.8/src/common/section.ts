import Exercice from './exercice'

export default interface Section {
    name: string
    exercices: Exercice[]
}
