
export default interface Configuration {
    option: Option
}

export interface Option {
    port: number
    databaseFile: string
}
