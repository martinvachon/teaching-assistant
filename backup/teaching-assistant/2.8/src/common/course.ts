import { Entity } from './entity'
import { Content } from './content'

interface Course {
    id: number
    title: string
    folder: string
    contents: Content[]
}

/**
 * Base course version use to crerate all course instances
 */
export interface CourseTemplate extends Course, Entity {
    displayOrder: number
    courseInstances: CourseInstance[]
}

/**
 * Course instance containing exercice progression and student grades.
 */
export interface CourseInstance extends Course, Entity {
    startDate: string
    endDate: string
    courseTemplateId: number
}

export interface CourseTemplateDTO extends CourseTemplate {

}

export interface CourseInstanceDTO extends CourseInstance {

}
