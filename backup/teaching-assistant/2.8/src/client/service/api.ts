import { Entity } from '../../common/entity'

export class Api {
    private baseUrl: string

    constructor (baseUrl: string) {
        this.baseUrl = baseUrl
    }

    private buildHeaders () {
        return {
            'Content-Type': 'application/json'
        }
    }

    get (endpoint: string, successCallBack: (result: any) => void) {
        fetch(this.baseUrl + endpoint, {
            method: 'get',
            headers: this.buildHeaders()
        })
            .then(response => {
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                return response.json()
            })
            .then(successCallBack).catch(error => {
                throw error
            })
    }

    save (endpoint: string, body: Entity, successCallBack: (result: any) => void) {
        fetch(this.baseUrl + endpoint, {
            method: body.id > 0 ? 'put' : 'post',
            headers: this.buildHeaders(),
            body: JSON.stringify(body)
        })
            .then(response => {
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                return response.json()
            })
            .then(successCallBack).catch(error => {
                throw error
            })
    }

    put (endpoint: string, successCallBack: (result: any) => void, body: any) {
        fetch(this.baseUrl + endpoint, {
            method: 'put',
            headers: this.buildHeaders(),
            body: JSON.stringify(body)
        })
            .then(response => {
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                return response.json()
            })
            .then(successCallBack).catch(error => {
                throw error
            })
    }

    post (endpoint: string, body: any, successCallBack: (result: any) => void) {
        fetch(this.baseUrl + endpoint, {
            method: 'post',
            headers: this.buildHeaders(),
            body: JSON.stringify(body)
        })
            .then(response => {
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                return response.json()
            })
            .then(successCallBack).catch(error => {
                throw error
            })
    }

    delete (endpoint: string, successCallBack: (result: any) => void) {
        fetch(this.baseUrl + endpoint, {
            method: 'delete',
            headers: this.buildHeaders()
        })
            .then(response => {
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                return response.json()
            })
            .then(successCallBack).catch(error => {
                throw error
            })
    }
}
