import React from 'react'
import {
    Route,
    Routes,
    Link,
    Navigate
} from 'react-router-dom'

import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import NavDropdown from 'react-bootstrap/NavDropdown'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faGear } from '@fortawesome/free-solid-svg-icons/faGear'
import { faCheck } from '@fortawesome/free-solid-svg-icons/faCheck'

import { Api } from './service/api'
import StudentsContainer from './students'
import TemplatesContainer from './templates'
import { Help } from './component/help'
import { CourseInstance } from '../common/course'
import InstancesContainer from './instances'
import { withRouter } from './service/withRouter'
import QuizContainer from './quiz'
import TemplateEditor from './template-editor'

interface TeachingAssistantProps {
    navigate: (path: string) => void
}

interface TeachingAssistantState {
    showHelp: boolean
    selectedCourseInstance?: CourseInstance
    showSummary: boolean
    showAttendance: boolean
    showExercice: boolean
}

class TeachingAssistant extends React.Component<TeachingAssistantProps, TeachingAssistantState> {
    private api: Api

    constructor (props: TeachingAssistantProps) {
        super(props)

        this.state = {
            showHelp: false,
            showSummary: true,
            showAttendance: true,
            showExercice: true
        }

        this.api = new Api('http://localhost:8080')
    }

    handleShowHelp = () => {
        this.setState({
            showHelp: true
        })
    }

    handleCloseHelp = () => {
        this.setState({
            showHelp: false
        })
    }

    handleSelectInstance = (templateId: number, instanceId: number) => () => {
        this.api.get('/templates/' + templateId + '/instances/' + instanceId, courseInstance => {
            console.log(courseInstance)

            this.setState({
                selectedCourseInstance: courseInstance
            })

            this.props.navigate('/instances/' + courseInstance.id + '/summary')
        })
    }

    handleUnselectCourseInstance = () => {
        this.setState({
            selectedCourseInstance: undefined
        })
    }

    handleCourseInstanceDropdown = (key: string) => {
        if (key === 'dropdown_summary_key' || key === 'dropdown_attendance_key' || key === 'dropdown_exercice_key') {
            const stateName = this.getFlagStateName(key)

            this.setState((prevState) => ({
                ...prevState,
                [stateName]: !this.state[stateName]
            }))
        }
    }

    /**
     * Convert key as example: dropdown_summay_key to showSummary
     * @param key
     * @returns
     */
    getFlagStateName = (key: string) => {
        const token = key.split('_')[1]
        return 'show' + token.charAt(0).toUpperCase() + token.slice(1)
    }

    renderCourseInstanceTitle = (selectedCourseInstance: CourseInstance) => {
        return <span> / {selectedCourseInstance.title} </span>
    }

    renderDropdownItemFlag = (key: string, label: string) => {
        return (
            <NavDropdown.Item eventKey={key}>
                {this.state[this.getFlagStateName(key)] && < FontAwesomeIcon icon={faCheck} />} {label}
            </NavDropdown.Item>
        )
    }

    renderCourseInstanceDropdown = (handleCourseInstanceDropdown: (key: string) => void) => {
        return (
            <Nav onSelect={handleCourseInstanceDropdown} activeKey="">
                <NavDropdown title={<FontAwesomeIcon icon={faGear} />} id="course_instance_dropdown">

                    <NavDropdown.Item as={Link} to={'/instances/' + this.state.selectedCourseInstance.id + '/students'} eventKey="dropdown_student_key">Students</NavDropdown.Item>

                    <NavDropdown.Item as={Link} to={'/instances/' + this.state.selectedCourseInstance.id + '/evaluations'} eventKey="4.2">Evaluations</NavDropdown.Item>

                    <NavDropdown.Item eventKey="4.3">Something else here</NavDropdown.Item>
                    <NavDropdown.Divider />
                    {this.renderDropdownItemFlag('dropdown_summary_key', 'Show summary')}
                    {this.renderDropdownItemFlag('dropdown_attendance_key', 'Show attendance')}
                    {this.renderDropdownItemFlag('dropdown_exercice_key', 'Show exercice')}
                </NavDropdown>
            </Nav>
        )
    }

    renderInstanceSummaryLink = () => {
        return (
            <Link to={'/instances/' + this.state.selectedCourseInstance.id + '/summary'}>
                {this.renderCourseInstanceTitle(this.state.selectedCourseInstance)}
            </Link>
        )
    }

    render () {
        return (
            <Container id='teaching_assistant' fluid>
                <Navbar id='main_navbar' collapseOnSelect expand='lg' bg='dark' variant='dark' fixed='top'>
                    <Container fluid>
                        <Navbar.Brand>
                            <Link to='/templates' onClick={this.handleUnselectCourseInstance}>
                                Teaching Assistant
                            </Link>
                            {this.state.selectedCourseInstance && this.renderInstanceSummaryLink()}
                        </Navbar.Brand>
                        {this.state.selectedCourseInstance && this.renderCourseInstanceDropdown(this.handleCourseInstanceDropdown)}
                        <Navbar.Toggle aria-controls='responsive-navbar-nav' />
                        <Navbar.Collapse id='responsive-navbar-nav' className="justify-content-end">
                            <Nav className='mr-auto'>
                                <Nav.Link as={Link} style={{}} to='/template-editor'>Template</Nav.Link>
                                <Nav.Link as={Link} style={{}} to='/quiz'>Quiz</Nav.Link>
                                <NavDropdown align="end" title='Course' id='course_dropdown'>
                                    <NavDropdown.Item as={Link} to='/students'>Student</NavDropdown.Item>

                                    <NavDropdown.Divider />

                                    <NavDropdown.Item onClick={this.handleShowHelp}>Help</NavDropdown.Item>
                                </NavDropdown>
                            </Nav>
                        </Navbar.Collapse>
                    </Container>
                </Navbar>

                <Row>
                    <Col>

                        <Routes>

                            <Route
                                path='/template-editor/*'
                                element={<TemplateEditor />} />

                            <Route
                                path='/quiz/*'
                                element={<QuizContainer />} />

                            <Route
                                path='/students/*'
                                element={<StudentsContainer api={this.api} />} />

                            <Route
                                path='/templates'
                                element={<TemplatesContainer
                                    handleSelectInstance={this.handleSelectInstance}
                                    api={this.api} />} />

                            <Route
                                path='/instances/:instanceId/*'
                                element={<InstancesContainer
                                    api={this.api}
                                    showSummary={this.state.showSummary}
                                    showAttendance={this.state.showAttendance}
                                    showExercice={this.state.showExercice} />} />

                            <Route
                                path='*'
                                element={<Navigate to='/templates' />} />

                        </Routes>
                    </Col>
                </Row>

                <Help handleClose={this.handleCloseHelp} show={this.state.showHelp} />
            </Container>
        )
    }
}

export default withRouter(TeachingAssistant)
