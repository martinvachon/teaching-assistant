export interface Task {
    name: string
    value: number
}

export interface Evaluation {
    title: string
    total: number
    tasks: Task[]
    teams: []
}

export interface Template {
    title: string
    code: string
    evaluations: Evaluation[]
}

export interface TemplateConfiguration {
    folder: string
    template: Template
}
