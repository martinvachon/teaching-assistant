import React from 'react'

import { Link } from 'react-router-dom'

import ListGroup from 'react-bootstrap/ListGroup'
import ButtonGroup from 'react-bootstrap/ButtonGroup'
import ButtonToolbar from 'react-bootstrap/ButtonToolbar'
import Button from 'react-bootstrap/Button'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'

import { faListOl } from '@fortawesome/free-solid-svg-icons/faListOl'
import { faUserGraduate } from '@fortawesome/free-solid-svg-icons/faUserGraduate'
import { faPlus } from '@fortawesome/free-solid-svg-icons/faPlus'
import { faPencilAlt } from '@fortawesome/free-solid-svg-icons/faPencilAlt'
import { faFileExport } from '@fortawesome/free-solid-svg-icons/faFileExport'
import { faMinus } from '@fortawesome/free-solid-svg-icons/faMinus'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import { CourseTemplateDTO, CourseInstance } from '../../../common/course'

interface TemplateListProps {
    courseTemplateDTOs: CourseTemplateDTO[]
    exportInstanceHandler: (event: React.MouseEvent, instanceId: number) => void
    handleEditTemplate: (id: number) => (event: React.MouseEvent) => void
    handleDeleteTemplate: (id: number) => (event: React.MouseEvent) => void
    handleAddInstance: (template: CourseTemplateDTO) => (event: React.MouseEvent) => void
    handleEditInstance: (templateId: number, instanceId: number) => (event: React.MouseEvent) => void
    handleDeleteInstance: (templateId: number, instanceId: number) => (event: React.MouseEvent) => void
    handleSelectInstance: (templateId: number, instanceId: number) => (event: React.MouseEvent) => void
}

function renderInstance (props: TemplateListProps, courseInstance: CourseInstance) {
    const exportInstanceHandler = (instanceId: number) => (event: React.MouseEvent) => {
        props.exportInstanceHandler(event, instanceId)
    }

    return (
        <ListGroup.Item key={courseInstance.id} >
            <Row>
                <Col onClick={props.handleSelectInstance(courseInstance.courseTemplateId, courseInstance.id)}>{courseInstance.title}</Col>
                <Col>{courseInstance.startDate}</Col>
                <Col>{courseInstance.endDate}</Col>
                <Col className='d-flex justify-content-end'>
                    <ButtonGroup aria-label='Exercices toolbar' size='sm'>
                        <Link to={'/instances/' + courseInstance.id + '/exercices'} className='btn btn-secondary' title='Exercies'>
                            <FontAwesomeIcon icon={faListOl} />
                        </Link>
                    </ButtonGroup>

                    <ButtonGroup aria-label='Marks toolbar' size='sm'>
                        <Link to={'/templates/' + courseInstance.courseTemplateId + '/instances/' + courseInstance.id + '/marks'} className='btn btn-secondary' title='Marks'>
                            <FontAwesomeIcon icon={faUserGraduate} />
                        </Link>
                    </ButtonGroup>

                    <ButtonGroup aria-label='Course instance toolbar' size='sm'>
                        <Button variant='secondary' title='Export course instance archive' onClick={exportInstanceHandler(courseInstance.id)}>
                            <FontAwesomeIcon icon={faFileExport} />
                        </Button>
                        <Button variant='secondary' title='Edit course instance' onClick={props.handleEditInstance(courseInstance.courseTemplateId, courseInstance.id)}>
                            <FontAwesomeIcon icon={faPencilAlt} />
                        </Button>
                        <Button variant='secondary' title='Delete course instance' onClick={props.handleDeleteInstance(courseInstance.courseTemplateId, courseInstance.id)}>
                            <FontAwesomeIcon icon={faMinus} />
                        </Button>
                    </ButtonGroup>
                </Col>
            </Row>
        </ListGroup.Item>
    )
}

function renderTemplate (props: TemplateListProps, courseTemplateDTO: CourseTemplateDTO) {
    return (
        <ListGroup.Item key={courseTemplateDTO.id} variant='dark'>
            <Row>
                <Col>{courseTemplateDTO.title}</Col>
                <Col>

                </Col>
                <Col>
                    <ButtonToolbar className="justify-content-end" aria-label="Course template toolbar">
                        <ButtonGroup aria-label='Course template buttons group' size='sm'>
                            <Button onClick={props.handleEditTemplate(courseTemplateDTO.id)} variant='secondary' title='Edit course template'>
                                <FontAwesomeIcon icon={faPencilAlt} />
                            </Button>
                            <Button onClick={props.handleDeleteTemplate(courseTemplateDTO.id)} variant='secondary' title='Delete course template'>
                                <FontAwesomeIcon icon={faMinus} />
                            </Button>
                        </ButtonGroup>
                        <ButtonGroup aria-label='Course instance buttons group' size='sm'>
                            <Button onClick={props.handleAddInstance(courseTemplateDTO)} variant='secondary' title='Add course instance'>
                                <FontAwesomeIcon icon={faPlus} />
                            </Button>
                        </ButtonGroup>
                    </ButtonToolbar>
                </Col>
            </Row>

            <ListGroup >
                {courseTemplateDTO.courseInstances.map(courseInstance => renderInstance(props, courseInstance))}
            </ListGroup>
        </ListGroup.Item >
    )
}

export const TemplateList = (props: TemplateListProps) => (
    <ListGroup >
        {props.courseTemplateDTOs.map(courseTemplateDTO => renderTemplate(props, courseTemplateDTO))}
    </ListGroup>
)
