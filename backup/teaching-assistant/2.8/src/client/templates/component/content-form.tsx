import React from 'react'

import Form from 'react-bootstrap/Form'
import Table from 'react-bootstrap/Table'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlus } from '@fortawesome/free-solid-svg-icons/faPlus'
import { faMinus } from '@fortawesome/free-solid-svg-icons/faMinus'

import { Content } from '../../../common/content'

interface ContentFormProps {
    handleContentChange: (index: number) => (event: React.ChangeEvent<HTMLInputElement>) => void
    contents: Content[]
}

function renderRow (props: ContentFormProps, content: Content, index: number) {
    return (
        <tr key={content.id}>
            <td>
                <Form.Control type='number' name='id' id='id' value={content.id.toString()} onChange={props.handleContentChange(index)} />
            </td>
            <td>
                <Form.Control type='name' name='name' id='name' value={content.name} onChange={props.handleContentChange(index)} />
            </td>
            <td>
                <Form.Control as='select' name='type' id='type' value={content.type.toString()} onChange={props.handleContentChange(index)}>
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                </Form.Control>
            </td>
            <td>
                <Form.Control type='number' name='value' id='value' value={content.value.toString()} onChange={props.handleContentChange(index)} />
            </td>
            <td>
                <FontAwesomeIcon icon={faMinus} />
            </td>
        </tr>
    )
}

export const ContentForm = (props: ContentFormProps) => (
    <Table striped bordered hover>
        <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Type</th>
                <th>Value</th>
                <th><FontAwesomeIcon icon={faPlus} /></th>
            </tr>
        </thead>
        <tbody>
            {props.contents.map((content, index) => renderRow(props, content, index))}
        </tbody>
    </Table>
)
