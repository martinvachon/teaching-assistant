import React from 'react'

import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'
import Col from 'react-bootstrap/Col'
import Row from 'react-bootstrap/Row'

import { FormRow } from '../../component/form-row'
import { CourseInstance } from '../../../common/course'

interface InstanceFormProps {
    showForm: boolean
    handleClose: (event: React.MouseEvent<HTMLElement, MouseEvent>) => void
    handleSave: (event: React.MouseEvent<HTMLElement, MouseEvent>) => void
    handleChange: (event: React.ChangeEvent<HTMLInputElement>) => void
    instance: CourseInstance
}

export const InstanceForm = (props: InstanceFormProps) => (
    <Modal show={props.showForm} onHide={props.handleClose} >
        <Modal.Header closeButton>
            <Modal.Title>Course Instance</Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <Form>
                <FormRow id='title' label='Title:'>
                    <Form.Control type='text' placeholder='Course title' value={props.instance.title} onChange={props.handleChange} />
                </FormRow>

                <FormRow id='folder' label='Folder:'>
                    <Form.Control type='text' placeholder='Folder' value={props.instance.folder} onChange={props.handleChange} />
                </FormRow>

                <FormRow id='date' label='Date:'>
                    <Form.Group as={Row}>
                        <Form.Group controlId='startDate' as={Row}>
                            <Col sm={2}>
                                <Form.Label column >Start:</Form.Label>
                            </Col>
                            <Col sm={10}>
                                <Form.Control type='date' value={props.instance.startDate} onChange={props.handleChange} />
                            </Col>
                        </Form.Group>
                        <Form.Group controlId='endDate' as={Row}>
                            <Col sm={2}>
                                <Form.Label column >End:</Form.Label>
                            </Col>
                            <Col sm={10}>
                                <Form.Control type='date' value={props.instance.endDate} onChange={props.handleChange} />
                            </Col>
                        </Form.Group>
                    </Form.Group>
                </FormRow>
            </Form>
        </Modal.Body>
        <Modal.Footer>
            <Button variant='secondary' onClick={props.handleClose}>Close</Button>
            <Button variant='secondary' onClick={props.handleSave}>Save Changes</Button>
        </Modal.Footer>
    </Modal >
)
