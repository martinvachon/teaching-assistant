import React from 'react'

import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'

import { FormRow } from '../../component/form-row'
import { ContentForm } from './content-form'
import { CourseTemplateDTO } from '../../../common/course'

interface TemplateFormProps {
    showForm: boolean
    handleClose: () => void
    handleSave: (event: React.MouseEvent) => void
    handleChange: (event: React.ChangeEvent<HTMLInputElement>) => void
    handleContentChange: (index: number) => (event: React.ChangeEvent<HTMLInputElement>) => void
    template: CourseTemplateDTO
}

export const TemplateForm = (props: TemplateFormProps) => (
    <Modal show={props.showForm} onHide={props.handleClose} >
        <Modal.Header closeButton>
            <Modal.Title>Course Template</Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <Form>
                <FormRow id='title' label='Title:'>
                    <Form.Control type='text' placeholder='Course title' value={props.template.title} onChange={props.handleChange} />
                </FormRow>

                <FormRow id='folder' label='Folder:'>
                    <Form.Control type='text' placeholder='Folder' value={props.template.folder} onChange={props.handleChange} />
                </FormRow>

                <FormRow id='displayOrder' label='Order:'>
                    <Form.Control type='number' placeholder='Display order' value={props.template.displayOrder.toString()} onChange={props.handleChange} />
                </FormRow>

                <ContentForm
                    handleContentChange={props.handleContentChange}
                    contents={props.template.contents} />
            </Form>
        </Modal.Body>
        <Modal.Footer>
            <Button variant='secondary' onClick={props.handleClose}>Close</Button>
            <Button variant='secondary' onClick={props.handleSave}>Save Changes</Button>
        </Modal.Footer>
    </Modal>
)
