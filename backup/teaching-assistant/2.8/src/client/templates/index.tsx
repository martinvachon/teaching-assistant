import React from 'react'

import ButtonToolbar from 'react-bootstrap/ButtonToolbar'
import ButtonGroup from 'react-bootstrap/ButtonGroup'
import Button from 'react-bootstrap/Button'

import { faPlus } from '@fortawesome/free-solid-svg-icons/faPlus'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import { CourseTemplateDTO, CourseInstanceDTO } from '../../common/course'

import { TemplateList } from './component/template-list'
import { TemplateForm } from './component/template-form'
import { InstanceForm } from './component/instance-form'
import { TemplateInitForm } from './component/template-init-form'

import { Api } from '../service/api'

import { Init } from '../../common/init'

// const logoImage = require('../style/image/logo.png')

const CONTENTS = [
    {
        id: 10,
        name: 'Exercice',
        type: 1,
        value: 10
    },
    {
        id: 20,
        name: 'Quiz 1',
        type: 2,
        value: 15
    },
    {
        id: 30,
        name: 'Quiz 2',
        type: 2,
        value: 15
    },
    {
        id: 40,
        name: 'Quiz 3',
        type: 2,
        value: 15
    },
    {
        id: 50,
        name: 'Travail pratique',
        type: 3,
        value: 25
    },
    {
        id: 60,
        name: 'Examain pratique',
        type: 4,
        value: 20
    }
]

interface TemplatesContainerProps {
    api: Api
    handleSelectInstance: (templateId: number, instanceId: number) => () => void
}

interface TemplatesContainerState {
    courseTemplateDTOs: CourseTemplateDTO[]
    courseTemplateDTO: CourseTemplateDTO
    courseInstanceDTO: CourseInstanceDTO
    isInstanceFormShow: boolean
    isTemplateFormShow: boolean
    showInit: boolean
    init: Init
    selectedCourseInstance?: CourseInstanceDTO
}

const COURSE_TEMPLATE_DTO_STATE = {
    id: 0,
    title: '',
    folder: '',
    displayOrder: 0,
    contents: CONTENTS,
    courseInstances: []
}

const COURSE_INSTANCE_DTO_STATE = {
    id: 0,
    courseTemplateId: 0,
    title: '',
    folder: '',
    startDate: '',
    endDate: '',
    displayOrder: 0,
    contents: []
}

class TemplatesContainer extends React.Component<TemplatesContainerProps, TemplatesContainerState> {
    constructor (props: TemplatesContainerProps) {
        super(props)

        this.state = {
            courseTemplateDTOs: [],
            courseTemplateDTO: COURSE_TEMPLATE_DTO_STATE,
            courseInstanceDTO: COURSE_INSTANCE_DTO_STATE,
            isInstanceFormShow: false,
            isTemplateFormShow: false,
            showInit: false,
            init: {
                folder: ''
            },
            selectedCourseInstance: null
        }

        this.exportInstanceHandler = this.exportInstanceHandler.bind(this)
    }

    componentDidMount () {
        this.props.api.get('/templates', courseTemplateDTOs => {
            this.setState({ courseTemplateDTOs })
        })
    }

    handleEditTemplate = (id: number) => () => {
        this.props.api.get('/templates/' + id, courseTemplateDTO => {
            this.setState({
                courseTemplateDTO,
                isTemplateFormShow: true
            })
        })
    }

    handleSaveTemplate = () => {
        this.props.api.save('/templates', this.state.courseTemplateDTO, courseTemplateDTOs => {
            this.setState({
                isTemplateFormShow: false,
                courseTemplateDTOs
            })
        })
    }

    handleDeleteTemplate = (templateId: number) => () => {
        if (confirm('Delete course template ?')) {
            this.props.api.delete('/templates/' + templateId, courseTemplateDTOs => {
                this.setState({
                    courseTemplateDTOs
                })
            })
        }
    }

    handleAddInstance = (template: CourseTemplateDTO) => () => {
        this.setState({
            courseInstanceDTO: {
                ...COURSE_INSTANCE_DTO_STATE,
                courseTemplateId: template.id,
                title: template.title + '_instance',
                folder: template.folder + '_instance',
                contents: template.contents
            },
            isInstanceFormShow: true
        })
    }

    handleEditInstance = (templateId: number, instanceId: number) => () => {
        this.props.api.get('/templates/' + templateId + '/instances/' + instanceId, courseInstanceDTO => {
            this.setState({
                courseInstanceDTO,
                isInstanceFormShow: true
            })
        })
    }

    handleDeleteInstance = (templateId: number, instanceId: number) => () => {
        if (confirm('Delete course instance ?')) {
            this.props.api.delete('/templates/' + templateId + '/instances/' + instanceId, courseTemplateDTOs => {
                this.setState({
                    courseTemplateDTOs
                })
            })
        }
    }

    handleSaveInstance = () => {
        this.props.api.save('/templates/' + this.state.courseInstanceDTO.courseTemplateId + '/instances', this.state.courseInstanceDTO, courseTemplateDTOs => {
            this.setState({
                isInstanceFormShow: false,
                courseTemplateDTOs
            })
        })
    }

    handleClose = () => {
        this.setState({
            isTemplateFormShow: false,
            isInstanceFormShow: false
        })
    }

    handleTemplateChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({
            courseTemplateDTO: {
                ...this.state.courseTemplateDTO,
                [event.target.id]: event.target.value
            }
        })
    }

    handleTemplateContentChange = (contentIndex: number) => (event: React.ChangeEvent<HTMLInputElement>) => {
        const contentName = event.target.id
        const contentValue = event.target.value

        this.setState(state => {
            return {
                courseTemplateDTO: {
                    ...state.courseTemplateDTO,
                    contents: state.courseTemplateDTO.contents.map((content, index) => {
                        let contentResult
                        if (contentIndex === index) {
                            contentResult = {
                                ...state.courseTemplateDTO.contents[index],
                                [contentName]: contentValue
                            }
                        } else {
                            contentResult = content
                        }

                        return contentResult
                    })
                }
            }
        })
    }

    handleInstanceChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({
            courseInstanceDTO: {
                ...this.state.courseInstanceDTO,
                [event.target.id]: event.target.value
            }
        })
    }

    exportInstanceHandler (event: React.MouseEvent, instanceId: number) {
        const url = 'http://localhost:8080/instances/' + instanceId + '/export'

        fetch(url, {
            method: 'post',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.json())
            .then(courseTemplateDTOs => {
                this.setState({
                    courseTemplateDTOs
                })
            }
            ).catch(error => {
                throw error
            })
    }

    handleInitChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({
            init: {
                ...this.state.init,
                [event.target.id]: event.target.value
            }
        })
    }

    handleInitOpen = () => {
        this.props.api.post('/templates/init', this.state.init, (courseTemplateDTOs) => {
            this.setState({
                courseTemplateDTOs
            })

            this.handleCloseInit()
        })
    }

    handleInitTemplate = () => {
        this.setState({
            showInit: true
        })
    }

    handleCloseInit = () => {
        this.setState({
            showInit: false
        })
    }

    render () {
        return (
            <div id='templates'>
                <TemplateInitForm
                    init={this.state.init}
                    handleChange={this.handleInitChange}
                    showForm={this.state.showInit}
                    handleCancel={this.handleCloseInit}
                    handleOpen={this.handleInitOpen} />

                <TemplateForm
                    template={this.state.courseTemplateDTO}
                    handleChange={this.handleTemplateChange}
                    handleContentChange={this.handleTemplateContentChange}
                    handleSave={this.handleSaveTemplate}
                    handleClose={this.handleClose}
                    showForm={this.state.isTemplateFormShow} />

                <InstanceForm
                    instance={this.state.courseInstanceDTO}
                    handleChange={this.handleInstanceChange}
                    handleSave={this.handleSaveInstance}
                    handleClose={this.handleClose}
                    showForm={this.state.isInstanceFormShow} />

                <section>

                    <ButtonToolbar className="justify-content-end" aria-label="Main template toolbar" >
                        <ButtonGroup aria-label='Template button group' size='sm'>
                            <Button onClick={this.handleInitTemplate} variant='secondary' title='Add new course template'>
                                <FontAwesomeIcon icon={faPlus} />
                            </Button>
                        </ButtonGroup>
                    </ButtonToolbar>

                    <TemplateList
                        handleSelectInstance={this.props.handleSelectInstance}
                        exportInstanceHandler={this.exportInstanceHandler}
                        handleEditTemplate={this.handleEditTemplate}
                        handleDeleteTemplate={this.handleDeleteTemplate}
                        handleAddInstance={this.handleAddInstance}
                        handleEditInstance={this.handleEditInstance}
                        handleDeleteInstance={this.handleDeleteInstance}
                        courseTemplateDTOs={this.state.courseTemplateDTOs} />
                </section>
            </div>
        )
    }
}

export default TemplatesContainer
