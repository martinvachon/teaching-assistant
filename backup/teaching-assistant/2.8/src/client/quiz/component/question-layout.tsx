import React, { FunctionComponent } from 'react'

import { Question } from '../model'

interface QuestionLayoutProps {
    question: Question
    children: React.ReactNode
}

export const QuestionLayout: FunctionComponent<QuestionLayoutProps> = ({ question, children }) => (
    <div className='question-layout'>
        <h2>{question.text}</h2>
        <ol>
            {children}
        </ol>
    </div>
)
