import React, { FunctionComponent } from 'react'

import { Answer, Question } from '../model'
import { QuestionLayout } from './question-layout'

interface SingleChoiceQuestionProps {
    question: Question
    userAnswer?: string
    onChange: (event: React.ChangeEvent<HTMLInputElement>) => void
}

export const SingleChoiceQuestion: FunctionComponent<SingleChoiceQuestionProps> = ({ question, userAnswer, onChange }) => {
    const renderAnswer = (answer: Answer, index: number) => {
        const id = question.name + '_answerIndex-' + index
        let checked = false
        if (userAnswer && (userAnswer === id)) {
            checked = true
        }
        return (
            <li key={index}>
                <input type="radio" id={id} value={id} name={question.name} onChange={onChange} checked={checked} />
                <label htmlFor={id}>{answer.text}</label>
            </li>
        )
    }
    return (
        <div className='single-choice-question'>
            <QuestionLayout question={question}>
                {question.answers.map((answer, index) => renderAnswer(answer, index))}
            </QuestionLayout>
        </div>
    )
}
