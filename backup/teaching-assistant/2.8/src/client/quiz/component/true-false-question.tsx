import React, { FunctionComponent } from 'react'

import { Answer, Question } from '../model'
import { QuestionLayout } from './question-layout'

interface TrueFalseQuestionProps {
    question: Question,
    userAnswer?: string
    onChange: (event: React.ChangeEvent<HTMLInputElement>) => void
}

export const TrueFalseQuestion: FunctionComponent<TrueFalseQuestionProps> = ({ question, onChange, userAnswer }) => {
    const renderAnswer = (answer: Answer, index: number) => {
        const id = question.name + '_answerIndex-' + index
        let checked = false
        if (userAnswer && (userAnswer === id)) {
            checked = true
        }
        return (
            <li key={index}>
                <input type="radio" id={id} value={id} name={question.name} onChange={onChange} checked={checked} />
                <label htmlFor={id}>{answer.text}</label>
            </li>
        )
    }
    return (
        <div className='true-false-question'>
            <QuestionLayout question={question}>
                {question.answers.map((answer, index) => renderAnswer(answer, index))}
            </QuestionLayout>
        </div>
    )
}
