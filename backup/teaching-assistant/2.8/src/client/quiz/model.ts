
export interface Answer {
    text: string
    point: number
}

export interface Question {
    name: string
    type: 'SINGLE_CHOICE' | 'MULTIPLE_CHOICE' | 'TRUE_FALSE'
    text: string
    answers: Answer[]
}

export interface Quiz {
    title: string
    course: string
    questions: Question[]
}
