import React, { FunctionComponent } from 'react'
import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFolder } from '@fortawesome/free-solid-svg-icons/faFolder'
import { faFolderOpen } from '@fortawesome/free-solid-svg-icons/faFolderOpen'
import { faCheck } from '@fortawesome/free-solid-svg-icons/faCheck'
import { faFileExport } from '@fortawesome/free-solid-svg-icons/faFileExport'

import { faPlus } from '@fortawesome/free-solid-svg-icons/faPlus'
import { faMinus } from '@fortawesome/free-solid-svg-icons/faMinus'
import { faEye } from '@fortawesome/free-solid-svg-icons/faEye'
import { faPencilAlt } from '@fortawesome/free-solid-svg-icons/faPencilAlt'

import { faUserGraduate } from '@fortawesome/free-solid-svg-icons/faUserGraduate'
import { faListOl } from '@fortawesome/free-solid-svg-icons/faListOl'
import { faBookOpen } from '@fortawesome/free-solid-svg-icons/faBookOpen'

import { faCodeBranch } from '@fortawesome/free-solid-svg-icons/faCodeBranch'
import { faCode } from '@fortawesome/free-solid-svg-icons/faCode'
import { faGear } from '@fortawesome/free-solid-svg-icons/faGear'

import ListGroup from 'react-bootstrap/ListGroup'

interface HelpProps {
    show: boolean
    handleClose: (event: React.MouseEvent<HTMLElement, MouseEvent>) => void
}

export const Help: FunctionComponent<HelpProps> = (props: HelpProps) => (
    <Modal show={props.show} onHide={props.handleClose} id='help' >
        <Modal.Header closeButton>
            <Modal.Title>Help</Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <h3>General</h3>
            <ListGroup variant="flush">
                <ListGroup.Item><FontAwesomeIcon icon={faPlus} /> <span>Create</span></ListGroup.Item>
                <ListGroup.Item><FontAwesomeIcon icon={faMinus} /> <span>Delete</span></ListGroup.Item>
                <ListGroup.Item><FontAwesomeIcon icon={faEye} /> <span>View</span></ListGroup.Item>
                <ListGroup.Item><FontAwesomeIcon icon={faPencilAlt} /> <span>Edit</span></ListGroup.Item>
            </ListGroup>

            <h3>Exercise</h3>
            <ListGroup variant="flush">
                <ListGroup.Item><FontAwesomeIcon icon={faUserGraduate} /> <span>Exercises corrections</span></ListGroup.Item>
                <ListGroup.Item><FontAwesomeIcon icon={faListOl} /> <span>Exercises distribution</span></ListGroup.Item>
                <ListGroup.Item><FontAwesomeIcon icon={faFolder} /> <span>Exercise question</span></ListGroup.Item>
                <ListGroup.Item><FontAwesomeIcon icon={faFolderOpen} /> <span>Exercise solution</span></ListGroup.Item>
            </ListGroup>

            <h3>Template / instance</h3>
            <ListGroup variant="flush">
                <ListGroup.Item><FontAwesomeIcon icon={faCheck} /></ListGroup.Item>
                <ListGroup.Item><FontAwesomeIcon icon={faFileExport} /></ListGroup.Item>
                <ListGroup.Item><FontAwesomeIcon icon={faBookOpen} /></ListGroup.Item>
                <ListGroup.Item><FontAwesomeIcon icon={faCodeBranch} /></ListGroup.Item>
                <ListGroup.Item><FontAwesomeIcon icon={faCode} /></ListGroup.Item>
                <ListGroup.Item><FontAwesomeIcon icon={faGear} /></ListGroup.Item>
            </ListGroup>
        </Modal.Body>
        <Modal.Footer>
            <Button variant='secondary' onClick={props.handleClose}>Close</Button>
        </Modal.Footer>
    </Modal >
)
