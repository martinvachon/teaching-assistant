import React, { FunctionComponent } from 'react'

import Form from 'react-bootstrap/Form'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'

interface FormRowProps {
    id: string
    label: string
}

export const FormRow: FunctionComponent<FormRowProps> = ({ id, label, children }) => (
    <Form.Group as={Row} controlId={id}>
        <Form.Label column sm={2}>{label}</Form.Label>
        <Col sm={10}>{children}</Col>
    </Form.Group>
)
