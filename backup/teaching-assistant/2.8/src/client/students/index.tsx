import React from 'react'
import {
    Routes,
    Route,
    Link
} from 'react-router-dom'

import Button from 'react-bootstrap/Button'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faMinus } from '@fortawesome/free-solid-svg-icons/faMinus'
import { faEye } from '@fortawesome/free-solid-svg-icons/faEye'
import { faPencilAlt } from '@fortawesome/free-solid-svg-icons/faPencilAlt'
import { faPlus } from '@fortawesome/free-solid-svg-icons/faPlus'

import { Api } from '../service/api'
import { StudentTable } from './component/student-table'
import { Student } from '../../common/student'
import { StudentForm } from './component/student-form'

interface StudentsContainerProps {
    api: Api
}

interface StudentsContainerState {
    students: Student[]
    student: Student
    studentFormShow: boolean

}

const STUDENT_DTO_STATE = {
    id: 0,
    firstName: '',
    lastName: '',
    permanentCode: '',
    email: ''
}

class StudentsContainer extends React.Component<StudentsContainerProps, StudentsContainerState> {
    constructor (props: StudentsContainerProps) {
        super(props)

        this.state = {
            students: [],
            student: STUDENT_DTO_STATE,
            studentFormShow: false
        }
    }

    componentDidMount () {
        this.props.api.get('/students', students => {
            this.setState({
                students
            })
        })
    }

    handleEditStudent = (id: number) => (event: React.MouseEvent) => {
        this.props.api.get('/students/' + id, student => {
            this.setState({
                student,
                studentFormShow: true
            })
        })
    }

    handleDeleteStudent = (id: number) => (event: React.MouseEvent) => {
        this.props.api.delete('/students/' + id, students => {
            this.setState({
                students
            })
        })
    }

    handleAddStudent = (event: React.MouseEvent) => {
        this.setState({
            student: STUDENT_DTO_STATE,
            studentFormShow: true
        })
    }

    handleStudentChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({
            student: {
                ...this.state.student,
                [event.target.id]: event.target.value
            }
        })
    }

    handleStudentSave = () => {
        this.props.api.save('/students', this.state.student, students => {
            this.setState({
                studentFormShow: false,
                students
            })
        })
    }

    handleClose = () => {
        this.setState({
            studentFormShow: false
        })
    }

    renderRowCommand = (student: Student) => {
        return (
            <React.Fragment>
                <Link to={'/students/' + student.id + '/viewer'} className='btn btn-secondary' title='View student'>
                    <FontAwesomeIcon icon={faEye} />
                </Link>
                <Button onClick={this.handleEditStudent(student.id)} variant='secondary' title='Edit student'>
                    <FontAwesomeIcon icon={faPencilAlt} />
                </Button>
                <Button onClick={this.handleDeleteStudent(student.id)} variant='secondary' title='Delete student'>
                    <FontAwesomeIcon icon={faMinus} />
                </Button>
            </React.Fragment>
        )
    }

    renderHeaderCommand = () => {
        return (
            <Button onClick={this.handleAddStudent} variant='secondary' title='Add student'>
                <FontAwesomeIcon icon={faPlus} />
            </Button>
        )
    }

    render () {
        return (
            <div id='students' >
                <StudentForm
                    student={this.state.student}
                    handleChange={this.handleStudentChange}
                    handleSave={this.handleStudentSave}
                    handleClose={this.handleClose}
                    showForm={this.state.studentFormShow} />

                <section>
                    <Routes>
                        <Route
                            path='/'
                            element={<StudentTable
                                renderRowCommand={this.renderRowCommand}
                                renderHeadCommand={this.renderHeaderCommand}
                                students={this.state.students} />}
                        />
                    </Routes>
                </section>
            </div>
        )
    }
}

export default StudentsContainer
