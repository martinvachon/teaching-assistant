import React from 'react'

import { Student } from '../../../common/student'

import ButtonGroup from 'react-bootstrap/ButtonGroup'
import Table from 'react-bootstrap/Table'

interface StudentListProps {
    students: Student[]
    renderRowCommand: (student: Student) => JSX.Element
    renderHeadCommand: () => JSX.Element
}

function renderStudent (props: StudentListProps, student: Student) {
    return (
        <tr key={student.id}>
            <td>{student.firstName}</td>
            <td>{student.lastName}</td>
            <td>{student.email}</td>
            <td>{student.permanentCode}</td>
            <td className='command-column'>
                <ButtonGroup aria-label='Student toolbar' size='sm'>
                    {props.renderRowCommand(student)}
                </ButtonGroup>
            </td>
        </tr>
    )
}

export const StudentTable = (props: StudentListProps) => (
    <Table striped bordered hover responsive>
        <thead>
            <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Permanent code</th>
                <th className='command-column'>
                    <ButtonGroup aria-label='Add student' size='sm'>
                        {props.renderHeadCommand()}
                    </ButtonGroup>
                </th>
            </tr>
        </thead>
        <tbody>
            {props.students.map(Student => renderStudent(props, Student))}
        </tbody>
    </Table>
)
