import React from 'react'

import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'

import { FormRow } from '../../component/form-row'
import { Student } from '../../../common/student'

interface StudentFormProps {
    showForm: boolean
    handleClose: any
    handleSave: any
    handleChange: (event: React.ChangeEvent<HTMLInputElement>) => void
    student: Student
}

export const StudentForm = (props: StudentFormProps) => (
    <Modal show={props.showForm} onHide={props.handleClose} >
        <Modal.Header closeButton>
            <Modal.Title>Student</Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <Form>
                <FormRow id='firstName' label='First name:'>
                    <Form.Control type='text' placeholder='First name' value={props.student.firstName} onChange={props.handleChange} />
                </FormRow>

                <FormRow id='lastName' label='Last name:'>
                    <Form.Control type='text' placeholder='Last name' value={props.student.lastName} onChange={props.handleChange} />
                </FormRow>

                <FormRow id='permanentCode' label='Permanent code:'>
                    <Form.Control type='text' placeholder='Permanent code' value={props.student.permanentCode} onChange={props.handleChange} />
                </FormRow>

                <FormRow id='email' label='Email:'>
                    <Form.Control type='email' placeholder='email@server.com' value={props.student.email} onChange={props.handleChange} />
                </FormRow>
            </Form>
        </Modal.Body>
        <Modal.Footer>
            <Button variant='secondary' onClick={props.handleClose}>Close</Button>
            <Button variant='secondary' onClick={props.handleSave}>Save Changes</Button>
        </Modal.Footer>
    </Modal>
)
