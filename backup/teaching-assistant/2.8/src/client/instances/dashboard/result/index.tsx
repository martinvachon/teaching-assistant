import React from 'react'

import { Evaluation, SummaryResult, TaskResult } from '../../../../common/evaluation'
import { Student } from '../../../../common/student'

import { Api } from '../../../service/api'

interface ResultContainerProps {
    api: Api
    instanceId: number
    student: Student
    evaluation: Evaluation
    summaryResult: SummaryResult
}

interface ResultContainerState {
    value: string
    isValid: boolean
    isFocus: boolean
}

class ResultContainer extends React.Component<ResultContainerProps, ResultContainerState> {
    constructor (props: ResultContainerProps) {
        super(props)

        this.state = {
            value: props.summaryResult.total.toString(),
            isValid: true,
            isFocus: false
        }
    }

    componentDidMount () {
    }

    componentDidUpdate (prevProps: ResultContainerProps) {
        if (prevProps.summaryResult.total !== this.props.summaryResult.total) {
            this.setState({
                value: this.props.summaryResult.total.toString()
            })
        }
    }

    handleOnFocus = (event: React.FocusEvent) => {
        this.setState({
            isFocus: true
        })
    }

    handleOnBlur = (event: React.FocusEvent) => {
        if (this.state.isValid) {
            const url = '/instances/' + this.props.instanceId + '/summary/result'

            const taskResult: TaskResult = {
                taskId: this.props.summaryResult.taskId,
                studentId: this.props.student.id,
                studentResult: parseFloat(this.state.value)
            }

            this.props.api.post(url, taskResult, (response) => {
                this.setState({
                    isFocus: false
                })
            })
        }
    }

    handleOnChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const inputValue = event.target.value
        let isValid = true

        const result = parseFloat(inputValue)
        if (isNaN(result) || result.toString() !== inputValue) {
            isValid = false
        }

        this.setState({
            value: inputValue,
            isValid
        })
    }

    buildClassName = () => {
        let className = ' '

        if (this.state.isFocus || !this.state.isValid) {
            className = 'input-result-' + this.state.isValid
        }

        return className
    }

    render () {
        const key = this.props.evaluation.id + '-' + this.props.student.id

        return (
            <td key={key}>
                {(this.props.summaryResult.taskId === 0
                    ? this.state.value
                    : <input
                        type="number"
                        className={this.buildClassName()}
                        value={this.state.value}
                        onFocus={this.handleOnFocus}
                        onBlur={this.handleOnBlur}
                        onChange={this.handleOnChange} />)}
            </td>
        )
    }
}

export default ResultContainer
