import React from 'react'

import Table from 'react-bootstrap/Table'

import { Evaluation, Summary } from '../../../common/evaluation'
import { Student } from '../../../common/student'

import { Api } from '../../service/api'

import Result from './result'

interface DashboardContainerProps {
    api: Api
    instanceId: number
}

interface DashboardContainerState {
    summary?: Summary
}

class DashboardContainer extends React.Component<DashboardContainerProps, DashboardContainerState> {
    constructor (props: DashboardContainerProps) {
        super(props)

        this.state = {

        }
    }

    componentDidMount () {
        this.props.api.get('/instances/' + this.props.instanceId + '/summary ', summary => {
            this.setState({
                summary
            })
        })
    }

    renderSummaryEvaluationHeader = (evaluation: Evaluation) => {
        return (
            <th key={evaluation.id}>
                {evaluation.title}
            </th>
        )
    }

    renderSummaryEvaluation = (student: Student, evaluation: Evaluation) => {
        return (
            <td key={evaluation.id + '-' + student.id}>
                <input type="text" value={this.state.summary.results[evaluation.id + '-' + student.id].total} />
            </td>
        )
    }

    getSummaryResult = (evaluation, student) => {
        const result = this.state.summary.results[evaluation.id + '-' + student.id]
        console.log('result', result)

        return result
    }

    renderSummaryRow = (student: Student) => {
        return (
            <tr key={student.id}>
                <td>
                    {student.firstName}
                </td>
                <td>
                    {student.lastName}
                </td>
                {this.state.summary.evaluations.map(evaluation => <Result
                    key={evaluation.id + '-' + student.id}
                    instanceId={this.props.instanceId}
                    api={this.props.api}
                    student={student}
                    evaluation={evaluation}
                    summaryResult={this.getSummaryResult(evaluation, student)} />)}
            </tr>
        )
    }

    renderSummary = () => {
        return (
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        {this.state.summary.evaluations.map(evaluation => this.renderSummaryEvaluationHeader(evaluation))}
                    </tr>
                </thead>
                <tbody>
                    {this.state.summary.students.map(student => this.renderSummaryRow(student))}
                </tbody>
            </Table>
        )
    }

    render () {
        return (
            <div id='dashboard'>

                DashboardContainer TODO

                {this.state.summary && this.renderSummary()}
            </div>
        )
    }
}

export default DashboardContainer
