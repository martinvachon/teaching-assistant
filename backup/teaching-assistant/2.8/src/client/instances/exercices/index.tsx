import React from 'react'

import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFolder } from '@fortawesome/free-solid-svg-icons/faFolder'
import { faFolderOpen } from '@fortawesome/free-solid-svg-icons/faFolderOpen'
import { faCheck } from '@fortawesome/free-solid-svg-icons/faCheck'

import Exercice from '../../../common/exercice'
import Section from '../../../common/section'

interface ExerciceContainerProps {
    instanceId: number
}

interface ExerciceContainerState {
    sections: Section[]
}

class ExerciceContainer extends React.Component<ExerciceContainerProps, ExerciceContainerState> {
    constructor (props: ExerciceContainerProps) {
        super(props)

        this.state = {
            sections: []
        }

        this.copyActionOnClickHandler = this.copyActionOnClickHandler.bind(this)
    }

    componentDidMount () {
        fetch('http://localhost:8080/instances/' + this.props.instanceId + '/exercices')
            .then(response => response.json())
            .then(response => this.setState({
                sections: response
            }))
            .catch((error) => { throw error })
    }

    copyActionOnClickHandler (sectionIndex: number, exerciceIndex: number) {
        fetch('http://localhost:8080/exercices/' + sectionIndex + '/' + exerciceIndex, { method: 'POST' })
            .then(response => response.json())
            .then(response => {
                this.setState(state => {
                    const sectionsCopy = [...state.sections]

                    sectionsCopy[sectionIndex].exercices[exerciceIndex] = response.result
                    return {
                        sections: sectionsCopy
                    }
                })
            })
            .catch((error: any) => { throw error })
    }

    renderCopyActionLink (exercice: any, sectionIndex: number, exerciceIndex: number) {
        let resultComponent

        if (!exercice.question) {
            resultComponent = <span><FontAwesomeIcon icon={faFolder} /> Question</span>
        } else if (!exercice.solution) {
            resultComponent = <span><FontAwesomeIcon icon={faFolderOpen} /> Solution</span>
        } else {
            resultComponent = <FontAwesomeIcon icon={faCheck} />
        }

        return <a onClick={() => this.copyActionOnClickHandler(sectionIndex, exerciceIndex)}>{resultComponent}</a>
    }

    renderExercice (exercice: Exercice, sectionIndex: number, exerciceIndex: number) {
        return (
            <article key={exerciceIndex}>
                <span className='exercice-name'>{exercice.name}</span>
                <span className='exercice-copy-action'>{this.renderCopyActionLink(exercice, sectionIndex, exerciceIndex)}</span>
            </article>
        )
    }

    renderSection (section: Section, sectionIndex: number) {
        return (
            <section key={sectionIndex}>
                {section.name}
                <div>
                    {section.exercices.map((exercice, exerciceIndex: number) => this.renderExercice(exercice, sectionIndex, exerciceIndex))}
                </div>
            </section>
        )
    }

    render () {
        return (
            <div id='exercice'>
                <Row>
                    <Col>
                        <div className='text-center'>
                            Exercice
                        </div>
                    </Col>

                </Row>

                <Row>
                    <Col>
                        {this.state.sections.map((section, index) => this.renderSection(section, index))}
                    </Col>
                </Row>
            </div>
        )
    }
}

export default ExerciceContainer
