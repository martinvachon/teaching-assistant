import React from 'react'

import { Student } from '../../../common/student'
import { Api } from '../../service/api'

interface InstanceStudentsContainerProps {
    api: Api
    instanceId: number
}

interface InstanceStudentsContainerState {
    instanceStudents: Student[]
}

class InstanceStudentsContainer extends React.Component<InstanceStudentsContainerProps, InstanceStudentsContainerState> {
    constructor (props: InstanceStudentsContainerProps) {
        super(props)

        this.state = {
            instanceStudents: []
        }
    }

    componentDidMount () {
        this.props.api.get('/instances/' + this.props.instanceId + '/students ', students => {
            this.setState({
                instanceStudents: students
            })
        })
    }

    render () {
        return (
            <div id='instance_students'>

                InstanceStudentsContainer TODO
            </div>
        )
    }
}

export default InstanceStudentsContainer
