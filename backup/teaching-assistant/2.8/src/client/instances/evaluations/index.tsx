import React from 'react'

import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'

import Button from 'react-bootstrap/Button'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faMinus } from '@fortawesome/free-solid-svg-icons/faMinus'
import { faPencilAlt } from '@fortawesome/free-solid-svg-icons/faPencilAlt'
import { faPlus } from '@fortawesome/free-solid-svg-icons/faPlus'

import { Evaluation, Task } from '../../../common/evaluation'
import { Api } from '../../service/api'

import { EvaluationTable } from './evaluation-table'
import { EvaluationForm } from './evaluation-form'
import { TaskTable } from './task-table'

interface EvaluationsContainerProps {
    api: Api
    instanceId: number
}

interface EvaluationsContainerState {
    evaluation?: Evaluation,
    evaluations: Evaluation[],
    evaluationFormShow: boolean
    tasks: Task[],
    task?: Task,
    taskFormShow: boolean
}

const EVALUATION_STATE: Evaluation = {
    courseInstanceId: 0,
    evaluationCategoryId: 0,
    title: '',
    total: 0,
    displayOrder: 0,
    id: 0
}

const TASK_STATE: Task = {
    title: '',
    total: 0,
    evaluationId: 0,
    displayOrder: 0,
    id: 0
}

class EvaluationsContainer extends React.Component<EvaluationsContainerProps, EvaluationsContainerState> {
    constructor (props: EvaluationsContainerProps) {
        super(props)

        this.state = {
            evaluations: [],
            evaluationFormShow: false,
            tasks: [],
            taskFormShow: true
        }
    }

    componentDidMount () {
        this.props.api.get('/instances/' + this.props.instanceId + '/evaluations/', evaluations => {
            this.setState({
                evaluations
            })
        })
    }

    handleEditEvaluation = (evaluationId: number) => (event: React.MouseEvent) => {
        event.stopPropagation()

        const url = '/instances/' + this.props.instanceId + '/evaluations/' + evaluationId

        this.props.api.get(url, (evaluation) => {
            this.setState({
                evaluationFormShow: true,
                evaluation
            })
        })
    }

    handleChangeEvaluation = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({
            evaluation: {
                ...this.state.evaluation,
                [event.target.id]: event.target.value
            }
        })
    }

    handleSaveEvaluation = () => {
        // this.props.api.save()
    }

    handleCloseEvaluationForm = () => {
        this.setState({
            evaluationFormShow: false
        })
    }

    handleDeleteEvaluation = (evaluationId: number) => (event: React.MouseEvent) => {
        event.stopPropagation()

        console.log(evaluationId)
    }

    handleAddEvaluation = (event: React.MouseEvent) => {
        this.setState({
            evaluation: EVALUATION_STATE,
            evaluationFormShow: true
        })
    }

    handleAddTask = (event: React.MouseEvent) => {
        this.setState({
            task: TASK_STATE,
            taskFormShow: true
        })
    }

    renderEvaluationHeadCommand = () => {
        return (
            <Button onClick={this.handleAddEvaluation} variant='secondary' title='Add evaluation'>
                <FontAwesomeIcon icon={faPlus} />
            </Button>
        )
    }

    renderEvaluationRowCommand = (evaluation: Evaluation) => {
        return (
            <React.Fragment>
                <Button onClick={this.handleEditEvaluation(evaluation.id)} variant='secondary' title='Edit evaluation'>
                    <FontAwesomeIcon icon={faPencilAlt} />
                </Button>
                <Button onClick={this.handleDeleteEvaluation(evaluation.id)} variant='secondary' title='Delete evaluation'>
                    <FontAwesomeIcon icon={faMinus} />
                </Button>
            </React.Fragment>
        )
    }

    handleEvaluationRowOnClick = (evaluationId: number) => (event: React.MouseEvent) => {
        this.props.api.get('/instances/' + this.props.instanceId + '/evaluations/' + evaluationId + '/tasks', tasks => {
            this.setState({
                tasks
            })
        })
    }

    renderTaskHeadCommand = () => {
        return (
            <Button onClick={this.handleAddTask} variant='secondary' title='Add evaluation'>
                <FontAwesomeIcon icon={faPlus} />
            </Button>
        )
    }

    handleEditTask = (taskId: number) => (event: React.MouseEvent) => {
        event.stopPropagation()

        console.log(taskId)
    }

    handleDeleteTask = (taskId: number) => (event: React.MouseEvent) => {
        event.stopPropagation()

        console.log(taskId)
    }

    renderTaskRowCommand = (task: Task) => {
        return (
            <React.Fragment>
                <Button onClick={this.handleEditTask(task.id)} variant='secondary' title='Edit evaluation'>
                    <FontAwesomeIcon icon={faPencilAlt} />
                </Button>
                <Button onClick={this.handleDeleteTask(task.id)} variant='secondary' title='Delete evaluation'>
                    <FontAwesomeIcon icon={faMinus} />
                </Button>
            </React.Fragment>
        )
    }

    handleTaskRowOnClick = (evaluationId: number) => (event: React.MouseEvent) => {
        this.props.api.get('/instances/' + this.props.instanceId + '/evaluations/' + evaluationId + '/tasks', tasks => {
            this.setState({
                tasks
            })
        })
    }

    render () {
        return (
            <div id='evaluations'>
                <Row>
                    <Col>
                        <EvaluationTable
                            evaluations={this.state.evaluations}
                            renderRowCommand={this.renderEvaluationRowCommand}
                            renderHeadCommand={this.renderEvaluationHeadCommand}
                            handleRowOnClick={this.handleEvaluationRowOnClick} />
                    </Col>
                    <Col>
                        <TaskTable
                            tasks={this.state.tasks}
                            renderRowCommand={this.renderTaskRowCommand}
                            renderHeadCommand={this.renderTaskHeadCommand} />
                    </Col>
                </Row>

                {this.state.evaluationFormShow && <EvaluationForm
                    showForm={this.state.evaluationFormShow}
                    evaluation={this.state.evaluation}
                    handleChange={this.handleChangeEvaluation}
                    handleClose={this.handleCloseEvaluationForm}
                    handleSave={this.handleSaveEvaluation}
                />}
            </div>
        )
    }
}

export default EvaluationsContainer
