import React, { FunctionComponent } from 'react'

import Table from 'react-bootstrap/Table'
import ButtonGroup from 'react-bootstrap/ButtonGroup'

import { Evaluation } from '../../../common/evaluation'

type renderRowCommandType = (evaluation: Evaluation) => JSX.Element
type renderHeadCommandType = () => JSX.Element
type handleRowOnClickType = (evaluationId: number) => (event: React.MouseEvent) => void

interface EvaluationTableProps {
    evaluations: Evaluation[]
    renderRowCommand: renderRowCommandType
    renderHeadCommand: renderHeadCommandType,
    handleRowOnClick: handleRowOnClickType
}

const renderRow = (evaluation: Evaluation, renderRowCommand: renderRowCommandType, handleRowOnClick: handleRowOnClickType) => {
    return (
        <tr key={evaluation.id} onClick={handleRowOnClick(evaluation.id)}>
            <td>{evaluation.title}</td>
            <td>{evaluation.evaluationCategoryId}</td>
            <td>{evaluation.total}</td>
            <td>{evaluation.displayOrder}</td>
            <td className='command-column'>
                <ButtonGroup aria-label='Evaluation toolbar' size='sm'>
                    {renderRowCommand(evaluation)}
                </ButtonGroup>
            </td>
        </tr>
    )
}

export const EvaluationTable: FunctionComponent<EvaluationTableProps> = ({ evaluations, renderHeadCommand, renderRowCommand, handleRowOnClick }) => (
    <div>
        <Table striped bordered hover>
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Category</th>
                    <th>Total</th>
                    <th>Order</th>
                    <th>{renderHeadCommand()}</th>
                </tr>
            </thead>
            <tbody>
                {evaluations.map(evaluation => renderRow(evaluation, renderRowCommand, handleRowOnClick))}
            </tbody>
        </Table>
    </div>
)
