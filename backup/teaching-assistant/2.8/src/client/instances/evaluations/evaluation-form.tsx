import React from 'react'

import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'

import { FormRow } from '../../component/form-row'
import { Evaluation } from '../../../common/evaluation'

interface EvaluationFormProps {
    showForm: boolean
    handleClose: any
    handleSave: any
    handleChange: (event: React.ChangeEvent<HTMLInputElement>) => void
    evaluation: Evaluation
}

export const EvaluationForm = (props: EvaluationFormProps) => (
    <Modal show={props.showForm} onHide={props.handleClose} >
        <Modal.Header bsPrefix='modal-header' closeButton>
            <Modal.Title>Student</Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <Form>
                <FormRow id='title' label='Title:'>
                    <Form.Control type='text' placeholder='Title' value={props.evaluation.title} onChange={props.handleChange} />
                </FormRow>

                <FormRow id='evaluationCategoryId' label='Category:'>
                    <Form.Control type='text' placeholder='Category' value={props.evaluation.evaluationCategoryId} onChange={props.handleChange} />
                </FormRow>

                <FormRow id='displayOrder' label='Display order:'>
                    <Form.Control type='number' placeholder='Permanent code' value={props.evaluation.displayOrder} onChange={props.handleChange} />
                </FormRow>

                <FormRow id='total' label='Total:'>
                    <Form.Control type='number' placeholder='email@server.com' value={props.evaluation.total} onChange={props.handleChange} />
                </FormRow>
            </Form>
        </Modal.Body>
        <Modal.Footer>
            <Button variant='secondary' onClick={props.handleClose}>Close</Button>
            <Button variant='secondary' onClick={props.handleSave}>Save Changes</Button>
        </Modal.Footer>
    </Modal>
)
