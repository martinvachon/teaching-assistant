import React, { FunctionComponent } from 'react'

import Table from 'react-bootstrap/Table'
import ButtonGroup from 'react-bootstrap/ButtonGroup'

import { Task } from '../../../common/evaluation'

type renderRowCommandType = (task: Task) => JSX.Element
type renderHeadCommandType = () => JSX.Element

interface TaskTableProps {
    tasks: Task[]
    renderRowCommand: renderRowCommandType
    renderHeadCommand: renderHeadCommandType,
}

const renderRow = (task: Task, renderRowCommand: renderRowCommandType) => {
    return (
        <tr key={task.id}>
            <td>{task.title}</td>
            <td>{task.total}</td>
            <td>{task.displayOrder}</td>
            <td className='command-column'>
                <ButtonGroup aria-label='Task toolbar' size='sm'>
                    {renderRowCommand(task)}
                </ButtonGroup>
            </td>
        </tr>
    )
}

export const TaskTable: FunctionComponent<TaskTableProps> = ({ tasks, renderRowCommand, renderHeadCommand }) => (
    <div>
        <Table striped bordered hover>
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Total</th>
                    <th>Order</th>
                    <th>{renderHeadCommand()}</th>
                </tr>
            </thead>
            <tbody>
                {tasks.map(evaluation => renderRow(evaluation, renderRowCommand))}
            </tbody>
        </Table>
    </div>
)
