import React, { FunctionComponent } from 'react'
import {
    Routes,
    Navigate,
    Route,
    useParams
} from 'react-router-dom'

import ExerciceContainer from './exercices'
import InstanceStudentsContainer from './instance-students'
import EvaluationsContainer from './evaluations'
import DashboardContainer from './dashboard'

import { Api } from '../service/api'

// const logoImage = require('../style/image/logo.png')

interface InstancesContainerProps {
    api: Api
    showSummary: boolean,
    showAttendance: boolean,
    showExercice: boolean
}

export const InstancesContainer: FunctionComponent<InstancesContainerProps> = ({ api, showSummary, showAttendance, showExercice }) => {
    const { instanceId } = useParams()
    const id = parseInt(instanceId, 10)
    return (
        <div id='instances'>
            <section>
                <Routes>
                    <Route
                        path='/summary'
                        element={<DashboardContainer
                            api={api}
                            instanceId={id} />} />

                    <Route
                        path='/students'
                        element={<InstanceStudentsContainer
                            api={api}
                            instanceId={id} />} />

                    <Route
                        path='/evaluations'
                        element={<EvaluationsContainer
                            api={api}
                            instanceId={id} />} />

                    <Route
                        path='/exercices'
                        element={<ExerciceContainer
                            instanceId={id} />} />

                    <Route
                        path='*'
                        element={<Navigate to={'./summary'} />} />

                </Routes>
            </section>
        </div>
    )
}

export default InstancesContainer
