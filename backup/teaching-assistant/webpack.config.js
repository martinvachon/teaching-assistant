const HtmlWebPackPlugin = require('html-webpack-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const ManifestPlugin = require('webpack-manifest-plugin')
// const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin')
const TerserJSPlugin = require('terser-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin')
// const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin

const webpack = require('webpack')
const path = require('path')

const PRODUCTION = process.env.NODE_ENV === 'production'

module.exports = {
    entry: {
        app: path.join(__dirname, 'src', 'client', 'index.tsx')
    },

    output: {
        filename: '[name].min.js',
        path: path.join(__dirname, 'dist', 'client'),
        publicPath: '/'
    },

    devServer: {
        contentBase: [path.join(__dirname, 'src', 'client'), path.join(__dirname, 'src', 'player'), path.join(__dirname, 'template')],
        historyApiFallback: true,
        open: true,
        publicPath: '/'
    },

    optimization: {
        minimizer: [new TerserJSPlugin({}), new OptimizeCSSAssetsPlugin({})],
        splitChunks: {
            cacheGroups: {
                vendor: {
                    chunks: 'initial',
                    test: path.resolve(process.cwd(), 'node_modules'),
                    name: 'vendor',
                    enforce: true
                }
            }
        }
    },

    devtool: 'eval-source-map',

    plugins: [
        new HtmlWebPackPlugin({
            template: './src/client/index.html',
            filename: './index.html'
        }),
        new CleanWebpackPlugin(),
        new ManifestPlugin(),
        new ForkTsCheckerWebpackPlugin(),

        new webpack.DefinePlugin({
            __IS_PRODUCTION__: JSON.stringify(PRODUCTION)
        }),
        new MiniCssExtractPlugin({
            filename: '[name].css',
            chunkFilename: '[id].css'
        })
    ],

    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.json'],
        modules: ['node_modules', './src']
    },

    module: {
        rules: [
            {
                test: /\.js$/,
                use: ['source-map-loader'],
                enforce: 'pre'
            },
            {
                test: /\.(ts|tsx)$/,
                use: {
                    loader: 'babel-loader'
                },
                include: [path.resolve(__dirname, 'src')],
                exclude: /node_modules/
            },
            {
                test: /\.tsx?$/,
                loader: 'ts-loader',
                options: {
                    // disable type checker - we will use it in fork plugin
                    transpileOnly: true
                },
                include: [path.resolve(__dirname, 'src')]
            },
            {
                test: /\.html$/,
                use: [
                    {
                        loader: 'html-loader',
                        options: {
                            minimize: false
                        }
                    }
                ]
            },
            {
                test: /\.css$/,
                use: [MiniCssExtractPlugin.loader, 'css-loader']
            },
            {
                test: /\.png$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            limit: 100000
                        }
                    }
                ]
            },
            {
                test: /\.jpg$/,
                use: [
                    {
                        loader: 'file-loader'
                    }
                ]
            },
            {
                test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            limit: 10000,
                            mimetype: 'application/font-woff'
                        }
                    }
                ]
            },
            {
                test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            limit: 10000,
                            mimetype: 'application/octet-stream'
                        }
                    }
                ]
            },
            {
                test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
                use: [
                    {
                        loader: 'file-loader'
                    }
                ]
            },
            {
                test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            limit: 10000,
                            mimetype: 'image/svg+xml'
                        }
                    }
                ]
            }
        ]
    }
}

if (PRODUCTION) {

} else {
    module.exports.plugins.push(
        new webpack.EvalSourceMapDevToolPlugin({})
    )
    // module.exports.plugins.push(
    //     new BundleAnalyzerPlugin({
    //         analyzerMode: 'static',
    //         generateStatsFile: true,
    //         statsOptions: { source: false }
    //     })
    // )
}
