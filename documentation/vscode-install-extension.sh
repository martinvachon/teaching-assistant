###########################################
# VSCode extensions installation script
###########################################


# Viewer and utilities
# -------------------------
code --install-extension hediet.vscode-drawio
code --install-extension tomoki1207.pdf

code --install-extension druideinformatique.antidote

code --install-extension techer.open-in-browser
code --install-extension formulahendry.code-runner

code --install-extension vscode-icons-team.vscode-icons

code --install-extension ms-azuretools.vscode-docker


# Docker extension pack
# -------------------------
# formulahendry.docker-explorer
# ms-azuretools.vscode-docker
code --install-extension formulahendry.docker-extension-pack


# Code format and validation
# -------------------------
code --install-extension redhat.fabric8-analytics
code --install-extension editorconfig.editorconfig
code --install-extension dbaeumer.vscode-eslint
code --install-extension aeschli.vscode-css-formatter
code --install-extension redhat.vscode-xml
code --install-extension umoxfo.vscode-w3cvalidation


# Java extension pack
# -------------------------
# redhat.java
# VisualStudioExptTeam.vscodeintellicode
# vscjava.vscode-java-debug
# vscjava.vscode-java-dependency
# vscjava.vscode-java-test
# vscjava.vscode-maven
code --install-extension vscjava.vscode-java-pack


