import React, { FunctionComponent } from 'react'

import { Link, Navigate, Route, Routes, NavLink } from 'react-router-dom'

import { Templates } from './view/templates'

import PackageJson from '../../package.json'
import { APIError } from './service/api-error'
import { Api } from './service/api'

interface TeachingAssistantProps {

}

export const TeachingAssistant: FunctionComponent<TeachingAssistantProps> = () => {
    const apiTokenCallback = () => {
        return ''
    }

    const apiErrorsCallback = (apiError: APIError) => {
        // this.setState({
        //    apiErrors: [...this.state.apiErrors, apiError]
        // })
    }

    const api = new Api('http://localhost:8080', apiTokenCallback, apiErrorsCallback)

    return (
        <div className='ta'>
            <header>
                <div className='container'>
                    <div className='logo'>
                        <Link to="/templates">
                            <div></div>
                            <div>Teaching assistant</div>
                        </Link>
                    </div>
                    <nav>
                        <ul>
                            <li><NavLink to="./templates">Templates</NavLink></li>
                            <li><NavLink to='/students'>Students</NavLink></li>
                        </ul>
                    </nav>
                </div>
            </header>
            <main>
                <Routes>

                    <Route path='/templates/*' element={<Templates api={api} />} />

                    <Route
                        path='*'
                        element={<Navigate to='/templates' />} />

                </Routes>
            </main>
            <footer>
                <div className='container'>
                    <div>
                        <span>Version: </span> {PackageJson.version}
                    </div>
                </div>
            </footer>
        </div>
    )
}
