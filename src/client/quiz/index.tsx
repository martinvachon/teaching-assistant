import React from 'react'
import { MultipleChoiceQuestion } from './component/multiple-choice-question'
import { SingleChoiceQuestion } from './component/single-choice-question'
import { TrueFalseQuestion } from './component/true-false-question'

import { Quiz } from './model'

interface QuizProps {
}

interface QuizState {
    quiz: Quiz
    questionIndex: number
    answers: any
}

class QuizContainer extends React.Component<QuizProps, QuizState> {
    constructor (props: QuizProps) {
        super(props)

        this.state = {
            quiz: {
                course: 'Loading ...',
                title: '',
                questions: []
            },
            questionIndex: 0,
            answers: {}
        }
    }

    componentDidMount (): void {
        fetch('http://localhost:8081/quiz/quiz.json')
            .then(response => {
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                return response.json()
            }).then(quiz => {
                this.setState({
                    quiz
                })
            })
    }

    renderQuestion = (question) => {
        const answer = this.state.answers[question.name]
        let component
        switch (question.type) {
            case 'SINGLE_CHOICE':
                component = <SingleChoiceQuestion question={question} userAnswer={answer} onChange={this.handleChange} />
                break
            case 'MULTIPLE_CHOICE':
                component = <MultipleChoiceQuestion question={question} userAnswers={answer} onChange={this.handleChangeMultiple} />
                break
            case 'TRUE_FALSE':
                component = <TrueFalseQuestion question={question} userAnswer={answer} onChange={this.handleChange} />
                break
            default:
                break
        }

        return component
    }

    handlePreviousClick = () => {
        this.setState({
            questionIndex: this.state.questionIndex - 1
        })
    }

    handleNextClick = () => {
        this.setState({
            questionIndex: this.state.questionIndex + 1
        })
    }

    handleChange = (event) => {
        this.setState({
            answers: {
                ...this.state.answers,
                [event.target.name]: event.target.value
            }
        })
    }

    handleChangeMultiple = (event: React.ChangeEvent<HTMLInputElement>) => {
        console.log(event.target.checked)

        const answers = this.state.answers[event.target.name]
        if (answers) {
            if (event.target.checked) {
                this.setState({
                    answers: {
                        ...this.state.answers,
                        [event.target.name]: [
                            ...answers,
                            event.target.value
                        ]
                    }
                })
            } else {
                this.setState({
                    answers: {
                        ...this.state.answers,
                        [event.target.name]: answers.filter(answer => answer !== event.target.value)
                    }
                })
            }
        } else {
            this.setState({
                answers: {
                    ...this.state.answers,
                    [event.target.name]: [
                        event.target.value
                    ]
                }
            })
        }
    }

    handleSubmit = () => {
        alert('send')
    }

    render () {
        const { questionIndex, quiz } = this.state
        const question = quiz.questions[questionIndex]
        return (
            <div>
                <h1>{quiz.course}</h1>
                <h2>{quiz.title}</h2>

                {question && this.renderQuestion(question)}

                <button disabled={questionIndex === 0} onClick={this.handlePreviousClick}>&lt;&lt; Previous</button>
                <button disabled={questionIndex === (quiz.questions.length - 1)} onClick={this.handleNextClick}>Next &gt;&gt;</button>

                {quiz.questions.length === Object.keys(this.state.answers).length && <button onClick={this.handleSubmit}>Submit</button>}

                <div>
                    <h2>Answers</h2>
                    <pre>
                        {JSON.stringify(this.state.answers, null, 4)}
                    </pre>
                </div>
            </div>
        )
    }
}

export default QuizContainer
