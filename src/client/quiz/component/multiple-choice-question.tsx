import React, { FunctionComponent } from 'react'

import { Answer, Question } from '../model'
import { QuestionLayout } from './question-layout'

interface MultipleChoiceQuestionProps {
    question: Question,
    userAnswers: string[]
    onChange: (event: React.ChangeEvent<HTMLInputElement>) => void
}

export const MultipleChoiceQuestion: FunctionComponent<MultipleChoiceQuestionProps> = ({ question, userAnswers, onChange }) => {
    const renderAnswer = (answer: Answer, index: number) => {
        const id = question.name + '_answerIndex-' + index
        let checked = false
        if (userAnswers && userAnswers.includes(id)) {
            checked = true
        }
        return (
            <li key={index}>
                <input type="checkbox" id={id} value={id} name={question.name} onChange={onChange} checked={checked} />
                <label htmlFor={id}>{answer.text}</label>
            </li>
        )
    }
    return (
        <div className='multiple-choice-question'>
            <QuestionLayout question={question}>
                {question.answers.map((answer, index) => renderAnswer(answer, index))}
            </QuestionLayout>
        </div>
    )
}
