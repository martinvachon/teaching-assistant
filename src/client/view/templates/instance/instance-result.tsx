import React, { FunctionComponent } from 'react'

import { Api } from '../../../service/api'

interface InstanceResultProps {
    api: Api
}

export const InstanceResult: FunctionComponent<InstanceResultProps> = ({ api }) => {
    return (
        <div>instance result</div>
    )
}
