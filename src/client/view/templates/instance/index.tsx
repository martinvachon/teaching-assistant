import React, { FunctionComponent } from 'react'

import { Navigate, Route, Routes } from 'react-router-dom'

import { Api } from '../../../service/api'
import { InstanceEditor } from './instance-editor'
import { InstanceResult } from './instance-result'

interface InstanceProps {
    api: Api
}

export const Templates: FunctionComponent<InstanceProps> = ({ api }) => {
    return (
        <div className='instance'>
            <Routes>

                <Route path='/' element={<InstanceResult api={api} />} />

                <Route path='/:instanceCode' element={<InstanceEditor api={api} />} />

                <Route
                    path='*'
                    element={<Navigate to='/' />} />

            </Routes>
        </div>
    )
}
