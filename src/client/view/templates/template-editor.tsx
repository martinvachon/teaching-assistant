import React, { FunctionComponent, useEffect, useState } from 'react'

import { Link, useNavigate, useParams } from 'react-router-dom'

import { Evaluation, Task, Template } from '../../../common/template'
import { Api } from '../../service/api'

interface TemplateEditorProps {
    api: Api
}

const DEFAULT_TEMPLATE_STATE = {
    code: '',
    title: '',
    folder: '',
    evaluations: []
}

export const TemplateEditor: FunctionComponent<TemplateEditorProps> = ({ api }) => {
    const [template, setTemplate] = useState<Template>(DEFAULT_TEMPLATE_STATE)
    const { templateCode } = useParams()
    const navigate = useNavigate()

    const loadTemplate = () => {
        api.get<Template>('/templates/' + templateCode, (response) => {
            setTemplate(response)
        })
    }

    const handleSaveTemplate = () => {
        api.post<Template, Template[]>('/templates', template, (templates) => {
            console.log('response before navigate')

            navigate('../')
        })
    }

    useEffect(() => {
        loadTemplate()
    }, [])

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setTemplate({
            ...template,
            [event.target.name]: event.target.value
        })
    }

    const changePosition = (initialArray: any[], initialPosition: number, finalPosition: number) => {
        const length = initialArray.length
        if (initialPosition < 0 || initialPosition > length || finalPosition < 0 || finalPosition >= length) {
            throw Error('Invalid position request')
        }

        const clonedArray = [...initialArray]
        const removeElement = clonedArray.splice(initialPosition, 1)

        clonedArray.splice(finalPosition, 0, removeElement[0])

        return clonedArray
    }

    const handleEvaluationChange = (index: number) => (event: React.ChangeEvent<HTMLInputElement>) => {
        const newTemplate = { ...template }

        newTemplate.evaluations[index] = {
            ...template.evaluations[index],
            [event.target.name]: event.target.value
        }

        setTemplate(newTemplate)
    }

    const handleTaskChange = (evaluationIndex: number, taskIndex: number) => (event: React.ChangeEvent<HTMLInputElement>) => {
        const newTemplate = { ...template }
        newTemplate.evaluations[evaluationIndex].tasks[taskIndex] = {
            ...template.evaluations[evaluationIndex].tasks[taskIndex],
            [event.target.name]: event.target.value
        }

        setTemplate(newTemplate)
    }

    const handleEvaluationAdd = (event: React.MouseEvent) => {
        const newTemplate = { ...template }

        newTemplate.evaluations.push({
            title: '',
            total: 0,
            tasks: []
        })

        setTemplate(newTemplate)
    }

    const handleEvaluationDelete = (evaluationIndex: number) => (event: React.MouseEvent) => {
        const newTemplate = { ...template }

        newTemplate.evaluations.splice(evaluationIndex, 1)

        setTemplate(newTemplate)
    }

    const handleEvaluationMove = (evaluationIndex: number, destinationIndex: number) => (event: React.MouseEvent) => {
        const newTemplate = { ...template }

        newTemplate.evaluations = changePosition(template.evaluations, evaluationIndex, destinationIndex)

        setTemplate(newTemplate)
    }

    const renderTask = (task: Task, evaluationIndex: number, taskIndex: number) => {
        return (
            <tr key={taskIndex}>
                <td><input type="text" name="name" value={task.name} onChange={handleTaskChange(evaluationIndex, taskIndex)} /></td>
                <td><input type="text" name="value" value={task.value} onChange={handleTaskChange(evaluationIndex, taskIndex)} /></td>
            </tr>
        )
    }

    const renderEvaluationToolbar = (evaluationIndex: number) => {
        return (
            <ul>
                <li>
                    {(evaluationIndex > 0 ? <a onClick={handleEvaluationMove(evaluationIndex, evaluationIndex - 1)}>Up</a> : <span>Up</span>)}
                </li>
                <li>
                    <a onClick={handleEvaluationDelete(evaluationIndex)}>Delete</a>
                </li>
                <li>
                    {(evaluationIndex + 1 < template.evaluations.length ? <a onClick={handleEvaluationMove(evaluationIndex, evaluationIndex + 1)}>Down</a> : <span>Down</span>)}
                </li>
            </ul>
        )
    }

    const renderEvaluationForm = (evaluation: Evaluation, evaluationIndex: number) => {
        return (
            <li key={evaluationIndex}>

                {renderEvaluationToolbar(evaluationIndex)}

                <div>
                    <label htmlFor="evaluation.title">Title</label>
                    <input type="text" id="evaluationTitle" name="title" value={evaluation.title} onChange={handleEvaluationChange(evaluationIndex)} />
                </div>
                <div>
                    <label htmlFor="evaluation.total">Total</label>
                    <input type="text" id="evaluationTotal" name="total" value={evaluation.total} onChange={handleEvaluationChange(evaluationIndex)} />
                </div>

                <h3>Task</h3>
                <ul>
                    <table>
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Value</th>
                            </tr>
                        </thead>
                        <tbody>
                            {evaluation.tasks.map((task, taskIndex) => renderTask(task, evaluationIndex, taskIndex))}
                        </tbody>
                    </table>
                </ul>
            </li>
        )
    }

    return (
        <div className='template-editor'>

            <Link to='../'>list</Link>

            <h3>Template</h3>
            <div>
                <label htmlFor="title">Title</label>
                <input type="text" name="title" id="title" value={template.title} onChange={handleChange} />
            </div>
            <div>
                <label htmlFor="code">Code</label>
                <input type="text" name="code" id="code" value={template.code} onChange={handleChange} />
            </div>
            <div>
                <label htmlFor="templateFolder">Folder</label>
                <input type="text" name="folder" id="templateFolder" value={template.folder} onChange={handleChange} />
            </div>
            <h3>Evaluations</h3>
            <a onClick={handleEvaluationAdd}>Add</a>
            <ul>
                {template.evaluations.map((evaluation, index) => renderEvaluationForm(evaluation, index))}
            </ul>

            <button onClick={handleSaveTemplate}>Save</button>
        </div>
    )
}
