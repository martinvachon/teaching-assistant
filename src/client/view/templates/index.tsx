import React, { FunctionComponent } from 'react'

import { Navigate, Route, Routes } from 'react-router-dom'
import { Api } from '../../service/api'

import { TemplateEditor } from './template-editor'
import { TemplateList } from './template-list'

interface TemplatesProps {
    api: Api
}

export const Templates: FunctionComponent<TemplatesProps> = ({ api }) => {
    return (
        <div className='templates'>
            <Routes>

                <Route path='/' element={<TemplateList api={api} />} />

                <Route path='/:templateCode' element={<TemplateEditor api={api} />} />

                <Route
                    path='*'
                    element={<Navigate to='/' />} />

            </Routes>
        </div>
    )
}
