import React, { FunctionComponent, useEffect, useState } from 'react'

import { Link } from 'react-router-dom'

import { Api } from '../../service/api'

import { TemplateDto } from '../../../common/dto'
import { Instance } from '../../../common/instance'

interface TemplateListProps {
    api: Api
}

export const TemplateList: FunctionComponent<TemplateListProps> = ({ api }) => {
    const [templateDtos, setTemplateDtos] = useState<TemplateDto[]>([])

    const loadTemplateDtos = () => {
        api.get<TemplateDto[]>('/templates', (response) => {
            setTemplateDtos(response)
        })
    }

    useEffect(() => {
        loadTemplateDtos()
    }, [])

    const renderInstance = (instance: Instance) => {
        return (
            <li key={instance.session}>{instance.session}</li>
        )
    }

    const renderTemplate = (templateDto: TemplateDto) => {
        const template = templateDto.template
        return (
            <li key={template.code}>
                <div>{template.code}</div>
                <div>{template.title}</div>

                <ul>
                    {templateDto.instances.map(instance => renderInstance(instance))}
                </ul>

                <ul className='toolbar'>
                    <li><Link to={'./' + template.code}>Edit</Link></li>
                </ul>
            </li>
        )
    }

    return (
        <div className='template-list'>
            template list

            {templateDtos.map(templateDto => renderTemplate(templateDto))}

        </div>
    )
}
