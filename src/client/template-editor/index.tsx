import React from 'react'

import { Evaluation, Task, TemplateConfiguration } from './model'

interface TemplateEditorProps {
}

interface TemplateEditorState {
    templateConfigurations: TemplateConfiguration[]
    templateIndex: number
}

class TemplateEditor extends React.Component<TemplateEditorProps, TemplateEditorState> {
    constructor (props: TemplateEditorProps) {
        super(props)

        this.state = {
            templateConfigurations: [],
            templateIndex: -1
        }
    }

    componentDidMount (): void {
        fetch('http://localhost:8081/template-editor/ta-configuration.json')
            .then(response => {
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                return response.json()
            }).then(taConfiguration => {
                this.setState({
                    templateConfigurations: taConfiguration.templateConfigurations
                })
            })
    }

    componentDidUpdate (prevProps: Readonly<TemplateEditorProps>, prevState: Readonly<TemplateEditorState>, snapshot?: any): void {
        console.log('saving data')
    }

    changePosition = (initialArray: any[], initialPosition: number, finalPosition: number) => {
        const length = initialArray.length
        if (initialPosition < 0 || initialPosition > length || finalPosition < 0 || finalPosition >= length) {
            throw Error('Invalid position request')
        }

        const clonedArray = [...initialArray]
        const removeElement = clonedArray.splice(initialPosition, 1)

        clonedArray.splice(finalPosition, 0, removeElement[0])

        return clonedArray
    }

    handleFolderChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const newTemplateConfigurations = [...this.state.templateConfigurations]

        newTemplateConfigurations[this.state.templateIndex] = {
            ...this.state.templateConfigurations[this.state.templateIndex],
            folder: event.target.value
        }

        this.setState({
            templateConfigurations: newTemplateConfigurations
        })
    }

    handleTemplateChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const newTemplateConfigurations = [...this.state.templateConfigurations]

        newTemplateConfigurations[this.state.templateIndex] = {
            ...this.state.templateConfigurations[this.state.templateIndex],
            template: {
                ...this.state.templateConfigurations[this.state.templateIndex].template,
                [event.target.name]: event.target.value
            }
        }

        this.setState({
            templateConfigurations: newTemplateConfigurations
        })
    }

    handleEvaluationChange = (index: number) => (event: React.ChangeEvent<HTMLInputElement>) => {
        const newTemplateConfigurations = [...this.state.templateConfigurations]

        newTemplateConfigurations[this.state.templateIndex].template.evaluations[index] = {
            ...newTemplateConfigurations[this.state.templateIndex].template.evaluations[index],
            [event.target.name]: event.target.value
        }

        this.setState({
            templateConfigurations: newTemplateConfigurations
        })
    }

    handleTaskChange = (evaluationIndex: number, taskIndex: number) => (event: React.ChangeEvent<HTMLInputElement>) => {
        const newTemplateConfigurations = [...this.state.templateConfigurations]

        newTemplateConfigurations[this.state.templateIndex].template.evaluations[evaluationIndex].tasks[taskIndex] = {
            ...newTemplateConfigurations[this.state.templateIndex].template.evaluations[evaluationIndex].tasks[taskIndex],
            [event.target.name]: event.target.value
        }

        this.setState({
            templateConfigurations: newTemplateConfigurations
        })
    }

    handleEvaluationAdd = (event: React.MouseEvent) => {
        const newTemplateConfigurations = [...this.state.templateConfigurations]

        const newEvaluations = [...this.state.templateConfigurations[this.state.templateIndex].template.evaluations]
        newEvaluations.push({
            title: '',
            total: 0,
            tasks: [],
            teams: []
        })

        newTemplateConfigurations[this.state.templateIndex].template.evaluations = newEvaluations

        this.setState({
            templateConfigurations: newTemplateConfigurations
        })
    }

    handleEvaluationDelete = (evaluationIndex: number) => (event: React.MouseEvent) => {
        const newTemplateConfigurations = [...this.state.templateConfigurations]

        const newEvaluations = [...this.state.templateConfigurations[this.state.templateIndex].template.evaluations]
        newEvaluations.splice(evaluationIndex, 1)

        newTemplateConfigurations[this.state.templateIndex].template.evaluations = newEvaluations

        this.setState({
            templateConfigurations: newTemplateConfigurations
        })
    }

    // https://dev.to/andyrewlee/cheat-sheet-for-updating-objects-and-arrays-in-react-state-48np

    handleEvaluationMove = (evaluationIndex: number, destinationIndex: number) => (event: React.MouseEvent) => {
        const newTemplateConfigurations = [...this.state.templateConfigurations]

        newTemplateConfigurations[this.state.templateIndex].template.evaluations = this.changePosition(
            this.state.templateConfigurations[this.state.templateIndex].template.evaluations,
            evaluationIndex,
            destinationIndex)

        this.setState({
            templateConfigurations: newTemplateConfigurations
        })
    }

    handleSubmit = () => {
        alert('send')
    }

    handleEditTemplate = (index: number) => (event: React.MouseEvent) => {
        this.setState({
            templateIndex: index
        })
    }

    renderTask = (task: Task, evaluationIndex: number, taskIndex: number) => {
        return (
            <tr key={taskIndex}>
                <td><input type="text" name="name" value={task.name} onChange={this.handleTaskChange(evaluationIndex, taskIndex)} /></td>
                <td><input type="text" name="value" value={task.value} onChange={this.handleTaskChange(evaluationIndex, taskIndex)} /></td>
            </tr>
        )
    }

    renderEvaluationToolbar = (evaluationIndex: number) => {
        const evaluationLength = this.state.templateConfigurations[this.state.templateIndex].template.evaluations.length
        return (
            <ul>
                <li>{(evaluationIndex > 0 ? <a onClick={this.handleEvaluationMove(evaluationIndex, evaluationIndex - 1)}>Up</a> : <span>Up</span>)}</li>
                <li>
                    <a onClick={this.handleEvaluationDelete(evaluationIndex)}>Delete</a>
                </li>
                <li>{(evaluationIndex + 1 < evaluationLength ? <a onClick={this.handleEvaluationMove(evaluationIndex, evaluationIndex + 1)}>Down</a> : <span>Down</span>)}</li>
            </ul>
        )
    }

    renderEvaluationForm = (evaluation: Evaluation, evaluationIndex: number) => {
        return (
            <li key={evaluationIndex}>

                {this.renderEvaluationToolbar(evaluationIndex)}

                <div>
                    <label htmlFor="evaluation.title">Title</label>
                    <input type="text" id="evaluationTitle" name="title" value={evaluation.title} onChange={this.handleEvaluationChange(evaluationIndex)} />
                </div>
                <div>
                    <label htmlFor="evaluation.total">Total</label>
                    <input type="text" id="evaluationTotal" name="total" value={evaluation.total} onChange={this.handleEvaluationChange(evaluationIndex)} />
                </div>

                <h3>Task</h3>
                <ul>
                    <table>
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Value</th>
                            </tr>
                        </thead>
                        <tbody>
                            {evaluation.tasks.map((task, taskIndex) => this.renderTask(task, evaluationIndex, taskIndex))}
                        </tbody>
                    </table>
                </ul>
            </li>
        )
    }

    renderTemplateForm = (templateConfiguration: TemplateConfiguration) => {
        return (
            <form action="">
                <h3>Template</h3>
                <div>
                    <label htmlFor="title">Title</label>
                    <input type="text" name="title" id="title" value={templateConfiguration.template.title} onChange={this.handleTemplateChange} />
                </div>
                <div>
                    <label htmlFor="code">Code</label>
                    <input type="text" name="code" id="code" value={templateConfiguration.template.code} onChange={this.handleTemplateChange} />
                </div>
                <div>
                    <label htmlFor="templateFolder">Folder</label>
                    <input type="text" name="folder" id="templateFolder" value={templateConfiguration.folder} onChange={this.handleFolderChange} />
                </div>
                <h3>Evaluations</h3>
                <a onClick={this.handleEvaluationAdd}>Add</a>
                <ul>
                    {templateConfiguration.template.evaluations.map((evaluation, index) => this.renderEvaluationForm(evaluation, index))}
                </ul>
            </form>
        )
    }

    render () {
        return (
            <div>
                <h1>Template editor</h1>
                <ul>
                    {this.state.templateConfigurations.map((templateConfiguration, index) => <li key={index} onClick={this.handleEditTemplate(index)}>{templateConfiguration.template.title}</li>)}
                </ul>

                {this.state.templateIndex !== -1 && this.renderTemplateForm(this.state.templateConfigurations[this.state.templateIndex])}
            </div>
        )
    }
}

export default TemplateEditor
