import React from 'react'
import { createRoot } from 'react-dom/client'
import { BrowserRouter } from 'react-router-dom'

import './style/css/teaching-assistant.css'
import { TeachingAssistant } from './teaching-assistant'

const container = document.getElementById('app')
const root = createRoot(container!)
root.render(
    <React.StrictMode>
        <BrowserRouter>
            <TeachingAssistant />
        </BrowserRouter>
    </React.StrictMode>
)
