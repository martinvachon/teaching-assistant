import React, { FunctionComponent, useState } from 'react'

import { IconProp } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

interface DropdownProps {
    icon?: IconProp
    label: string
    children: React.ReactNode
}

export const Dropdown: FunctionComponent<DropdownProps> = ({ icon, label, children }) => {
    const [open, setOpen] = useState(false)

    return (
        <div className={'dropdown ' + (open ? 'open' : 'close')} onClick={() => setOpen(!open)}>
            <span>{icon && <FontAwesomeIcon icon={icon} />} {label}</span>
            <ul className={open ? 'open' : 'close'}>
                {children}
            </ul>
        </div>
    )
}
