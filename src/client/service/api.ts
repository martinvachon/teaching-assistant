import { Entity } from './entity'
import { APIError } from './api-error'

export interface FetchAllParameter {
    endpoint: string
    method: 'get' | 'put' | 'post' | 'delete'
    body?: any
}

export class Api {
    private baseUrl: string
    private errorCallback: (apiError: APIError) => void
    private tokenCallback: () => string | undefined

    constructor (baseUrl: string, tokenCallback: () => string | undefined, errorCallback: (apiError: APIError) => void) {
        this.baseUrl = baseUrl
        this.errorCallback = errorCallback
        this.tokenCallback = tokenCallback
    }

    private buildHeaders () {
        const requestHeaders: HeadersInit = new Headers()
        requestHeaders.set('Content-Type', 'application/json')

        const token = this.tokenCallback()
        if (token) {
            requestHeaders.set('Authorization', token)
        }

        return requestHeaders
    }

    private async parserCallback (response: Response) {
        if (!response.ok) {
            const json = await response.json().then((json) => json)
            throw new APIError(json.message, response.status, response.statusText)
        }
        return response.json()
    }

    private executeFetch = (endpoint: string, method: 'get' | 'put' | 'post' | 'delete', successCallback: (result: any) => void, body?: any) => {
        fetch(this.baseUrl + endpoint, {
            method,
            headers: this.buildHeaders(),
            body: body ? JSON.stringify(body) : undefined
        })
            .then(this.parserCallback)
            .then(successCallback)
            .catch(this.errorCallback)
    }

    async fetchAll (fetchAllParameters: FetchAllParameter[], successCallback: (result: any) => void) {
        const requests = fetchAllParameters.map(fetchAllParameter => fetch(this.baseUrl + fetchAllParameter.endpoint, {
            method: fetchAllParameter.method,
            headers: this.buildHeaders(),
            body: fetchAllParameter.body ? JSON.stringify(fetchAllParameter.body) : undefined
        }).then(this.parserCallback))

        Promise.allSettled(requests).then(successCallback).catch(this.errorCallback)
    }

    get<E> (endpoint: string, successCallback: (result: E) => void) {
        this.executeFetch(endpoint, 'get', successCallback)
    }

    save (endpoint: string, body: Entity, successCallback: (result: any) => void) {
        if (body.id !== '') {
            this.executeFetch(endpoint + body.id, 'put', successCallback, body)
        } else {
            this.executeFetch(endpoint, 'post', successCallback, body)
        }
    }

    put (endpoint: string, body: any, successCallback: (result: any) => void) {
        this.executeFetch(endpoint, 'put', successCallback, body)
    }

    post<S, R> (endpoint: string, body: S, successCallback: (result: R) => void) {
        this.executeFetch(endpoint, 'post', successCallback, body)
    }

    delete (endpoint: string, successCallback: (result: any) => void) {
        this.executeFetch(endpoint, 'delete', successCallback)
    }
}
