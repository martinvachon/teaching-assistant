export class APIError {
    private message: string
    private status: number
    private statusText: string

    constructor (message: string, status: number, statuText: string) {
        this.message = message
        this.status = status
        this.statusText = statuText
    }

    getMessage = () => {
        return this.message
    }
}
