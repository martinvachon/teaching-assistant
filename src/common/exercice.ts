
export default interface Exercice {
    name: string
    question: boolean
    solution: boolean
}
