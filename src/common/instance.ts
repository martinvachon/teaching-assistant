
export interface InstanceTask {
    id: string
    name: string
}

export interface InstanceEvaluation {
    id: string
    title: string
    total: number
    instanceTasks: InstanceTask[]
}

export interface Instance {
    session: string
    code: string
    title: string
    folder: string
    instanceEvaluations: InstanceEvaluation[]
}
