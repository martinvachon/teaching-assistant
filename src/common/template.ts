
export interface Task {
    name: string
    value: number
}

export interface Evaluation {
    title: string
    total: number
    tasks: Task[]
}

export interface Template {
    code: string
    title: string
    folder: string
    evaluations: Evaluation[]
}
