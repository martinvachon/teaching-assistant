import { Entity } from './entity'

export interface Student extends Entity {
    firstName: string
    lastName: string
    permanentCode: string
    email: string
}
