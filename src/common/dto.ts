import { Instance } from './instance'
import { Template } from './template'

export interface TemplateDto {
    template: Template
    instances: Instance[]
}
