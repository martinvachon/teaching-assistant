import { Entity } from './entity'
import { Student } from './student'

export interface Evaluation extends Entity {
    courseInstanceId: number
    evaluationCategoryId: number
    title: string
    total: number
    displayOrder: number
}

export interface Task extends Entity {
    evaluationId: number,
    title: string,
    total: number,
    displayOrder: number
}

export interface TaskResult {
    taskId: number
    studentId: number
    studentResult: number
}

export interface SummaryResult {
    evaluationId: number
    studentId: number
    taskId: number
    total: number
}

export type ResultType = { [x: string]: SummaryResult }

export interface Summary {
    evaluations: Evaluation[]
    students: Student[]
    results: ResultType[]
}
