import { Summary, SummaryResult, ResultType, TaskResult } from '../../../common/evaluation'
import { Dao } from '../../service/database/dao'
import { InstanceStudentService } from './instance-student-service'
import { EvaluationService } from './evaluation-service'

export class SummaryService {
    private dao: Dao
    private instanceStudentsService: InstanceStudentService
    private evaluationService: EvaluationService

    constructor (dao: Dao, instanceStudentsService: InstanceStudentService, evaluationService: EvaluationService) {
        this.dao = dao
        this.instanceStudentsService = instanceStudentsService
        this.evaluationService = evaluationService
    }

    async getSummaryResult (instanceId: number): Promise<ResultType[]> {
        const sql = 'SELECT * FROM v_task_result_summary WHERE courseInstanceId = ?'

        const summaries = await this.dao.getAll<SummaryResult, SummaryResult>(sql, [instanceId], (row) => {
            return {
                evaluationId: row.evaluationId,
                studentId: row.studentId,
                taskId: row.taskId,
                total: row.total
            }
        })

        const result: any = {}
        summaries.forEach(summary => {
            result[summary.evaluationId + '-' + summary.studentId] = summary
        })

        return result
    }

    async getSummary (instanceId: number): Promise<Summary> {
        return {
            evaluations: await this.evaluationService.getAll(instanceId),
            students: await this.instanceStudentsService.getAll(instanceId, 'firstName', 'DESC'),
            results: await this.getSummaryResult(instanceId)
        }
    }

    async saveResult ({ taskId, studentId, studentResult }: TaskResult): Promise<number> {
        let sql = 'INSERT INTO taskResult (taskId, studentId, studentResult) '
        sql += 'VALUES (?, ?, ?) ON CONFLICT (taskId, studentId) DO '
        sql += 'UPDATE SET studentResult = ?;'

        const test = await this.dao.execute(sql, [taskId, studentId, studentResult, studentResult])
        console.log(test)

        return test
    }
}
