import { Evaluation } from '../../../common/evaluation'
import { AbstractCRUDService, CRUDService } from '../../service/crud'
import { Dao } from '../../service/database/dao'

const EVALUATION_TABLE = 'evaluation'

export class EvaluationService extends AbstractCRUDService<Evaluation, Evaluation> implements CRUDService<Evaluation, Evaluation> {
    constructor (dao: Dao) {
        super(dao, [], 'evaluation', 'displayOrder')
    }

    async getAll (instanceId: number) {
        const sql = 'SELECT * FROM ' + EVALUATION_TABLE + ' WHERE courseInstanceId = ? ORDER BY displayOrder'

        return await this.dao.getAll<Evaluation, Evaluation>(sql, [instanceId], (row) => {
            return {
                id: row.id,
                courseInstanceId: row.courseInstanceId,
                evaluationCategoryId: row.evaluationCategoryId,
                title: row.title,
                total: row.total,
                displayOrder: row.displayOrder
            }
        })
    }
}
