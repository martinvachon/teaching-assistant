import { Student } from '../../../common/student'
import { Dao } from '../../service/database/dao'

const INSTANCE_STUDENTS_VIEW = 'v_instanceStudents'

export class InstanceStudentService {
    private dao: Dao

    constructor (dao: Dao) {
        this.dao = dao
    }

    async getAll (instanceId: number, field: string, order: string) {
        const sql = `SELECT * FROM ${INSTANCE_STUDENTS_VIEW} WHERE courseInstanceId = ? ORDER BY ${field} ${order.toUpperCase()}`

        return await this.dao.getAll<Student, Student>(sql, [instanceId], (row) => {
            return {
                id: row.id,
                firstName: row.firstName,
                lastName: row.lastName,
                permanentCode: row.permanentCode,
                email: row.email
            }
        })
    }
}
