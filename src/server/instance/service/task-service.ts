import { Task } from '../../../common/evaluation'
import { AbstractCRUDService, CRUDService } from '../../service/crud'
import { Dao } from '../../service/database/dao'

const TASK_TABLE = 'task'

export class TaskService extends AbstractCRUDService<Task, Task> implements CRUDService<Task, Task> {
    constructor (dao: Dao) {
        super(dao, [], 'task', 'displayOrder')
    }

    async getAll (evaluationId: number) {
        const sql = 'SELECT * FROM ' + TASK_TABLE + ' WHERE evaluationId = ? ORDER BY displayOrder'

        return await this.dao.getAll<Task, Task>(sql, [evaluationId], (row) => {
            return {
                id: row.id,
                evaluationId: row.evaluationId,
                title: row.title,
                total: row.total,
                displayOrder: row.displayOrder
            }
        })
    }
}
