import express from 'express'
import { ReasonPhrases, StatusCodes } from 'http-status-codes'

import { HttpException } from '../error'
import { CRUDService } from './crud-service'

// TODO
// require('express-async-errors')

export function crudRouterBuilder<E, T extends CRUDService<E, T>> (service: T, defaultSortField: string) {
    const router = express.Router()

    router.get('/', async (request, response) => {
        const field = request.query.field as string || defaultSortField
        const order = request.query.order as string || 'desc'

        const result = await service.all(field, order)
        response.status(StatusCodes.OK).send(result)
    })

    router.get('/:id', (request, response, next) => {
        service.one(parseInt(request.params.id, 10))
            .then(result => response.status(StatusCodes.OK).send(result))
            .catch(() => next(new HttpException(StatusCodes.NOT_FOUND, ReasonPhrases.NOT_FOUND)))
    })

    router.post('/', async (request, response) => {
        const result = await service.insert(request.body)
        response.status(StatusCodes.OK).send(result)
    })

    router.put('/', async (request, response) => {
        const result = await service.update(request.body)
        response.status(StatusCodes.OK).send(result)
    })

    router.delete('/:id', async (request, response) => {
        const result = await service.delete(parseInt(request.params.id, 10))
        response.status(StatusCodes.OK).send(result)
    })

    return router
}
