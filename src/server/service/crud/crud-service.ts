export interface CRUDService<E, T> {
    all (field: string, order: string): Promise<E[] | T[]>
    one (id: number): Promise<E | T>
    insert (inventory: E): Promise<E[] | T[]>
    update (inventory: E): Promise<E[] | T[]>
    delete (id: number): Promise<E[] | T[]>
}
