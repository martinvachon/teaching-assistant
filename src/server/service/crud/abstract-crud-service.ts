import { Entity } from '../../../common/entity'
import { Dao } from '../database/dao'
import { CRUDService } from './crud-service'

const ORDERS = ['asc', 'desc']

export abstract class AbstractCRUDService<E extends Entity, T> implements CRUDService<E, T> {
    protected dao: Dao
    protected fieldNames: string[]
    protected tableName: string
    protected defaultSortField: string
    protected defaultSortOrder: 'asc' | 'desc'

    constructor (dao: Dao, fieldNames: string[], tableName: string, defaultSortField: string, defaultSortOrder?: 'asc' | 'desc') {
        this.dao = dao
        this.fieldNames = fieldNames
        this.tableName = tableName
        this.defaultSortField = defaultSortField
        this.defaultSortOrder = (defaultSortOrder || 'asc')
    }

    async all (field: string, order: string, rowMapper?: (row: E) => T) {
        let sortField = this.defaultSortField
        if (this.fieldNames.includes(field)) {
            sortField = field
        }

        if (!ORDERS.includes(order)) {
            throw new Error('Invalid order parmeter: ' + order)
        }

        const sql = `SELECT * FROM ${this.tableName} ORDER BY ${sortField} ${order.toUpperCase()}`

        return this.dao.getAll(sql, [], rowMapper)
    }

    one (id: number, rowMapper?: (row: E) => T) {
        // // Runtime validation
        // const parsedId = parseInt(id, 10)
        // if (isNaN(parsedId)) {
        //     throw new Error('Invalid id:' + id)
        // }

        const sql = `SELECT * FROM ${this.tableName} WHERE id = ${id}`
        return this.dao.getOne(sql, [], rowMapper)
    }

    delete (id: number, rowMapper?: (row: E) => T) {
        const sql = `DELETE FROM ${this.tableName} WHERE id = ?`

        return this.dao.execute(sql, [id]).then(() => this.all(this.defaultSortField, this.defaultSortOrder, rowMapper))
    }

    update (entity: Partial<E>, rowMapper?: (row: E) => T) {
        const { id, ...rest } = entity

        const sql = `UPDATE ${this.tableName} SET ${Object.keys(rest).join(' = ?, ') + ' = ? '} WHERE id = ?`

        const values = [
            ...Object.values(rest),
            id
        ]

        return this.dao.execute(sql, values).then(() => this.all(this.defaultSortField, this.defaultSortOrder, rowMapper))
    }

    insert (entity: Partial<E>) {
        const keys = Object.keys(entity)

        const sql = `INSERT INTO ${this.tableName} (${keys.join(', ')}) VALUES (${keys.fill('?').join(', ')})`

        return this.dao.execute(sql, Object.values(entity)).then(() => this.all(this.defaultSortField, this.defaultSortOrder))
    }
}
