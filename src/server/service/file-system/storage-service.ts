import path from 'path'
import fs from 'fs'

import Configuration from '../../../common/configuration'

const { logger } = require('../../../../script/build')

const CONFIGURATION_FOLDER = '.teaching-assistant'
const CONFIGURATION_FILE = 'teaching-assistant.json'
const DATABASE_FILE = 'teaching-assistant.sqlite3'

/**
 * Manage the following folder structure:
 * /user home folder/
 *      .teaching-assistant/
 *          teaching-assistant.json
 *          teaching-assistant.sqlite3
 */
export class StorageService {
    private configurationPath: string

    /**
     * @param storageFolder Root folder for application configuration and data.
     *
     * @see require('os').homedir()
     **/
    constructor (storageFolder: string) {
        if (!fs.existsSync(storageFolder)) {
            throw Error('Invalid configuration folder:' + storageFolder)
        }

        this.configurationPath = path.join(storageFolder, CONFIGURATION_FOLDER)
        if (!fs.existsSync(this.configurationPath)) {
            logger.info('First startup, creating configuration folder: ' + this.configurationPath)
            fs.mkdirSync(this.configurationPath)
        }
    }

    getDatabaseFilePath () {
        return path.join(this.configurationPath, DATABASE_FILE)
    }

    loadConfiguration () {
        let configuration: Configuration

        const configurationFile = path.join(this.configurationPath, CONFIGURATION_FILE)

        if (fs.existsSync(configurationFile)) {
            // TODO: use constant instead
            configuration = JSON.parse(fs.readFileSync(configurationFile, { encoding: 'utf-8' }))
        } else {
            configuration = {
                option: {
                    databaseFile: path.join(this.configurationPath, DATABASE_FILE),
                    port: 8080
                }
            }

            fs.writeFileSync(configurationFile, JSON.stringify(configuration))
        }

        return configuration
    }
}
