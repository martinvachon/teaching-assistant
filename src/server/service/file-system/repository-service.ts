import path from 'path'
import fs from 'fs'

import { Instance } from '../../../common/instance'
import { Template } from '../../../common/template'

const ROOT_FOLDER = '.teaching-assistant'
const TEMPLATES_FILE = 'templates.json'
const INSTANCES_FOLDER = 'instances'
const INSTANCE_FILE = 'instance.json'

const DEFAULT_TEMPLATES = {

}

export class RepositoryService {
    private repositoryFolder: string

    constructor (repositoryFolder: string) {
        this.repositoryFolder = repositoryFolder
    }

    getRootFolder () {
        return path.join(this.repositoryFolder, ROOT_FOLDER)
    }

    getTemplatesFile () {
        return path.join(this.getRootFolder(), TEMPLATES_FILE)
    }

    getTemplates (): Template[] {
        const json = fs.readFileSync(this.getTemplatesFile(), { encoding: 'utf-8' })
        return JSON.parse(json)
    }

    getInstance (templateCode: string, instanceNumber: string): Instance {
        const instanceFile = path.join(this.getRootFolder(), INSTANCES_FOLDER, templateCode, instanceNumber, INSTANCE_FILE)
        const json = fs.readFileSync(instanceFile, { encoding: 'utf-8' })
        return JSON.parse(json)
    }

    getInstances (templateCode: string): Instance[] {
        const instances: Instance[] = []

        const codeFolder = path.join(this.getRootFolder(), INSTANCES_FOLDER, templateCode)
        if (fs.existsSync(codeFolder)) {
            const instanceFolders = fs.readdirSync(codeFolder)

            instanceFolders.forEach(instanceFolder => {
                instances.push(this.getInstance(templateCode, instanceFolder))
            })
        }

        return instances
    }

    saveTemplate (template: Template) {
        const templates = this.getTemplates()

        const index = templates.findIndex(t => t.code === template.code)
        templates[index] = template

        fs.writeFileSync(this.getTemplatesFile(), JSON.stringify(templates, null, 4))

        return templates
    }

    getNextInstanceFolder (templateCode: string) {
        const codeFolder = path.join(this.getRootFolder(), INSTANCES_FOLDER, templateCode)
        if (fs.existsSync(codeFolder)) {
            const instanceFolders = fs.readdirSync(codeFolder)
            instanceFolders.sort()
            console.log('SORTED', instanceFolders)
        }
    }

    createInstance (templateCode: string) {
        const templates = this.getTemplates()
        const index = templates.findIndex(t => t.code === templateCode)
        const template = templates[index]

        const nextFolder = this.getNextInstanceFolder(templateCode)
        console.log(template, nextFolder)
    }

    validate () {
        if (!fs.existsSync(this.getRootFolder())) {
            return false
        }

        if (!fs.existsSync(this.getTemplatesFile())) {
            return false
        }

        return true
    }

    init () {
        if (fs.existsSync(this.getRootFolder())) {
            throw Error('The current folder already contain a Teaching-Assistant repository.')
        }
        fs.mkdirSync(this.getRootFolder())
        fs.writeFileSync(this.getTemplatesFile(), JSON.stringify(DEFAULT_TEMPLATES))
    }
}
