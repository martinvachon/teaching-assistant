import express from 'express'

import { TemplateDto } from '../../common/dto'
import { RepositoryService } from '../service/file-system'

const HTTP_OK = 200

export function templateRouterBuilder (repositoryService: RepositoryService) {
    const router = express.Router()

    router.post('/', (request, response, next) => {
        try {
            const result = repositoryService.saveTemplate(request.body)

            response.status(HTTP_OK).send(result)
        } catch (error) {
            return next(error)
        }
    })

    router.post('/:templateCode/instance', (request, response, next) => {
        try {
            const result = repositoryService.createInstance(request.params.templateCode)

            response.status(HTTP_OK).send(result)
        } catch (error) {
            return next(error)
        }
    })

    router.get('/:templateCode', (request, response, next) => {
        const templates = repositoryService.getTemplates()
        const template = templates.find(template => template.code === request.params.templateCode)

        response.status(HTTP_OK).send(template)
    })

    router.get('/', (request, response, next) => {
        try {
            const templates = repositoryService.getTemplates()
            const templateDtos: TemplateDto[] = templates.map(template => {
                return {
                    template,
                    instances: repositoryService.getInstances(template.code)
                }
            })

            response.status(HTTP_OK).send(JSON.stringify(templateDtos))
        } catch (error) {
            return next(error)
        }
    })

    return router
}
