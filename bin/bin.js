#!/usr/bin/env node

const nodemon = require('nodemon')
const path = require('path')
const os = require('os')
const open = require('open')

const create = require('../script/create')
const initialize = require('../script/initialize')
const aggregate = require('../script/aggregate')
const transform = require('../script/transform')
const distribute = require('../script/distribute')
const startServer = require('../dist/server').startServer
const translate = require('../script/translate')

const {
    getRootPath,
    getProjectRootPath,
    logger,
    PARMETER_PREFIX,
    DOC_FOLDER,
    LIB_FOLDER
} = require('../script/build')

function openIndexPage (context) {
    if (context.error) return

    open(path.join(getProjectRootPath(), 'dist/out/index.html'))
}

function setupNodemon () {
    let isOpen = false
    const docPath = path.join(getProjectRootPath(), DOC_FOLDER)
    const libPath = path.join(getProjectRootPath(), LIB_FOLDER)
    nodemon({
        script: path.join(getRootPath(), 'script/nodemon.js'),
        ext: 'xml',
        watch: [
            docPath,
            libPath
        ]
    })

    nodemon.on('start', function () {
        logger.info('Edit mode started')
        if (!isOpen) {
            openIndexPage({
                error: false
            })
            isOpen = true
        }
    }).on('quit', function () {
        logger.info('Edit mode has quit')
        process.exit()
    }).on('restart', function (files) {
        logger.info('Edit mode restarted due to change in: ', files)
    })
}

logger.debug(process.cwd())

const [,, ...args] = process.argv

logger.debug(args.join(' '))

const action = (args.length > 0 && !args[0].startsWith(PARMETER_PREFIX) ? args[0] : undefined)
const parameters = args.filter(parameter => parameter.startsWith(PARMETER_PREFIX))

if (action === undefined) {
    startServer(os.homedir(), process.cwd(), parameters)
} else {
    const context = {
        error: false
    }
    if (action === 'create') {
        create(context, parameters)
        initialize(context, parameters)
        aggregate(context, parameters)
        transform(context, parameters)
        distribute(context, parameters)
        openIndexPage(context)
    } else if (action === 'edit') {
        initialize(context, parameters)
        setupNodemon(context, parameters)
    } else if (action === 'dist') {
        initialize(context, parameters)
        aggregate(context, parameters)
        transform(context, parameters)
        distribute(context, parameters)
    } else if (action === 'translate') {
        translate(parameters)
    }
}
