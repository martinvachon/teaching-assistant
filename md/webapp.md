# teaching-assistant (beta)

## Web application

The command line manages your course documents and the web application manages student activities.

How to manage student activities ?

- Students list
- Exercises submitted
- Exercises received
- Course instance creation based on course template


> ### Start the web application:

```
ta
```

> ### Supported parameters:

```
ta --port=80
```