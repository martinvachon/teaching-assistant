# Release

    - complete command line for basic operation
    - Add watch for edit command line
    - Client and server build with typescript
    - Storage service
    - sqlite, database initialization
    - CRUD service, student entity
    - client student CRUD
    - ta command start the server

## 2.1.0

    - templates endpoint and ui

## 2.2.0

    - fix nodemon installation

## 2.2.1

    - Fix case in font name, unable to test on window$ shit

## 2.2.2

    - Possible fix: Add dummy file to keep empty folders in GIT

## 2.2.3

    - Better logging using winston
    - Stop execution when previous step fail
    - Update installation readme
    - Initial help in web app for icon selection
    - Fix context bug with edit nodemon
    - Fix getLanguages root project path
    - Support multiple generator (Moodle-slides)
    - Fix: nodemon config to enable "ta edit" in teaching-assistant project

## 2.2.4

    - Add port configuration --port= (for ta command)
    - Fix: Duplicate in output: <title>Teaching assistant</title>
    - Fix: use XSLT3 version attribute instead of doctype-system="about:legacy-compat"

## 2.3.0

    - Add teaching-assistant-doc meta for custom folder aggregation (<section> and /asset)
    - Open browser after command line actions
    - Add index level h3 in menu
    - Initial home page
    - Initial documentation

## 2.3.1

    - Upgrade bootstrap v4.3.1
    - Inject lang attribute according to folder and fix doc

## 2.4.0

    - Add version and date in page bottom (moodle-player)

## 2.4.1

    - Fix Asset loading in moodle-slides
    - Update "Copy" button position absolute

## 2.5.0

    - MathJax integration

## 2.5.1

    - Update doc and fix
    - remove duplicate lib with moodle-player and moodle-slide

## 2.6.0

    - Fix release file link
    - Update favicon and remove duplicate files
    - Update index page, bootstrap and logo

## 2.7.0

    - LibreTranslate integration

## 2.7.1

    - Add "ta translate" command with --url parmeter

## 2.7.2

    - Add support for &lt; &gt; &amp; &apos;
    - Fix lang attribute after translation
    - Delete config injected by initial aggregation: teaching-assistant-course-datetime, teaching-assistant-version
    - First letter uppercase
    - Add loggin and warning if folder exist
    - Translate and uppercase the first letter of each attributes for code and img (data-title, data-subtitle)
    - Add command line doc in readme

## 2.7.3

    - Documentation spell check
    - Complete doc refactoring
    - Add css support for ol, dl
    - Add doc for list
    - Add hook for generator beta version
    - Add beta _stephane theme

## 2.7.4

    - Fix id generation

## 2.7.5

    - Fix menu id generation for third level

## 2.7.6

    - moodle-player_book theme testing

## 2.7.7

    - Remove support for special character (use-character-maps)
    - Remove english sample when using "ta create"
    - Add parameter teaching-assistant-paragraph-bullet for paragraph format
    - Add parameters teaching-assistant-list-class and teaching-assistant-list-item-class for list format
    - Add parameter teaching-assistant-h1-class for h1 format
    - Add parameter teaching-assistant-h-indentation-class for indentation according h tag
    - Document the use of custom css file in asset folder

## 2.8.0

    - Bigger font size for code and strong
    - Add support for normalize-space attribut
    - Fix nested source discovery (third tab)

## 2.8.1

    - Fix nested source discovery (third tab) second fix
